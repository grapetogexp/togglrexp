﻿using System;
using NUnit.Framework;
using TogglrExp.Model.Jira;
using TogglrExp.Model.Settings;
using TogglrExp.Tests.ServiceSubstitutes;
using TogglrExp.Tests.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel;
using TogglrExp.ViewModel.DesignTime;
using TogglrExp.ViewModel.Navigation;
using JiraService = TogglrExp.ViewModel.DesignTime.JiraService;

namespace TogglrExp.Tests.ViewModel
{
    [TestFixture]
    public class JiraConfigurationSelectorViewModelTest
    {
        [Test]
        public void CreationAndDisposalDoNotFail()
        {
            Assert.DoesNotThrow(() =>
            {
                using (CreateNew())
                {
                    
                }
            });
        }

        public static Func<JiraConfigurationSelectorViewModel> CreateFactory(
            IJiraService jiraService = null,
            ISettingsManager settingsManager = null,
            INavigationService navigationService = null)
        {
            return () => CreateNew(jiraService, settingsManager, navigationService);
        }

        private static JiraConfigurationSelectorViewModel CreateNew(
            IJiraService jiraService = null,
            ISettingsManager settingsManager = null,
            INavigationService navigationService = null)
        {

            jiraService = jiraService ?? new JiraService();
            settingsManager = settingsManager ?? new SettingsManagerSubstitute();
            navigationService = navigationService ?? new NavigationService();

            return new JiraConfigurationSelectorViewModel(
                jiraService,
                settingsManager,
                navigationService,
                JiraConfigurationDialogTest.CreateFactory(navigationService));
        }
    }
}