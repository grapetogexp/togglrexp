﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.ViewModel.About;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.Tests.ViewModel.About
{
    [TestFixture]
    public class AboutViewModelTest
    {
        [Test]
        public void CreationDoesNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        public static Func<AboutViewModel> CreateFactory(INavigationService navigationService = null)
        {
            return () => CreateNew(navigationService);
        }

        private static AboutViewModel CreateNew(
            INavigationService navigationService = null)
        {
            navigationService = navigationService ?? A.Fake<INavigationService>();

            return new AboutViewModel(navigationService);
        }
    }
}