﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel;

namespace TogglrExp.Tests.ViewModel
{
    [TestFixture]
    public class JiraIssueListViewModelTest
    {
        [Test]
        public void CreationAndDisposalDoNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }
        
        public static Func<JiraIssueListViewModel> CreateFactory(
            IJiraService jiraService = null)
        {
            return () => CreateNew(jiraService);
        }

        private static JiraIssueListViewModel CreateNew(
            IJiraService jiraService = null)
        {
            jiraService = jiraService ?? A.Fake<IJiraService>();

            return new JiraIssueListViewModel(
                jiraService,
                JiraConfigurationSelectorViewModelTest.CreateFactory(jiraService));
        }
    }
}