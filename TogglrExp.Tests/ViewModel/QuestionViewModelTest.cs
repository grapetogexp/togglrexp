﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.ViewModel;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.Tests.ViewModel
{
    [TestFixture]
    public class QuestionViewModelTest
    {
        [Test]
        public void CreationDoesNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        [Test]
        public void ClickingYesCausesNavigationBackWithConfirmation()
        {
            // Given
            var navigationService = new NavigationService();

            var viewModel = CreateNew("Lorem ipsum", navigationService);
            
            // When
            var task = navigationService.Open(viewModel);
            viewModel.YesCommand.Execute(null);

            // Then
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.Result, Is.True);
        }

        [Test]
        public void ClickingNoCausesNavigationBackWithCancelation()
        {
            // Given
            var navigationService = new NavigationService();

            var viewModel = CreateNew("Lorem ipsum", navigationService);

            // When
            var task = navigationService.Open(viewModel);
            viewModel.NoCommand.Execute(null);

            // Then
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.Result, Is.False);
        }

        public static Func<string, QuestionViewModel> CreateFactory(
            INavigationService navigationService = null)
        {
            return message => CreateNew(message, navigationService);
        }

        private static QuestionViewModel CreateNew(
            string message = "Lorem ipsum",
            INavigationService navigationService = null)
        {

            navigationService = navigationService ?? A.Fake<INavigationService>();

            return new QuestionViewModel(message, navigationService);
        }
    }
}