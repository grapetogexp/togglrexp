﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model.DbUpdate;
using TogglrExp.Tests.ViewModel.About;
using TogglrExp.Tests.ViewModel.Holiday;
using TogglrExp.Tests.ViewModel.NotificationCenter;
using TogglrExp.Tests.ViewModel.OvertimeReport;
using TogglrExp.ViewModel;
using TogglrExp.ViewModel.About;
using TogglrExp.ViewModel.Holiday;
using TogglrExp.ViewModel.Navigation;
using TogglrExp.ViewModel.NotificationCenter;
using TogglrExp.ViewModel.OvertimeReport;
using TogglrExp.ViewModel.Timetrack;

namespace TogglrExp.Tests.ViewModel
{
    [TestFixture]
    public class MainWindowViewModelTest
    {
        [Test]
        public void CreationAndDisposalDoNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() =>
            {
                using (CreateNew())
                {
                    
                }
            });
        }
        
        private static MainWindowViewModel CreateNew(
            Func<SettingsViewModel> settingsViewModelFactory = null,
            Func<TimeTrackViewModel> timetrackFactory = null,
            Func<HolidayViewModel> holidayViewModelFactory = null,
            Func<OvertimeReportViewModel> overtimeReportViewModelFactory = null,
            Func<AboutViewModel> aboutViewModelFactory = null,
            INavigationService navigationService = null,
            NotificationCenterViewModel notificationCenterViewModel = null,
            IDbUpdateService dbUpdateService = null)
        {
            navigationService = navigationService ?? new NavigationService();

            settingsViewModelFactory = settingsViewModelFactory ??
                                       SettingsViewModelTest.CreateFactory(navigationService: navigationService);

            holidayViewModelFactory = holidayViewModelFactory ??
                                      HolidayViewModelTest.CreateFactory(navigationService: navigationService);

            overtimeReportViewModelFactory = overtimeReportViewModelFactory ??
                                             OvertimeReportViewModelTest.CreateFactory(
                                                 navigationService: navigationService);

            aboutViewModelFactory = aboutViewModelFactory ??
                                    AboutViewModelTest.CreateFactory(navigationService);

            notificationCenterViewModel = notificationCenterViewModel ??
                                          NotificationCenterViewModelTest.CreateNew();

            dbUpdateService = dbUpdateService ?? A.Fake<IDbUpdateService>();

            return new MainWindowViewModel(
                settingsViewModelFactory,
                timetrackFactory,
                holidayViewModelFactory,
                overtimeReportViewModelFactory,
                aboutViewModelFactory,
                navigationService,
                notificationCenterViewModel,
                dbUpdateService);
        }
    }
}