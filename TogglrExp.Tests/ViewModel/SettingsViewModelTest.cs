﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model.Settings;
using TogglrExp.Tests.ServiceSubstitutes;
using TogglrExp.Tests.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel;
using TogglrExp.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.Tests.ViewModel
{
    [TestFixture]
    public class SettingsViewModelTest
    {
        [Test]
        public void CreationDoesNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        public static Func<SettingsViewModel> CreateFactory(
            ISettingsManager settingsManager = null,
            INavigationService navigationService = null)
        {
            return () => CreateNew(settingsManager, navigationService);
        }

        private static SettingsViewModel CreateNew(
            ISettingsManager settingsManager = null,
            INavigationService navigationService = null,
            Func<JiraConfigurationsViewModel> jiraConfigurationsFactory = null)
        {
            settingsManager = settingsManager ?? new SettingsManagerSubstitute();
            navigationService = navigationService ?? A.Fake<INavigationService>();
            jiraConfigurationsFactory = jiraConfigurationsFactory ??
                                        JiraConfigurationsViewModelTest.CreateFactory(
                                            navigationService: navigationService);

            return new SettingsViewModel(
                settingsManager, 
                navigationService,
                jiraConfigurationsFactory);
        }
    }
}