﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TogglrExp.ViewModel.NotificationCenter;

namespace TogglrExp.Tests.ViewModel.NotificationCenter
{
    [TestFixture]
    public class NotificationCenterViewModelTest
    {
        [Test]
        public void ViewModelCreationAndDisposalDoesNotFails()
        {
            // Given
            // When

            using (new NotificationCenterViewModel(new[] {new SimpleNotificationSource()}))
            {

            }
        }

        [Test]
        public void IfNotificationAppersItIsDisplayed()
        {
            // Given
            var source = new SimpleNotificationSource();
            var viewModel = new NotificationCenterViewModel(new []{source});

            // When
            source.CreateNotifications(1);

            // Then
            CollectionAssert.IsNotEmpty(viewModel.Notifications);
        }

        [Test]
        public void NotificationIsRemovedIfItExpires()
        {
            // Given
            var source = new SimpleNotificationSource();
            var viewModel = new NotificationCenterViewModel(new[] { source });

            var notifications = source.CreateNotifications(2);
            var firstNotification = notifications.First();

            // When
            firstNotification.RaiseExpiration();

            // Then
            CollectionAssert.DoesNotContain(viewModel.Notifications, firstNotification);
        }

        public static NotificationCenterViewModel CreateNew(IEnumerable<INotificationSource> notificationSources = null)
        {
            notificationSources = notificationSources ?? Enumerable.Empty<INotificationSource>();
            return new NotificationCenterViewModel(notificationSources);
        }

        private sealed class SimpleNotificationSource : INotificationSource
        {
            public event EventHandler<NotificationAppearedArgs> NotificationAppeared;

            public SimpleNotification[] CreateNotifications(int amount)
            {
                var result =
                    Enumerable
                        .Range(0, amount)
                        .Select(o => new SimpleNotification())
                        .ToArray();

                NotificationAppeared?.Invoke(this, new NotificationAppearedArgs(result));

                return result;
            }
        }

        private sealed class SimpleNotification : INotification
        {
            public void RaiseExpiration()
            {
                Expired?.Invoke(this, EventArgs.Empty);
            }

            public void Dispose()
            {
                
            }

            public event EventHandler Expired;
        }
    }
}