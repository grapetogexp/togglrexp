﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.Tests.ViewModel.JiraConfigurations
{
    [TestFixture]
    public class JiraConfigurationsViewModelTest
    {
        [Test]
        public void CreationAndDisposalDoNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() =>
            {
                using (CreateNew())
                {

                }
            });
        }

        public static Func<JiraConfigurationsViewModel> CreateFactory(
            IJiraService jiraService = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            Func<Configuration, JiraConfigurationListItem> listItemFactory = null)
        {
            return () => CreateNew(jiraService, navigationService, businessManager, listItemFactory);
        }

        private static JiraConfigurationsViewModel CreateNew(
            IJiraService jiraService = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            Func<Configuration, JiraConfigurationListItem> listItemFactory = null,
            Func<SaveCallback, JiraConfigurationDialog> dialogFactory = null)
        {
            jiraService = jiraService ?? A.Fake<IJiraService>();
            navigationService = navigationService ?? A.Fake<INavigationService>();
            businessManager = businessManager ?? A.Fake<IBusinessManager>();

            
            listItemFactory = listItemFactory ??
                              JiraConfigurationListItemTest.CreateFactory(jiraService, navigationService);

            return new JiraConfigurationsViewModel(
                jiraService,
                navigationService,
                businessManager,
                listItemFactory,
                dialogFactory);
        }
    }
}