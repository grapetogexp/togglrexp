﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Infrastructure;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel;
using TogglrExp.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.Tests.ViewModel.JiraConfigurations
{
    [TestFixture]
    public class JiraConfigurationListItemTest
    {
        [Test]
        public void CreationDoesNotFails()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        public static Func<Configuration, JiraConfigurationListItem> CreateFactory(
            IJiraService jiraService = null,
            INavigationService navigationService = null,
            IUriOpener uriOpener = null,
            Func<SaveCallback, JiraConfigurationDialog> editDialogFactory = null,
            Func<string, QuestionViewModel> questionDialogFactory = null)
        {
            return configuration => CreateNew(
                configuration, 
                jiraService,
                navigationService,
                uriOpener,
                editDialogFactory,
                questionDialogFactory);
        }

        private static JiraConfigurationListItem CreateNew(
            Configuration configuration = null,
            IJiraService jiraService = null,
            INavigationService navigationService = null,
            IUriOpener uriOpener = null,
            Func<SaveCallback, JiraConfigurationDialog> editDialogFactory = null,
            Func<string, QuestionViewModel> questionDialogFactory = null)
        {
            configuration = configuration ?? new Configuration(1, "konfiguracja", "http://jira.com", string.Empty);
            jiraService = jiraService ?? A.Fake<IJiraService>();
            navigationService = navigationService ?? A.Fake<INavigationService>();
            uriOpener = uriOpener ?? A.Fake<IUriOpener>();
            editDialogFactory = editDialogFactory ?? JiraConfigurationDialogTest.CreateFactory(navigationService);
            questionDialogFactory = questionDialogFactory ?? QuestionViewModelTest.CreateFactory(navigationService);


            return new JiraConfigurationListItem(
                configuration, 
                jiraService,
                navigationService,
                uriOpener,
                editDialogFactory,
                questionDialogFactory);
        }
    }
}