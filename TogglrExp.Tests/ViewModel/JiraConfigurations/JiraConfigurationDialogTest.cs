﻿using System;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.Tests.ViewModel.JiraConfigurations
{
    [TestFixture]
    public class JiraConfigurationDialogTest
    {
        [Test]
        public void CreationDoesNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        public static Func<SaveCallback, JiraConfigurationDialog> CreateFactory(
            INavigationService navigationService = null,
            IBusinessManager businessManager = null)
        {
            return callback => CreateNew(callback, navigationService, businessManager);
        }

        private static JiraConfigurationDialog CreateNew(
            SaveCallback saveCallback = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null)
        {
            saveCallback = saveCallback ?? (prototype => Task.FromResult(ValidationResult.Valid));
            navigationService = navigationService ?? A.Fake<INavigationService>();
            businessManager = businessManager ?? A.Fake<IBusinessManager>();

            return new JiraConfigurationDialog(saveCallback, navigationService, businessManager);
        }
    }
}