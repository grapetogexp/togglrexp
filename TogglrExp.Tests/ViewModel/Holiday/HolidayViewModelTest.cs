﻿using System;
using FakeItEasy;
using GalaSoft.MvvmLight.Messaging;
using NUnit.Framework;
using TogglrExp.Model.Holiday;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Holiday;
using TogglrExp.ViewModel.Navigation;
using HolidayModel = TogglrExp.Model.Holiday.Holiday;

namespace TogglrExp.Tests.ViewModel.Holiday
{
    [TestFixture]
    public class HolidayViewModelTest
    {
        [Test]
        public void CreationAndDisposalDoNotFails()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() =>
            {
                using (CreateNew())
                {
                    
                }
            });
        }

        public static Func<HolidayViewModel> CreateFactory(
            IHolidayService holidayService = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            IMessenger messenger = null,
            Func<AddHolidayViewModel> addHolidayFactory = null,
            Func<HolidayModel, HolidayListItemViewModel> listItemFactory = null)
        {
            return () =>
                CreateNew(
                    holidayService,
                    navigationService,
                    businessManager,
                    messenger,
                    addHolidayFactory,
                    listItemFactory);
        }

        private static HolidayViewModel CreateNew(
            IHolidayService holidayService = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            IMessenger messenger = null,
            Func<AddHolidayViewModel> addHolidayFactory = null,
            Func<HolidayModel, HolidayListItemViewModel> listItemFactory = null)
        {
            holidayService = holidayService ?? A.Fake<IHolidayService>();
            navigationService = navigationService ?? A.Fake<INavigationService>();
            businessManager = businessManager ?? A.Fake<IBusinessManager>();
            messenger = messenger ?? A.Fake<IMessenger>();

            addHolidayFactory = addHolidayFactory ??
                                AddHolidayViewModelTest.CreateFactory(navigationService, holidayService,
                                    businessManager);

            listItemFactory = listItemFactory ??
                              HolidayListItemViewModelTest.CreateFactory(navigationService, holidayService,
                                  businessManager);

            return new HolidayViewModel(
                holidayService,
                navigationService,
                businessManager,
                messenger,
                addHolidayFactory,
                listItemFactory);
        }
    }
}