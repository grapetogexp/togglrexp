﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model.Holiday;
using TogglrExp.ViewModel;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Holiday;
using TogglrExp.ViewModel.Navigation;
using HolidayModel = TogglrExp.Model.Holiday.Holiday;

namespace TogglrExp.Tests.ViewModel.Holiday
{
    [TestFixture]
    public class HolidayListItemViewModelTest
    {
        [Test]
        public void CreationDoNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        [Test]
        public void IfUserClickDelete_And_ConfirmsDialog_RecordIsDeleted()
        {
            // Given
            var navigationService = new NavigationService();

            var holidayService = A.Fake<IHolidayService>();

            var holiday = new HolidayModel(new DateTime(2016, 05, 01), "Świeto pracy");
            var viewModel = CreateNew(holiday, navigationService, holidayService);

            // When
            viewModel.DeleteCommand.Execute(null);
            var dialog = (QuestionViewModel)navigationService.Current;
            dialog.YesCommand.Execute(null);

            // Then
            A
                .CallTo(() => holidayService.Delete(holiday.Date))
                .MustHaveHappened();
        }

        public static Func<HolidayModel, HolidayListItemViewModel> CreateFactory(
            INavigationService navigationService = null,
            IHolidayService holidayService = null,
            IBusinessManager businessManager = null,
            Func<string, QuestionViewModel> questionDialogFactory = null)
        {
            return holiday =>
                CreateNew(
                    holiday,
                    navigationService,
                    holidayService,
                    businessManager,
                    questionDialogFactory);
        }

        private static HolidayListItemViewModel CreateNew(
            HolidayModel holiday = null,
            INavigationService navigationService = null,
            IHolidayService holidayService = null,
            IBusinessManager businessManager = null,
            Func<string, QuestionViewModel> questionDialogFactory = null)
        {
            holiday = holiday ?? new HolidayModel(new DateTime(2017, 05, 03), "Konstytucja");
            navigationService = navigationService ?? A.Fake<INavigationService>();
            holidayService = holidayService ?? A.Fake<IHolidayService>();
            businessManager = businessManager ?? A.Fake<IBusinessManager>();
            questionDialogFactory = questionDialogFactory ??
                                    QuestionViewModelTest.CreateFactory(navigationService);

            return new HolidayListItemViewModel(
                holiday,
                navigationService,
                holidayService,
                businessManager,
                questionDialogFactory);
        }
    }
}