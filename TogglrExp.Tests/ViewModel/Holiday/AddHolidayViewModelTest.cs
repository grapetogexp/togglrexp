﻿using System;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model.Holiday;
using TogglrExp.Model.TimeProvider;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Holiday;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.Tests.ViewModel.Holiday
{
    [TestFixture]
    public class AddHolidayViewModelTest
    {
        [Test]
        public void CreationDoesNotFail()
        {
            // Given

            // When
            CreateNew();

            // Then
            Assert.Pass();
        }

        public static Func<AddHolidayViewModel> CreateFactory(
            INavigationService navigationService = null,
            IHolidayService holidayService = null,
            IBusinessManager businessManager = null,
            IDateTimeProvider dateTimeProvider = null)
        {
            return () => CreateNew(navigationService, holidayService, businessManager, dateTimeProvider);
        }

        private static AddHolidayViewModel CreateNew(
            INavigationService navigationService = null,
            IHolidayService holidayService = null,
            IBusinessManager businessManager = null,
            IDateTimeProvider dateTimeProvider = null)
        {
            navigationService = navigationService ?? A.Fake<INavigationService>();
            holidayService = holidayService ?? A.Fake<IHolidayService>();
            businessManager = businessManager ?? A.Fake<IBusinessManager>();
            dateTimeProvider = dateTimeProvider ?? A.Fake<IDateTimeProvider>();

            return new AddHolidayViewModel(
                navigationService,
                holidayService,
                businessManager,
                dateTimeProvider);
        }
    }
}