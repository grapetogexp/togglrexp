﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TogglrExp.Model;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;
using TogglrExp.ViewModel.Timetrack.ExportToMultipleIssues;

namespace TogglrExp.Tests.ViewModel.Timetrack.ExportToMultipleIssues
{
    [TestFixture]
    public class ExportToMultipleIssuesViewModelTest
    {
        [Test]
        public void CreationDoesNotFails()
        {
            Assert.DoesNotThrow(() => CreateNew());
        }

        public static Func<IEnumerable<Worklog>, ExportToMultipleIssuesViewModel> CreateFactory(
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            IJiraService jiraService = null,
            Func<Configuration, SelectIssuesViewModel> selectIssuesFactory = null)
        {
            return o => CreateNew(
                o, 
                navigationService, 
                businessManager, 
                jiraService, 
                selectIssuesFactory);
        }

        private static ExportToMultipleIssuesViewModel CreateNew(
            IEnumerable<Worklog> worklogs = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            IJiraService jiraService = null,
            Func<Configuration, SelectIssuesViewModel> selectIssuesFactory = null)
        {
            worklogs = worklogs ?? Enumerable.Empty<Worklog>();
            navigationService = navigationService ?? new NavigationService();

            return new ExportToMultipleIssuesViewModel(
                worklogs,
                navigationService,
                businessManager,
                jiraService,
                selectIssuesFactory);
        }
    }
}