﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TogglrExp.Model;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;
using TogglrExp.ViewModel.Timetrack;
using JiraService = TogglrExp.ViewModel.DesignTime.JiraService;

namespace TogglrExp.Tests.ViewModel.Timetrack
{
    [TestFixture]
    public class JiraWorklogViewModelTest
    {
        [Test]
        public void CreationDoesNotFail()
        {
            // Given

            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        public static Func<IEnumerable<Worklog>, JiraWorklogViewModel> CreateFactory(
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            IJiraService jiraService = null)
        {
            return worklogs => CreateNew(
                worklogs,
                navigationService,
                businessManager,
                jiraService);
        }

        private static JiraWorklogViewModel CreateNew(
            IEnumerable<Worklog> worklogs = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            IJiraService jiraService = null)
        {

            worklogs = worklogs ?? Enumerable.Empty<Worklog>();
            navigationService = navigationService ?? new NavigationService();
            businessManager = businessManager ?? new BusinessManager(navigationService);
            jiraService = jiraService ?? new JiraService();

            var selectorFactory =
                JiraConfigurationSelectorViewModelTest.CreateFactory(jiraService, navigationService: navigationService);

            return new JiraWorklogViewModel(
                worklogs,
                navigationService,
                businessManager,
                jiraService,
                selectorFactory,
                null);
        }
    }
}