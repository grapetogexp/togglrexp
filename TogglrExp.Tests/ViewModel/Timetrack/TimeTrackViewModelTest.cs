﻿using FakeItEasy;
using NUnit.Framework;
using TogglrExp.ViewModel.Navigation;
using TogglrExp.ViewModel.Timetrack;

namespace TogglrExp.Tests.ViewModel.Timetrack
{
    [TestFixture]
    public class TimeTrackViewModelTest
    {
        [Test]
        public void CreationDoesNotFail()
        {
            // Given

            // When
            CreateNew();

            // Then
            Assert.Pass();
        }

        [Test]
        public void RepeatableReportDoesNotFail()
        {
            // Given
            var viewModel = CreateNew();

            // When / Then
            Assert.DoesNotThrow(() => viewModel.RepeatableReportCommand.Execute(null));
        }

        [Test]
        public void TogglImportOpenDoesNotFail()
        {
            // Given
            var viewModel = CreateNew();

            // When / Then
            Assert.DoesNotThrow(() => viewModel.ImportFromTogglCommand.Execute(null));
        }

        [Test]
        public void ItemsFromRepeatableReport_CanBeExportedToClipboard()
        {
            // Given
            var viewModel = CreateNew();
            viewModel.RepeatableReportCommand.Execute(null);

            // When / Then
            Assert.DoesNotThrow(() => viewModel.RefreshPreviewCommand.Execute(null));
        }

        [Test]
        public void ItemsFromRepeatableReport_CanBeExportedToJira()
        {
            // Given
            var viewModel = CreateNew();
            viewModel.RepeatableReportCommand.Execute(null);
            viewModel.RefreshPreviewCommand.Execute(null);

            // When / Then
            Assert.DoesNotThrow(() => viewModel.GoNextCommand.Execute(null));
        }

        [Test]
        public void ItemsFromToggl_CanBeExportedToClipboard()
        {
            // Given
            var viewModel = CreateNew();
            viewModel.ImportFromTogglCommand.Execute(null);

            // When / Then
            Assert.DoesNotThrow(() => viewModel.RefreshPreviewCommand.Execute(null));
        }

        [Test]
        public void ItemsFromToggl_CanBeExportedToJira()
        {
            // Given
            var viewModel = CreateNew();
            viewModel.ImportFromTogglCommand.Execute(null);
            viewModel.RefreshPreviewCommand.Execute(null);

            // When / Then
            Assert.DoesNotThrow(() => viewModel.GoNextCommand.Execute(null));
        }
        
        private static TimeTrackViewModel CreateNew(
            INavigationService navigationService = null)
        {
            navigationService = navigationService ?? A.Fake<INavigationService>();

            var copyPasteFactory = WorklogPreviewViewModelTest.CreateFactory();

            return new TimeTrackViewModel(
                navigationService,
                TogglWorklogViewModelTest.CreateFactory(),
                RepeatableTimeTrackViewModelTest.CreateFactory(),
                copyPasteFactory,
                JiraWorklogViewModelTest.CreateFactory(navigationService));
        }
    }
}