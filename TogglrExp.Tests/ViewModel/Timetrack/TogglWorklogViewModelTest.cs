﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model.Settings;
using TogglrExp.Model.TimeProvider;
using TogglrExp.Model.Toggl;
using TogglrExp.Tests.ServiceSubstitutes;
using TogglrExp.ViewModel.Timetrack;

namespace TogglrExp.Tests.ViewModel.Timetrack
{
    [TestFixture]
    public class TogglWorklogViewModelTest
    {
        [Test]
        public void CreationAndDisposalDoNotFail()
        {
            // Given
            // WHen / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        [Test]
        public void CurrentDateIsSetInitially()
        {
            // Given
            var today = new DateTime(2016, 02, 24);
            var dateTimeProvider = A.Fake<IDateTimeProvider>();
            A
                .CallTo(() => dateTimeProvider.Today)
                .Returns(today);

            // When
            var viewModel = CreateNew(dateTimeProvider: dateTimeProvider);

            // Then
            Assert.That(viewModel.DateFrom, Is.EqualTo(today));
            Assert.That(viewModel.DateTo, Is.EqualTo(today));
        }

        [Test]
        public async Task CommentsAreFormatedAsExpected()
        {
            // Given
            var tasks = new List<TaskItem>
            {
                Item(08, 00, 8, 15, "Bla Bla"),
                Item(08, 15, 8, 30, "Ble Ble")
            };

            var taskSource = A.Fake<ITaskSource>();
            A
                .CallTo(() => taskSource.GetItems(A<TaskFilter>._, A<CancellationToken>._))
                .Returns(tasks);

            var viewModel = CreateNew(taskSource);

            // When
            var workLogs = await viewModel.Provide();

            // Then
            
            Assert.That(workLogs.First().Comment, Is.EqualTo("08:00-08:15\t00:15\tBla Bla"));
        }

        [Test]
        public async Task If_ColumnExportIsDisabled_ItContentIsNotExported()
        {
            // Given
            var settingsManager = new SettingsManagerSubstitute();
            settingsManager.Modify(settings =>
            {
                settings.DescriptionExported = false;
            });

            var tasks = new List<TaskItem>
            {
                Item(08, 00, 8, 15, "Bla Bbla"),
                Item(08, 15, 8, 30, "Ble Ble")
            };

            var taskSource = A.Fake<ITaskSource>();
            A
                .CallTo(() => taskSource.GetItems(A<TaskFilter>._, A<CancellationToken>._))
                .Returns(tasks);

            var viewModel = CreateNew(taskSource, settingsManager);

            // When
            var workLogs = await viewModel.Provide();

            // Then
            Assert.That(workLogs.First().Comment, Is.EqualTo("08:00-08:15\t00:15"));
        }

        [Test]
        public async Task AmountOfTime_Has_ExpectedValue()
        {
            // Given
            var tasks = new List<TaskItem>
            {
                Item(08, 00, 8, 15, "Bla Bla"),
                Item(08, 15, 8, 30, "Ble Ble")
            };

            var taskSource = A.Fake<ITaskSource>();
            A
                .CallTo(() => taskSource.GetItems(A<TaskFilter>._, A<CancellationToken>._))
                .Returns(tasks);

            var viewModel = CreateNew(taskSource);

            // When
            var workLogs = await viewModel.Provide();

            // Then
            Assert.That(workLogs.First().TimeSpent, Is.EqualTo(TimeSpan.FromMinutes(15)));
        }

        public static Func<TogglWorklogViewModel> CreateFactory(
            ITaskSource taskSource = null,
            ISettingsManager settingsManager = null,
            IDateTimeProvider dateTimeProvider = null)
        {
            return () => CreateNew(taskSource, settingsManager, dateTimeProvider);
        }

        private static TogglWorklogViewModel CreateNew(
            ITaskSource taskSource = null,
            ISettingsManager settingsManager = null,
            IDateTimeProvider dateTimeProvider = null)
        {
            taskSource = taskSource ?? A.Fake<ITaskSource>();
            settingsManager = settingsManager ?? new SettingsManagerSubstitute();
            dateTimeProvider = dateTimeProvider ?? A.Fake<IDateTimeProvider>();

            return new TogglWorklogViewModel(taskSource, settingsManager, dateTimeProvider);
        }

        private static TaskItem Item(
            int hourFrom,
            int minuteFrom,
            int hourTo,
            int minuteTo,
            string description = "Bla bla")
        {
            int minutes = (hourTo - hourFrom) * 60 + (minuteTo - minuteFrom);

            TimeSpan duration = TimeSpan.FromMinutes(minutes);

            return new TaskItem
            {
                Description = description,
                Duration = duration,
                TimeFrom = new DateTime(2016, 10, 01, hourFrom, minuteFrom, 00),
                TimeTo = new DateTime(2016, 10, 01, hourTo, minuteTo, 00)
            };
        }
    }
}