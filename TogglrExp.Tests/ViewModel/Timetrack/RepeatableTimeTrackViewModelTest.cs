﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model;
using TogglrExp.Model.Holiday;
using TogglrExp.Model.TimeProvider;
using TogglrExp.ViewModel.Timetrack;

namespace TogglrExp.Tests.ViewModel.Timetrack
{
    [TestFixture]
    public class RepeatableTimeTrackViewModelTest
    {
        [Test]
        public void CreationAndDisposalDoNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        [Test]
        public void CurrentDateIsSetInitially()
        {
            // Given
            var today = new DateTime(2016, 02, 24);
            var dateTimeProvider = A.Fake<IDateTimeProvider>();
            A
                .CallTo(() => dateTimeProvider.Today)
                .Returns(today);

            // When
            var viewModel = CreateNew(dateTimeProvider: dateTimeProvider);

            // Then
            Assert.That(viewModel.DateFrom, Is.EqualTo(today));
            Assert.That(viewModel.DateTo, Is.EqualTo(today));
        }

        [Test]
        public async Task WorklogIsCreatedForEveryDayInRange()
        {
            // Given
            var dateFrom = new DateTime(2017, 02, 06);
            var dateTo = new DateTime(2017, 02, 08);

            var viewModel = CreateNew();

            // When
            viewModel.DateFrom = dateFrom;
            viewModel.DateTo = dateTo;
            viewModel.Hours = 8;
            viewModel.Minutes = 0;
            viewModel.Comment = "URLOP";

            var worklogs = await viewModel.Provide();

            // Then
            var expected = new[]
            {
                new Worklog(new DateTime(2017, 02, 06), TimeSpan.FromHours(8), "URLOP", "URLOP"),
                new Worklog(new DateTime(2017, 02, 07), TimeSpan.FromHours(8), "URLOP", "URLOP"),
                new Worklog(new DateTime(2017, 02, 08), TimeSpan.FromHours(8), "URLOP", "URLOP")
            };
            Assert.That(worklogs, Is.EquivalentTo(expected));
        }

        [Test]
        public async Task HolidaysAreSkippedIfChecked()
        {
            // Given
            var dateFrom = new DateTime(2017, 02, 03);
            var dateTo = new DateTime(2017, 02, 07);

            IEnumerable<TogglrExp.Model.Holiday.Holiday> holidays = new[]
            {
                new TogglrExp.Model.Holiday.Holiday(new DateTime(2017, 02, 04), "Sobota"),
                new TogglrExp.Model.Holiday.Holiday(new DateTime(2017, 02, 05), "Niedziela"),
            };

            var holidayService = A.Fake<IHolidayService>();
            A
                .CallTo(() => holidayService.HolidayList(dateFrom, dateTo))
                .Returns(Task.FromResult(holidays));

            var viewModel = CreateNew(holidayService);

            // When
            viewModel.DateFrom = dateFrom;
            viewModel.DateTo = dateTo;
            viewModel.Hours = 8;
            viewModel.Minutes = 0;
            viewModel.Comment = "URLOP";
            viewModel.SkipHolidays = true;

            var worklogs = await viewModel.Provide();

            // Then
            var expected = new[]
            {
                new Worklog(new DateTime(2017, 02, 03), TimeSpan.FromHours(8), "URLOP", "URLOP"),
                new Worklog(new DateTime(2017, 02, 06), TimeSpan.FromHours(8), "URLOP", "URLOP"),
                new Worklog(new DateTime(2017, 02, 07), TimeSpan.FromHours(8), "URLOP", "URLOP")
            };
            Assert.That(worklogs, Is.EquivalentTo(expected));
        }

        public static Func<RepeatableTimeTrackViewModel> CreateFactory(
            IHolidayService holidayService = null,
            IDateTimeProvider dateTimeProvider = null)
        {
            return () => CreateNew(holidayService, dateTimeProvider);
        }

        private static RepeatableTimeTrackViewModel CreateNew(
            IHolidayService holidayService = null,
            IDateTimeProvider dateTimeProvider = null)
        {
            holidayService = holidayService ?? A.Fake<IHolidayService>();
            dateTimeProvider = dateTimeProvider ?? A.Fake<IDateTimeProvider>();

            return new RepeatableTimeTrackViewModel(
                holidayService,
                dateTimeProvider);
        }
    }
}