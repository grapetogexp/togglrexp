﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TogglrExp.Model;
using TogglrExp.Model.Clipboard;
using TogglrExp.Tests.ServiceSubstitutes;
using TogglrExp.ViewModel.Timetrack;

namespace TogglrExp.Tests.ViewModel.Timetrack
{
    [TestFixture]
    public class WorklogPreviewViewModelTest
    {
        [Test]
        public void CreationDoesNotFail()
        {
            // Given
            // When / Then
            Assert.DoesNotThrow(() => CreateNew());
        }

        [Test]
        public void ExpectedAmountOfItemsIsDisplayed()
        {
            // Given
            var items = new[]
            {
                new Worklog(new DateTime(2017, 05, 14), TimeSpan.FromHours(4), "Item 1", "Item 1"),
                new Worklog(new DateTime(2017, 05, 15), TimeSpan.FromHours(4), "Item 2", "Item 2"),
                new Worklog(new DateTime(2017, 05, 16), TimeSpan.FromHours(4), "Item 3", "Item 3"),
            };

            // When
            var viewmodel = CreateNew(items);

            // Then
            Assert.That(viewmodel.Items, Has.Exactly(3).Items);
        }

        [Test]
        public void ContentOfItemIsCopied()
        {
            // Given
            const string itemsComment = "Comment";

            var items = new[]
            {
                new Worklog(new DateTime(2017,05,16), TimeSpan.FromHours(4), itemsComment, itemsComment),
            };

            var clipboard = new ClipboardSubstitute();
            var viewmodel = CreateNew(items, clipboard: clipboard);

            // When
            viewmodel.Items.First().CopyToClipboardCommand.Execute(null);

            // Then
            Assert.That(clipboard.Content, Is.EqualTo(itemsComment));
        }

        public static Func<IEnumerable<Worklog>, WorklogPreviewViewModel> CreateFactory(
            IClipboard clipboard = null)
        {
            return worklogs => CreateNew(worklogs, clipboard);
        }

        private static WorklogPreviewViewModel CreateNew(
            IEnumerable<Worklog> worklogs = null,
            IClipboard clipboard = null)
        {
            worklogs = worklogs ?? Enumerable.Empty<Worklog>();
            clipboard = clipboard ?? new ClipboardSubstitute();

            return new WorklogPreviewViewModel(
                worklogs,
                clipboard);
        }
    }
}