﻿using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using TogglrExp.Model;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Jira.Credentials;
using TogglrExp.Model.Secrets;
using TogglrExp.Tests.ServiceSubstitutes;
using TogglrExp.ViewModel.JiraCredntials;
using TogglrExp.ViewModel.NotificationCenter;

namespace TogglrExp.Tests.ViewModel.JiraCredntials
{
    [TestFixture]
    public class PasswordSourceTest
    {
        private static readonly JiraCredentials ValidCredentials = new JiraCredentials("validUsername", "validPassword");
        private static readonly JiraCredentials InvalidCredentials = new JiraCredentials("invalidUserName", "invalidPassword");
        private const int ConfigurationId = 1;

        [Test]
        public void IfNoCredentialsAreStored_NotificationAppears()
        {
            // Given
            var environment = CreateEnvironment(Option<JiraCredentials>.None);

            var jiraCredentialsProvider = environment.JiraCredentialsProvider;

            var notificationCenter = environment.NotificationCenter;

            // When

            var task = jiraCredentialsProvider.Provide(
                new JiraCredentialRequest(ConfigurationId),
                OnlyValidCredentialsAreValid,
                CancellationToken.None);
            
            // Then
            Assert.That(notificationCenter.Notifications, Has.Exactly(1).Items);
            var notification = notificationCenter.Notifications[0];
            Assert.That(notification, Is.TypeOf<PasswordNotificationViewModel>());
            var jiraNotification = (PasswordNotificationViewModel)notification;
            Assert.That(jiraNotification.HasFailed, Is.False);
            Assert.That(jiraNotification.Username, Is.Null);
            Assert.That(jiraNotification.Password, Is.Null);
        }

        [Test]
        public void IfStoredCredentialsAreInvalid_ItIsDisplayedInNotification()
        {
            // Given
            var environment = CreateEnvironment(Option.Some(InvalidCredentials));

            var jiraCredentialsProvider = environment.JiraCredentialsProvider;

            var notificationCenter = environment.NotificationCenter;

            // When

            var task = jiraCredentialsProvider.Provide(
                new JiraCredentialRequest(ConfigurationId),
                OnlyValidCredentialsAreValid,
                CancellationToken.None);

            // Then
            var notification = notificationCenter.Notifications[0];
            var jiraNotification = (PasswordNotificationViewModel)notification;
            Assert.That(jiraNotification.HasFailed, Is.False);
            Assert.That(jiraNotification.Username, Is.EqualTo(InvalidCredentials.Username));
            Assert.That(jiraNotification.Password, Is.EqualTo(InvalidCredentials.Password));
        }

        [Ignore("Issue TE-46 already reported")]
        [Test]
        public void IfInvalidCredentialsAreProvidedByDialog_SecondDialogContainingErrorAppears()
        {
            // Given
            var environment = CreateEnvironment(Option<JiraCredentials>.None);

            var jiraCredentialsProvider = environment.JiraCredentialsProvider;

            var notificationCenter = environment.NotificationCenter;

            var task = jiraCredentialsProvider.Provide(
                new JiraCredentialRequest(ConfigurationId),
                OnlyValidCredentialsAreValid,
                CancellationToken.None);

            // When
            var notification = notificationCenter.Notifications[0];
            var jiraNotification = (PasswordNotificationViewModel)notification;
            jiraNotification.Username = InvalidCredentials.Username;
            jiraNotification.Password = InvalidCredentials.Password;
            jiraNotification.DoneCommand.Execute(null);

            // Then
            Assert.That(notificationCenter.Notifications, Has.Count.EqualTo(1));
            var secondNotification = notificationCenter.Notifications[0];
            Assert.That(secondNotification, Is.TypeOf<PasswordNotificationViewModel>());
            var secondJiraNotification = (PasswordNotificationViewModel)secondNotification;
            Assert.That(secondJiraNotification.HasFailed, Is.True);
            Assert.That(secondJiraNotification.Username, Is.EqualTo(InvalidCredentials.Username));
            Assert.That(secondJiraNotification.Password, Is.EqualTo(InvalidCredentials.Password));
        }

        [Test]
        public void IfDialogIsDismissed_NoneIsProvided()
        {
            // Given
            var environment = CreateEnvironment(Option<JiraCredentials>.None);

            var jiraCredentialsProvider = environment.JiraCredentialsProvider;

            var notificationCenter = environment.NotificationCenter;

            var task = jiraCredentialsProvider.Provide(
                new JiraCredentialRequest(ConfigurationId),
                OnlyValidCredentialsAreValid,
                CancellationToken.None);

            // When
            var notification = notificationCenter.Notifications[0];
            var jiraNotification = (PasswordNotificationViewModel)notification;
            jiraNotification.DismissCommand.Execute(null);

            // Then
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.Result, Is.EqualTo(Option<JiraCredentials>.None));
        }

        [Test]
        public void IfRequestIsCancelled_DialogDisappears()
        {
            // Given
            var environment = CreateEnvironment(Option<JiraCredentials>.None);

            var jiraCredentialsProvider = environment.JiraCredentialsProvider;

            var notificationCenter = environment.NotificationCenter;

            var cancelationTokenSource = new CancellationTokenSource();

            var task = jiraCredentialsProvider.Provide(
                new JiraCredentialRequest(ConfigurationId),
                OnlyValidCredentialsAreValid,
                cancelationTokenSource.Token);

            // When
            cancelationTokenSource.Cancel();

            // Then
            Assert.That(notificationCenter.Notifications, Has.Exactly(0).Items);
        }

        [Test]
        public void IfValidCredentialsAreStored_ItIsReturnedImediately()
        {
            // Given
            var environment = CreateEnvironment(Option.Some(ValidCredentials));

            var provider = environment.JiraCredentialsProvider;

            // When
            var task = provider.Provide(
                new JiraCredentialRequest(ConfigurationId),
                OnlyValidCredentialsAreValid,
                CancellationToken.None);

            // Then
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.Result, Is.EqualTo(Option.Some(ValidCredentials)));
        }

        [Test]
        public void IfValidCredentialsAreProvidedByDialog_ItIsReturned()
        {
            // Given
            var environment = CreateEnvironment(Option<JiraCredentials>.None);

            var jiraCredentialsProvider = environment.JiraCredentialsProvider;

            var notificationCenter = environment.NotificationCenter;

            var task = jiraCredentialsProvider.Provide(
                new JiraCredentialRequest(ConfigurationId),
                OnlyValidCredentialsAreValid,
                CancellationToken.None);

            // When
            var notification = notificationCenter.Notifications[0];
            var jiraNotification = (PasswordNotificationViewModel)notification;
            jiraNotification.Username = ValidCredentials.Username;
            jiraNotification.Password = ValidCredentials.Password;
            jiraNotification.DoneCommand.Execute(null);

            // Then
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.Result, Is.EqualTo(Option.Some(ValidCredentials)));
        }

        private class TestEnvironment
        {
            public TestEnvironment(
                NotificationCenterViewModel notificationCenter,
                AsyncOptionalProvider<JiraCredentialRequest, JiraCredentials> jiraCredentialsProvider)
            {
                NotificationCenter = notificationCenter;
                JiraCredentialsProvider = jiraCredentialsProvider;
            }

            public NotificationCenterViewModel NotificationCenter { get; private set; }

            public AsyncOptionalProvider<JiraCredentialRequest, JiraCredentials> JiraCredentialsProvider { get; private set; }
        }

        private static TestEnvironment CreateEnvironment(Option<JiraCredentials> savedCredentials)
        {
            var protector = new Protector();
            var jiraRepository = new JiraConfigurationRepositorySubstitute();

            if (!savedCredentials.IsNone)
            {
                var creds = savedCredentials.Value;
                var credsDto = new CreadentialsStoreModel
                {
                    Username = creds.Username,
                    Password = creds.Password
                };
                var data = protector.Protect(credsDto);
                jiraRepository.UpdateCredentials(ConfigurationId, data);
            }

            var passwordSource = new PasswordSource();

            var storedProvider = new JiraCredentialsProvider(
                passwordSource,
                jiraRepository,
                protector);

            var jiraCredentialsProvider = new AsyncOptionalProvider<JiraCredentialRequest, JiraCredentials>(storedProvider);

            var notificationCenter = new NotificationCenterViewModel(new[] { passwordSource });

            return new TestEnvironment(notificationCenter, jiraCredentialsProvider);
        }

        private static Task<bool> OnlyValidCredentialsAreValid(JiraCredentials credentials)
        {
            return Task.FromResult(credentials.Username == ValidCredentials.Username &&
                                   credentials.Password == ValidCredentials.Password);
        }
    }
}