﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model.Clipboard;
using TogglrExp.Model.Jira;
using TogglrExp.Model.OvertimeReport;
using TogglrExp.Model.Settings;
using TogglrExp.Model.TimeProvider;
using TogglrExp.Tests.ServiceSubstitutes;
using TogglrExp.ViewModel;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;
using TogglrExp.ViewModel.OvertimeReport;

namespace TogglrExp.Tests.ViewModel.OvertimeReport
{
    [TestFixture]
    public class OvertimeReportViewModelTest
    {
        [Test]
        public void ViewModelCreationDoesNotFail()
        {
            // Given

            // When
            CreateNew();

            // Then
            Assert.Pass();
        }

        [Test]
        public void CurrentMonthIsSetInitialy()
        {
            // Given
            var dateTimeProvider = A.Fake<IDateTimeProvider>();
            A
                .CallTo(() => dateTimeProvider.Today)
                .Returns(new DateTime(2016, 02, 24));
            
            // When
            var viewModel = CreateNew(dateTimeProvider: dateTimeProvider);

            // Then
            Assert.That(viewModel.DateFrom, Is.EqualTo(new DateTime(2016, 02, 01)));
            Assert.That(viewModel.DateTo, Is.EqualTo(new DateTime(2016, 02, 24)));
        }

        [Test]
        public async Task CopiedValueIsAsExpected()
        {
            // Given

            var clipboard = new ClipboardSubstitute();
            
            var overtimeReport = new TogglrExp.Model.OvertimeReport.OvertimeReport(
                new []
                {
                    new OvertimeItem(new DateTime(2016, 1, 3), new TimeSpan(2,15,0)),
                    new OvertimeItem(new DateTime(2016, 1, 4), new TimeSpan(3,30,0)),
                }, new TimeSpan(4, 45, 0));

            var overtimeReportService = A.Fake<IOvertimeReportService>();
            A
                .CallTo(() => overtimeReportService.Get(A<DateTime>._, A<DateTime>._, A<TimeSpan>._, A<JiraIssueModel>._, A<CancellationToken>._))
                .Returns(overtimeReport);

            var viewModel = CreateNew(overtimeReportService, clipboard);
            await viewModel.RefreshTaskAsync();

            // When
            viewModel.CopyCommand.Execute(null);

            // Then
            const string expected = 
                "2016-01-03\t2:15\r\n" +
                "2016-01-04\t3:30\r\n" + 
                "Razem:4:45";

            Assert.That(clipboard.Content, Is.EqualTo(expected));
        }

        [Test]
        public async Task MoreThan24HoursTotalIsProperlyFormated()
        {
            // Given

            var clipboard = new ClipboardSubstitute();

            var overtimeReport = new TogglrExp.Model.OvertimeReport.OvertimeReport(
                new[]
                {
                    new OvertimeItem(new DateTime(2016, 1, 4), new TimeSpan(4,15,0)),
                    new OvertimeItem(new DateTime(2016, 1, 5), new TimeSpan(3,30,0)),
                    new OvertimeItem(new DateTime(2016, 1, 6), new TimeSpan(2,30,0)),
                    new OvertimeItem(new DateTime(2016, 1, 7), new TimeSpan(4,45,0)),
                    new OvertimeItem(new DateTime(2016, 1, 8), new TimeSpan(3,30,0)),
                    new OvertimeItem(new DateTime(2016, 1, 9), new TimeSpan(10,30,0)),
                }, new TimeSpan(1, 4, 0, 0));

            var overtimeReportService = A.Fake<IOvertimeReportService>();
            A
                .CallTo(() => overtimeReportService.Get(A<DateTime>._, A<DateTime>._, A<TimeSpan>._, A<JiraIssueModel>._, A<CancellationToken>._))
                .Returns(overtimeReport);

            var viewModel = CreateNew(overtimeReportService, clipboard);
            await viewModel.RefreshTaskAsync();

            // When
            viewModel.CopyCommand.Execute(null);

            // Then
            const string expected = 
                "2016-01-04\t4:15\r\n" +
                "2016-01-05\t3:30\r\n" +
                "2016-01-06\t2:30\r\n" +
                "2016-01-07\t4:45\r\n" +
                "2016-01-08\t3:30\r\n" +
                "2016-01-09\t10:30\r\n" +
                "Razem:28:00";

            Assert.That(clipboard.Content, Is.EqualTo(expected));
        }

        public static Func<OvertimeReportViewModel> CreateFactory(
            IOvertimeReportService overtimeReportService = null,
            IClipboard clipboard = null,
            IJiraService jiraService = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            IDateTimeProvider dateTimeProvider = null,
            Func<JiraIssueListViewModel> jiraIssueListFactory = null)
        {
            return () => CreateNew(
                overtimeReportService,
                clipboard,
                jiraService,
                navigationService,
                businessManager,
                dateTimeProvider,
                jiraIssueListFactory);
        }

        private static OvertimeReportViewModel CreateNew(
            IOvertimeReportService overtimeReportService = null,
            IClipboard clipboard = null,
            IJiraService jiraService = null,
            INavigationService navigationService = null,
            IBusinessManager businessManager = null,
            IDateTimeProvider dateTimeProvider = null,
            Func<JiraIssueListViewModel> jiraIssueListFactory = null)
        {
            overtimeReportService = overtimeReportService ?? A.Fake<IOvertimeReportService>();
            clipboard = clipboard ?? A.Fake<IClipboard>();
            
            if (jiraService == null)
            {
                jiraService = A.Fake<IJiraService>();
                Func<int, CancellationToken, Task<IEnumerable<JiraIssueModel>>> itemFactory =
                    (configurationId, __) => Task.FromResult<IEnumerable<JiraIssueModel>>(new[]
                        {new JiraIssueModel(configurationId, "SOME-123", "Some issue")});

                A
                    .CallTo(() => jiraService.GetIssues(A<int>._, A<string>._, A<CancellationToken>._))
                    .ReturnsLazily(itemFactory);
            }

            navigationService = navigationService ?? A.Fake<INavigationService>();
            businessManager = businessManager ?? A.Fake<IBusinessManager>();
            dateTimeProvider = dateTimeProvider ?? A.Fake<IDateTimeProvider>();

            jiraIssueListFactory = jiraIssueListFactory ?? JiraIssueListViewModelTest.CreateFactory(jiraService);

            return new OvertimeReportViewModel(
                overtimeReportService,
                clipboard,
                businessManager,
                navigationService,
                dateTimeProvider,
                jiraIssueListFactory);
        }
    }
}
