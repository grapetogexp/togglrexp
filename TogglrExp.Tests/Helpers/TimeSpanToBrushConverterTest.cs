﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Media;
using NUnit.Framework;
using TogglrExp.Helpers;

namespace TogglrExp.Tests.Helpers
{
    [TestFixture]
    public class TimeSpanToBrushConverterTest
    {
        [Test, TestCaseSource("ColorTestCases")]
        public Color Convert(TimeSpan input)
        {
            // Given
            var converter = new TimeSpanToBrushConverter();
            // When

            var result = converter.Convert(input, typeof (Brush), null, CultureInfo.InvariantCulture);

            // Then
            return ((SolidColorBrush) result).Color;
        }

        public static IEnumerable ColorTestCases
        {
            get
            {
                yield return new TestCaseData(TimeSpan.FromHours(-10)).Returns(Colors.Red);
                yield return new TestCaseData(TimeSpan.FromHours(10)).Returns(Colors.LawnGreen);
            }
        }
    }
}