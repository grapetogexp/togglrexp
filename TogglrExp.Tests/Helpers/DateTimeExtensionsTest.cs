﻿using System;
using NUnit.Framework;
using TogglrExp.Helpers;

namespace TogglrExp.Tests.Helpers
{
    [TestFixture]
    public class DateTimeExtensionsTest
    {
        [Test]
        public void DaysRangeReturnsAllDaysInRange()
        {
            // Given
            DateTime startDate = new DateTime(2016, 1, 10);
            DateTime endDate = new DateTime(2016, 1, 14);

            var expected = new[]
            {
                startDate,
                new DateTime(2016, 1, 11),
                new DateTime(2016, 1, 12),
                new DateTime(2016, 1, 13),
                endDate,
            };

            // When
            var result = DateTimeExtesions.DaysRange(startDate, endDate);

            // Then
            Assert.That(result, Is.EquivalentTo(expected));
        }
    }
}