﻿using System;
using System.Collections;
using System.Globalization;
using NUnit.Framework;
using TogglrExp.Helpers;

namespace TogglrExp.Tests.Helpers
{
    [TestFixture]
    public class OverTimeSpanConverterTest
    {
        [Test, TestCaseSource("ConvertParameters")]
        public string Convert(TimeSpan input)
        {
            // Given
            var converter = new OverTimeSpanConverter();

            // When
            var result = converter.Convert(input, typeof (string), null, CultureInfo.InvariantCulture);

            // Then
            return (string) result;
        }

        public static IEnumerable ConvertParameters
        {
            get
            {
                yield return new TestCaseData(TimeSpan.FromMinutes(1)).Returns("0:01");
                yield return new TestCaseData(TimeSpan.FromMinutes(13)).Returns("0:13");
                yield return new TestCaseData(new TimeSpan(2, 13, 0)).Returns("2:13");
                yield return new TestCaseData(new TimeSpan(13, 0, 0)).Returns("13:00");
                yield return new TestCaseData(new TimeSpan(1, 10, 12, 0)).Returns("34:12");
                yield return new TestCaseData(-new TimeSpan(1, 10, 12, 0)).Returns("-34:12");
            }
        }
    }
}