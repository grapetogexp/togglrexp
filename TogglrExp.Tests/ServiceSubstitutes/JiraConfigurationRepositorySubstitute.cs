﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TogglrExp.Model.Jira.ConfigurationStorage;

namespace TogglrExp.Tests.ServiceSubstitutes
{
    public class JiraConfigurationRepositorySubstitute : IJiraConfigurationRepository
    {
        private readonly Dictionary<long, ConfigurationDTO> _configurationDtos;
        private readonly Dictionary<long, byte[]> _credentials;

        public JiraConfigurationRepositorySubstitute()
        {
            _configurationDtos = new Dictionary<long, ConfigurationDTO>();
            _credentials = new Dictionary<long, byte[]>();
        }

        public Task<IEnumerable<ConfigurationDTO>> GetAll()
        {
            return Task.FromResult<IEnumerable<ConfigurationDTO>>(_configurationDtos.Values);
        }

        public Task<ConfigurationDTO> Get(long id)
        {
            return Task.FromResult(_configurationDtos[id]);
        }

        public Task Insert(ConfigurationDTO configration)
        {
            var id = _configurationDtos.Keys.Max();
            id++;
            _configurationDtos[id] = configration;
            return Task.FromResult(false);
        }

        public Task Update(ConfigurationDTO configration)
        {
            _configurationDtos[configration.Id] = configration;
            return Task.FromResult(false);
        }

        public Task Delete(long id)
        {
            _configurationDtos.Remove(id);
            return Task.FromResult(false);
        }

        public Task<byte[]> GetCredentials(long id)
        {
            byte[] result;
            _credentials.TryGetValue(id, out result);
            return Task.FromResult(result);
        }

        public Task UpdateCredentials(long id, byte[] encryptedBytes)
        {
            _credentials[id] = encryptedBytes;
            return Task.FromResult(false);
        }
    }
}