﻿using System;
using TogglrExp.Model.Settings;

namespace TogglrExp.Tests.ServiceSubstitutes
{
    
    public class SettingsManagerSubstitute : ISettingsManager
    {
        private ModableSettings _settings;

        public SettingsManagerSubstitute()
        {
            _settings = new ModableSettings
            {
                UserToken = "ED725753-2D54-4CD5-A80D-E422856894F7",
                DescriptionExported = true,
                TimeFromExported = true,
                TimeToExported = true,
                DurationExported = true,
                JiraConfigurationId = null
            };
        }

        public void Modify(Action<ModableSettings> modifierAction)
        {
            modifierAction(_settings);
        }

        public Settings Get()
        {
            return new Settings(
                _settings.UserToken,
                _settings.DescriptionExported,
                _settings.TimeFromExported,
                _settings.TimeToExported,
                _settings.DurationExported,
                _settings.JiraConfigurationId);
        }
    }
}