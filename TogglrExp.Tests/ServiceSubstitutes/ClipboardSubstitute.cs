﻿using TogglrExp.Model.Clipboard;

namespace TogglrExp.Tests.ServiceSubstitutes
{
    public class ClipboardSubstitute : IClipboard
    {
        public string Content { get; private set; }

        public void Copy(string text)
        {
            Content = text;
        }
    }
}