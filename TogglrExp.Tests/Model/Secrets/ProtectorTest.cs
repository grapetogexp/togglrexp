﻿using NUnit.Framework;
using TogglrExp.Model.Secrets;

namespace TogglrExp.Tests.Model.Secrets
{
    [TestFixture]
    public class ProtectorTest
    {
        [Test]
        public void UnprotectedData_IsTheSameAsBeforeProtection()
        {
            // Given
            const string fieldValue = "BlaBla";
            var data = new Data {Field = fieldValue};

            var protector = new Protector();

            // When
            var protectedData = protector.Protect(data);
            var unprotected = protector.Unprotect<Data>(protectedData);

            // Then
            Assert.That(unprotected.Field, Is.EqualTo(fieldValue));
        }

        private class Data
        {
            public string Field { get; set; }
        }
    }
}