﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FakeItEasy;
using GalaSoft.MvvmLight.Messaging;
using NUnit.Framework;
using TogglrExp.Model.Holiday;
using RHoliday = TogglrExp.Model.Holiday.Holiday;

namespace TogglrExp.Tests.Model.Holiday
{
    [TestFixture]
    public class HolidayServiceTest
    {
        [Test]
        public async Task FirstCoupleOfWeekendsIn2016AreGeneratedProperly()
        {
            // Given
            var weekends = new List<RHoliday>();

            var holidayRepository = A.Fake<IHolidayRepository>();
            A
                .CallTo(() => holidayRepository.Insert(A<RHoliday>._))
                .Invokes((RHoliday holiday) => { weekends.Add(holiday); })
                .ReturnsLazily(() => Task.Delay(TimeSpan.FromMilliseconds(1)));

            
            var service = CreateNew(holidayRepository);

            // When
            await service.FillWeekends(2016);

            // Then
            var firstSix = weekends
                .OrderBy(o => o.Date)
                .Take(6)
                .Select(o => o.Date);
            Assert.That(firstSix, Is.EquivalentTo(new []
            {
                new DateTime(2016, 01, 02),
                new DateTime(2016, 01, 03),
                new DateTime(2016, 01, 09),
                new DateTime(2016, 01, 10),
                new DateTime(2016, 01, 16),
                new DateTime(2016, 01, 17),
            }));
        }

        private static HolidayService CreateNew(
            IHolidayRepository holidayRepository = null,
            IMessenger messenger = null)
        {
            holidayRepository = holidayRepository ?? A.Fake<IHolidayRepository>();
            messenger = messenger ?? A.Fake<IMessenger>();

            return new HolidayService(holidayRepository, messenger);
        }
    }
}