﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model;
using TogglrExp.Model.Holiday;
using TogglrExp.Model.Jira;
using TogglrExp.Model.OvertimeReport;
using RHoliday = TogglrExp.Model.Holiday.Holiday;

namespace TogglrExp.Tests.Model.OvertimeReport
{
    [TestFixture]
    public class OvertimeReportServiceTest
    {
        [Test]
        public async Task WorkDayAmountOfTimeIsReducedByNorm()
        {
            // Given
            DateTime reportDate = new DateTime(2016, 10, 1);
            TimeSpan norm = TimeSpan.FromHours(8);
            TimeSpan amountOfWork = TimeSpan.FromHours(10);
            JiraIssueModel issue = new JiraIssueModel(1, "some key", "some description");

            var workLogService = A.Fake<IJiraService>();
            A
                .CallTo(() => workLogService.Get(reportDate, reportDate, issue, A<CancellationToken>._))
                .Returns(new List<Worklog>
                {
                    new Worklog(reportDate, amountOfWork, "Lorem ipsum", "Lorem ipsum")
                });

            var service = CreateNew(workLogService);

            // When
            var result = await service.Get(reportDate, reportDate, norm, issue, CancellationToken.None);

            // Then
            var single = result.Items.Single();
            Assert.That(single.Overflow, Is.EqualTo(TimeSpan.FromHours(2)));
        }

        [Test]
        public async Task HolidayAmountOfWorkIsPassesThrought()
        {
            // Given
            DateTime reportDate = new DateTime(2016, 10, 1);
            TimeSpan norm = TimeSpan.FromHours(8);
            TimeSpan amountOfWork = TimeSpan.FromHours(10);
            JiraIssueModel someIssue = new JiraIssueModel(1, "SOME-1", "Some issue");

            var workLogService = A.Fake<IJiraService>();
            A
                .CallTo(() => workLogService.Get(reportDate, reportDate, someIssue, A<CancellationToken>._))
                .Returns(new List<Worklog>
                {
                    new Worklog(reportDate, amountOfWork, "Lorem ipsum", "Lorem ipsum")
                });

            var holidayService = A.Fake<IHolidayService>();
            A
                .CallTo(() => holidayService.HolidayList(reportDate, reportDate))
                .Returns(new List<RHoliday>
                {
                    new RHoliday(reportDate, "Lorem ipsum")
                });

            var service = CreateNew(workLogService, holidayService);

            // When
            var result = await service.Get(reportDate, reportDate, norm, someIssue, CancellationToken.None);

            // Then
            var single = result.Items.Single();
            Assert.That(single.Overflow, Is.EqualTo(TimeSpan.FromHours(10)));
        }

        [Test]
        public async Task IfNoTimeHaveBeenLoggedDurringWorkDayNegativeNormIsIncluded()
        {
            // Given
            DateTime reportDate = new DateTime(2016, 10, 1);
            TimeSpan norm = TimeSpan.FromHours(8);
            JiraIssueModel someIssue = new JiraIssueModel(1, "SOME-1", "Some issue");

            var service = CreateNew();

            // When
            var result = await service.Get(reportDate, reportDate, norm, someIssue, CancellationToken.None);

            // Then
            var single = result.Items.Single();
            Assert.That(single.Overflow, Is.EqualTo(TimeSpan.FromHours(-8)));
        }

        private static OvertimeReportService CreateNew(
            IJiraService jiraService = null,
            IHolidayService holidayService = null)
        {
            jiraService = jiraService ?? A.Fake<IJiraService>();
            holidayService = holidayService ?? A.Fake<IHolidayService>();

            return new OvertimeReportService(jiraService, holidayService);
        }
    }
}