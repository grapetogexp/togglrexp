﻿using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using TogglrExp.Model;
using TogglrExp.Model.AsyncOptionalProvider;

namespace TogglrExp.Tests.Model.AsyncOptionalProvider
{
    [TestFixture]
    public class AsyncOptionalProviderTest
    {
        private const string noMatter = "82927225-6734-4DBA-95E8-4E8537F3D1DC";

        [Test]
        public async Task IfProviderReturnsValidPassword_ItIsReturned()
        {
            // Given
            const string somePassword = "s0m3P@ssw0rd";

            var source = A.Fake<IAsyncOptionalSampleProvider<string, string>>();

            A
                .CallTo(() => source.Provide(noMatter, A<int>._, A<CancellationToken>._))
                .Returns(Option.Some(somePassword));

            var provider = new AsyncOptionalProvider<string, string>(source);

            // when
            var result = await provider.Provide(noMatter, s => Task.FromResult(true));

            // then
            Assert.That(result.IsNone, Is.False);
            Assert.That(result.Value, Is.EqualTo(somePassword));
        }

        [Test]
        public async Task IfProviderReturnsInvalidPassword_ItIsAskedAgain()
        {
            // Given
            const string invalidPassword = "1nv@l1dP@ssw0rd";
            const string validPassword = "v@l1dP@ssw0rd";

            var source = A.Fake<IAsyncOptionalSampleProvider<string, string>>();

            A
                .CallTo(() => source.Provide(noMatter, 0, A<CancellationToken>._))
                .Returns(Option.Some(invalidPassword));

            A
                .CallTo(() => source.Provide(noMatter, 1, A<CancellationToken>._))
                .Returns(Option.Some(validPassword));

            var provider = new AsyncOptionalProvider<string, string>(source);

            // When
            var result = await provider.Provide(noMatter, s => Task.FromResult(s == validPassword));

            // Then
            A
                .CallTo(() => source.Provide(noMatter, 1, A<CancellationToken>._))
                .MustHaveHappened();

            Assert.That(result.IsNone, Is.False);
            Assert.That(result.Value, Is.EqualTo(validPassword));
        }
        
        [Test]
        public async Task IfProvidersDontProvidePassword_NoneIsReturned()
        {
            // Given
            var source = A.Fake<IAsyncOptionalSampleProvider<string, string>>();

            A
                .CallTo(() => source.Provide(noMatter, A<int>._, A<CancellationToken>._))
                .Returns(Option<string>.None);
            
            var provider = new AsyncOptionalProvider<string, string>(source);

            // When
            var result = await provider.Provide(noMatter, p => Task.FromResult(false));

            // Then
            Assert.That(result.IsNone, Is.True);
        }

        [Test]
        public async Task OnceCancelledSourceProvidesNone()
        {
            // Given
            const string somePassword = "s0m3P@ssw0rd";

            var source = A.Fake<IAsyncOptionalSampleProvider<string, string>>();

            A
                .CallTo(() => source.Provide(noMatter, A<int>._, A<CancellationToken>._))
                // Task.Run matters in here
                .ReturnsLazily(() => Task.Run(() => Option.Some(somePassword)));

            var provider = new AsyncOptionalProvider<string, string>(source);

            // when
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            var task = provider.Provide(noMatter, s => Task.FromResult(false), cancellationTokenSource.Token);
            cancellationTokenSource.Cancel();
            var result = await task;

            // then
            Assert.That(result.IsNone, Is.True);
        }
    }
}