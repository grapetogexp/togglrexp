﻿$ErrorActionPreference = "Stop"

function Get-ScriptDirectory
{
  #$Invocation = (Get-Variable MyInvocation -Scope 1).Value
  #Split-Path $Invocation.MyCommand.Path
  Split-Path -parent $PSCommandPath
}

function Replace-AssemblyInfo ($version)
{
    $scriptDirectory = Get-ScriptDirectory
    
    $assemblyInfoPath = [System.IO.Path]::Combine($scriptDirectory, "TogglrExp\Properties\AssemblyInfo.cs")
    $assemblyInfoText = [System.IO.File]::ReadAllText($assemblyInfoPath)

    $assemblyInfoText = [System.Text.RegularExpressions.Regex]::Replace($assemblyInfoText, "(\[assembly: AssemblyVersion\(`")\d+\.\d+\.\d+(\.0`"\)\])",  "`${1}"+$version+"`${2}")
    $assemblyInfoText = [System.Text.RegularExpressions.Regex]::Replace($assemblyInfoText, "(\[assembly: AssemblyFileVersion\(`")\d+\.\d+\.\d+(\.0`"\)\])", "`${1}"+$version+"`${2}")

    [System.IO.File]::WriteAllText($assemblyInfoPath, $assemblyInfoText)
}

function Replace-Product-Wxs ($version)
{
    $scriptDirectory = Get-ScriptDirectory

    $wsxPath = [System.IO.Path]::Combine($scriptDirectory, "Setup\Product.wxs")
    $wsxText = [System.IO.File]::ReadAllText($wsxPath)
    
    $wsxText = [System.Text.RegularExpressions.Regex]::Replace($wsxText, "($\s+Version=`")\d+\.\d+\.\d+(`"\s*)",  "`${1}"+$version+"`${2}")
    [System.IO.File]::WriteAllText($wsxPath, $wsxText)
}

function Replace-Bundle-Wxs  ($version){
    $scriptDirectory = Get-ScriptDirectory

    $wsxPath = [System.IO.Path]::Combine($scriptDirectory, "Installer\Bundle.wxs")
    $wsxText = [System.IO.File]::ReadAllText($wsxPath)
    
    $wsxText = [System.Text.RegularExpressions.Regex]::Replace($wsxText, "($\s+Version=`")\d+\.\d+\.\d+(`"\s*)",  "`${1}"+$version+"`${2}")
    [System.IO.File]::WriteAllText($wsxPath, $wsxText)
}

function Build-Solution
{
    $msBuildKey = Get-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\MSBuild\ToolsVersions\14.0"
    $msBuildPath = [System.IO.Path]::Combine($msBuildKey.MSBuildToolsPath, "MSBuild.exe")

    & $msBuildPath TogglrExp\TogglrExp.sln /t:Rebuild /p:Configuration=Release
    if ($LASTEXITCODE -ne 0)
    {
        exit
    }
}

function Copy-To-Ftp ($version,$ftpUserName,$ftpPass) {    
    $ftpCredentials = New-Object System.Net.NetworkCredential($ftpUserName,$ftpPass) 
    
    $webclient = New-Object System.Net.WebClient 
    $webclient.Credentials = $ftpCredentials
    
    $scriptDirectory = Get-ScriptDirectory
    
    Write-Host "Uploading setup file"
    $directoryRequest = [System.Net.WebRequest]::Create("ftp://ftp.szogun-ui.pl/www/togglr-exp/download/$version")
    $directoryRequest.Method = [System.Net.WebRequestMethods+Ftp]::MakeDirectory
    $directoryRequest.Credentials = $ftpCredentials
    $resp = [System.Net.FtpWebResponse]($directoryRequest.GetResponse())
    $statusCode = $resp.StatusCode
    $resp.Close()
    
    if ($statusCode -ne [System.Net.FtpStatusCode]::PathnameCreated)
    {
        Write-Error "Unable to create taret directory: $statusCode"
        exit
    }
    
    $setupFilePath =  [System.IO.Path]::Combine($scriptDirectory, "Installer\bin\Release", "Installer.exe")
    $setupFileUri = New-Object System.Uri("ftp://ftp.szogun-ui.pl/www/togglr-exp/download/$version/Installer.exe")
    $webclient.UploadFile($setupFileUri, $setupFilePath)
    
    Write-Host "Uploading release notes"
    $releaseNotesPath = [System.IO.Path]::Combine($scriptDirectory, "web-content\index.html")
    $releaseNoteUri = New-Object System.Uri("ftp://ftp.szogun-ui.pl/www/togglr-exp/index.html")
    $webclient.UploadFile($releaseNoteUri, $releaseNotesPath)
    
    Write-Host "Uploading version.json"
    $versionJsonPath = [System.IO.Path]::Combine($scriptDirectory, "web-content\version.json")
    @{ version= $version; message="Wersja $version dostępna"; location="http://szogun-ui.pl/togglr-exp/download/$version/Installer.exe" } | ConvertTo-Json -Compress > $versionJsonPath
    
    $versionJsonUri = New-Object System.Uri("ftp://ftp.szogun-ui.pl/www/togglr-exp/version.json")
    $webclient.UploadFile($versionJsonUri, $versionJsonPath)
}

$Version = Read-Host -Prompt "Target version"

$ReleseNotesUpdated = Read-Host "Are release notes updated (y/n)?"
if ($ReleseNotesUpdated -ne "y")
{
    exit
}

$FtpUserName = Read-Host -Prompt "FTP User name"
$FtpPass = Read-Host -Prompt "FTP Password" -AsSecureString

Replace-AssemblyInfo $Version
Replace-Product-Wxs $Version
Replace-Bundle-Wxs $Version
Build-Solution
Copy-To-Ftp $Version $FtpUserName $FtpPass