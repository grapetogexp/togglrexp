﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Threading;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using TogglrExp.ViewModel;
using Tracer.Log4Net;

namespace TogglrExp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        public App()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            path = Path.Combine(path, "TogglrExp", "Logs.log");

            var patternLayout = new PatternLayout
            {
                Header = "---------------------------------------------------------------------------------------%n-- Service (pid=%processid) started at %d %n -- %property %newline---------------------------------------------------------------------------------------%n",
                Footer = "----------------- Loggin Ended at %d  ---------------------%n",
                ConversionPattern = "%d [%-5p][%t] %C{1}.%M: %m === %P{trace} %P{arguments} %P{timeTaken} %n"
            };
            patternLayout.ActivateOptions();

            var appender = new RollingFileAppender
            {
                AppendToFile = true,
                RollingStyle = RollingFileAppender.RollingMode.Size,
                MaximumFileSize = "2MB",
                File = path,
                MaxSizeRollBackups = 10,
                Layout = patternLayout
            };
            appender.ActivateOptions();

            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
            hierarchy.Root.RemoveAllAppenders();

            hierarchy.Root.Level = Level.All;
            hierarchy.Root.AddAppender(appender);

            log4net.Config.BasicConfigurator.Configure(hierarchy);

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            DispatcherUnhandledException += OnDispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;

            DispatcherHelper.Initialize();
        }

        private void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs args)
        {
            Log.Error("Exception have been globally handled", args.Exception);
        }

        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs args)
        {
            Log.Error("Exception have been globally handled", args.Exception);
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Log.Error("Exception have been globally handled", args.ExceptionObject as Exception);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            var locator = (ViewModelLocator) Resources["Locator"];
            locator.Dispose();
        }
    }
}
