﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TogglrExp.Helpers
{
    [ValueConversion(typeof(TimeSpan), typeof(string))]
    public class OverTimeSpanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is TimeSpan))
            {
                return null;
            }

            TimeSpan input = (TimeSpan)value;
            return OvertimeTimeSpanFormatHelper.Format(input);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}