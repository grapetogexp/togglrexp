﻿using System;
using System.Collections.Generic;

namespace TogglrExp.Helpers
{
    public static class DateTimeExtesions
    {
        public static IEnumerable<DateTime> DaysRange(DateTime startDate, DateTime endDate)
        {
            return Range(startDate, endDate, TimeSpan.FromDays(1));
        }

        public static IEnumerable<DateTime> Range(DateTime startDateTime, DateTime endDateTime, TimeSpan step)
        {
            DateTime current = startDateTime;

            while (current <= endDateTime)
            {
                yield return current;
                current += step;
            }
        } 
    }
}