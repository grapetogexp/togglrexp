﻿using System;
using Ninject;
using Ninject.Parameters;
using Ninject.Syntax;

namespace TogglrExp.Helpers
{
    public static class NinjectKernelExtensions
    {
        public static IBindingToSyntax<Func<T>> BindFactory<T>(this IKernel kernel)
        {
            return kernel.Bind<Func<T>>();
        }

        public static IBindingWhenInNamedWithOrOnSyntax<Func<T>> ToFactory<T>(this IBindingToSyntax<Func<T>> bindingToSyntax)
        {
            return bindingToSyntax
                .ToMethod(c => () => c.Kernel.Get<T>());
        }

        public static IBindingToSyntax<Func<TArg, TResult>> BindFactory<TArg, TResult>(this IKernel kernel)
        {
            return kernel.Bind<Func<TArg, TResult>>();
        }

        public static IBindingWhenInNamedWithOrOnSyntax<Func<TArg, TResult>> ToFactory<TArg, TResult>(this IBindingToSyntax<Func<TArg, TResult>> bindingToSyntax, string argumentName)
        {
            return bindingToSyntax
                .ToMethod(c => arg => c.Kernel.Get<TResult>(new ConstructorArgument(argumentName, arg)));
        }
    }
}