﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace TogglrExp.Helpers
{
    public class PasswordBoxBehavior : Behavior<PasswordBox>
    {
        public static readonly DependencyProperty TextPasswordProperty = DependencyProperty.Register(
            "TextPassword",
            typeof(string),
            typeof(PasswordBoxBehavior),
            new PropertyMetadata("", OnTextPasswordChanged));

        public string TextPassword
        {
            get { return Convert.ToString(GetValue(TextPasswordProperty)); }
            set { SetValue(TextPasswordProperty, value); }
        }

        private static void OnTextPasswordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pwBoxBexaviour = d as PasswordBoxBehavior;
            if (pwBoxBexaviour != null && pwBoxBexaviour.AssociatedObject != null)
            {
                if (pwBoxBexaviour.AssociatedObject.Password != pwBoxBexaviour.TextPassword)
                {
                    pwBoxBexaviour.AssociatedObject.Password = pwBoxBexaviour.TextPassword;
                }
            }
        }

        protected override void OnAttached()
        {
            AssociatedObject.PasswordChanged += AssociatedObject_PasswordChanged;
            AssociatedObject.Password = TextPassword;
            object o = AssociatedObject.TemplatedParent;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PasswordChanged += AssociatedObject_PasswordChanged;
            base.OnDetaching();
        }

        private void AssociatedObject_PasswordChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            object o = AssociatedObject.TemplatedParent;
            if (TextPassword != AssociatedObject.Password)
            {
                TextPassword = AssociatedObject.Password;
            }
        }
    }
}