﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace TogglrExp.Helpers
{
    public class TimeSpanToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TimeSpan)
            {
                TimeSpan span = (TimeSpan) value;
                return span < TimeSpan.Zero ? Brushes.Red : Brushes.LawnGreen;
            }
            return Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}