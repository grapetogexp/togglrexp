﻿using System;

namespace TogglrExp.Helpers
{
    public static class OvertimeTimeSpanFormatHelper
    {
        public static string Format(TimeSpan input)
        {
            if (Math.Abs(input.TotalDays) > 1)
            {
                int hours = (int)input.TotalHours;
                int minutes = Math.Abs(input.Minutes);
                return string.Format("{0:##0}:{1:00}", hours, minutes);
            }
            return input.ToString("h\\:mm");
        }
    }
}