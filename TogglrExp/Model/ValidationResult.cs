﻿using System.Collections.Generic;
using System.Linq;

namespace TogglrExp.Model
{
    public class ValidationResult
    {
        public static ValidationResult Valid { get; private set; }

        static ValidationResult()
        {
            Valid = new ValidationResult(Enumerable.Empty<string>());
        }

        public ValidationResult(IEnumerable<string> errorMessages)
        {
            var arr = errorMessages.ToArray();
            ErrorMessages = arr;
            IsValid = arr.Length == 0;
        }

        public bool IsValid { get; private set; }

        public IEnumerable<string> ErrorMessages { get; private set; }
    }
}