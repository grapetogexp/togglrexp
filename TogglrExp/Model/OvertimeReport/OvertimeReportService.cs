﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TogglrExp.Helpers;
using TogglrExp.Model.Holiday;
using TogglrExp.Model.Jira;
using TracerAttributes;

namespace TogglrExp.Model.OvertimeReport
{
    [TraceOn(TraceTarget.Public)]
    public class OvertimeReportService : IOvertimeReportService
    {
        private readonly IJiraService _jiraService;
        private readonly IHolidayService _holidayService;

        public OvertimeReportService(
            IJiraService jiraService,
            IHolidayService holidayService)
        {
            _jiraService = jiraService;
            _holidayService = holidayService;
        }

        public async Task<OvertimeReport> Get(DateTime dateFrom, DateTime dateTo, TimeSpan norm, JiraIssueModel issue, CancellationToken cancellationToken)
        {
            var dateRange = DateTimeExtesions.DaysRange(dateFrom, dateTo);

            var holidaysList = await _holidayService.HolidayList(dateFrom, dateTo);
            
            var holidaysSet = new HashSet<DateTime>(holidaysList.Select(o => o.Date));

            var workLogs = await _jiraService.Get(dateFrom, dateTo, issue, cancellationToken);

            var items = (
                from day in dateRange
                join workLog in workLogs on day equals workLog.Date.Date into workLogsSet
                from wl in workLogsSet.DefaultIfEmpty()
                let isHoliday = holidaysSet.Contains(day)
                let timeSpent = (wl == null) ? TimeSpan.Zero : wl.TimeSpent 
                let overFlow = isHoliday ? timeSpent : (timeSpent - norm)
                where overFlow != TimeSpan.Zero
                select new OvertimeItem(day, overFlow)).ToList();

            var sum = TimeSpan.FromMilliseconds(items.Sum(i => i.Overflow.TotalMilliseconds));

            return new OvertimeReport(items, sum);
        }
    }
}