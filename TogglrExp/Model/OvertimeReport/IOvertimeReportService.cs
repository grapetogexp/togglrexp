﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TogglrExp.Model.Jira;

namespace TogglrExp.Model.OvertimeReport
{
    public interface IOvertimeReportService
    {
        Task<OvertimeReport> Get(DateTime dateFrom, DateTime dateTo, TimeSpan norm, JiraIssueModel issue, CancellationToken cancellationToken);
    }
}