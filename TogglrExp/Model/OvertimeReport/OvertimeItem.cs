﻿using System;

namespace TogglrExp.Model.OvertimeReport
{
    public class OvertimeItem
    {
        public OvertimeItem(DateTime date, TimeSpan overflow)
        {
            Date = date;
            Overflow = overflow;
        }

        public DateTime Date { get; private set; }

        public TimeSpan Overflow { get; private set; }

        public override string ToString()
        {
            return string.Format("OvertimeItem [Date: ({0:yyy-mm-dd}) Overflow: ({1:hh\\:mm\\:ss})]", Date, Overflow);
        }
    }
}