﻿using System;
using System.Collections.Generic;

namespace TogglrExp.Model.OvertimeReport
{
    public class OvertimeReport
    {
        public OvertimeReport(IEnumerable<OvertimeItem> items, TimeSpan total)
        {
            Items = items;
            Total = total;
        }

        public IEnumerable<OvertimeItem> Items { get; private set; }

        public TimeSpan Total { get; private set; }

        public override string ToString()
        {

            return string.Format("OvertimeReport[Total: ({0:hh\\:mm}) Items ({1})]", Total, string.Join(";", Items));
        }
    }
}