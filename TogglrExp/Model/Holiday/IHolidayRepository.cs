﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TogglrExp.Model.Holiday
{
    public interface IHolidayRepository
    {
        Task<List<Holiday>> Get(DateTime dateFrom, DateTime dateTo);

        Task Delete(DateTime date);

        Task<bool> Exists(DateTime date);

        Task Insert(Holiday holiday);
    }
}