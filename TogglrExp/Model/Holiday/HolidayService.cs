﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using TogglrExp.Helpers;

namespace TogglrExp.Model.Holiday
{
    public class HolidayService : IHolidayService
    {
        private readonly IHolidayRepository _holidayRepository;
        private readonly IMessenger _messenger;

        public HolidayService(
            IHolidayRepository holidayRepository,
            IMessenger messenger)
        {
            _holidayRepository = holidayRepository;
            _messenger = messenger;
        }

        public async Task<IEnumerable<Holiday>> HolidayList(DateTime dateFrom, DateTime dateTo)
        {
            return await _holidayRepository.Get(dateFrom, dateTo);
        }

        public async Task Delete(DateTime dateTime)
        {
            await _holidayRepository.Delete(dateTime);
            _messenger.Send(new HolidayRemovedMessage());
        }

        public async Task<ValidationResult> Add(Holiday holiday)
        {
            List<string> errors = new List<string>();

            if (string.IsNullOrWhiteSpace(holiday.Name))
            {
                errors.Add("Nazwa jest wymagana");
            }
            else if (holiday.Name.Length > 100)
            {
                errors.Add("Długość nazwy nie może przekraczać 100 znaków");
            }

            bool exists = await _holidayRepository.Exists(holiday.Date);
            if (exists)
            {
                errors.Add("W tym dniu istnieje już święto");
            }

            var result = new ValidationResult(errors);

            if (result.IsValid)
            {
                await _holidayRepository.Insert(holiday);
            }

            return result;
        }

        public async Task FillWeekends(int year)
        {
            var newYear =  new DateTime(year, 1, 1);
            DateTime endOfTheYear = new DateTime(year, 12, 31);

            foreach (var day in DateTimeExtesions.DaysRange(newYear, endOfTheYear))
            {
                switch (day.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        await _holidayRepository.Insert(new Holiday(day, "Niedziela"));
                        break;
                    case DayOfWeek.Saturday:
                        await _holidayRepository.Insert(new Holiday(day, "Sobota"));
                        break;
                }
            }
        }
    }
}