﻿using System;

namespace TogglrExp.Model.Holiday
{
    public class Holiday
    {
        public Holiday(DateTime date, string name)
        {
            Date = date;
            Name = name;
        }

        public DateTime Date { get; private set; }

        public string Name { get; private set; }

        public override string ToString()
        {
            return string.Format("Holiday [Date: ({0:yyyy-MM-dd}); Name({1})]", Date, Name);
        }
    }
}