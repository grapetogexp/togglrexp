﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TogglrExp.Model.Holiday
{
    public interface IHolidayService
    {
        Task<IEnumerable<Holiday>> HolidayList(DateTime dateFrom, DateTime dateTo);

        Task Delete(DateTime dateTime);

        Task<ValidationResult> Add(Holiday holiday);

        Task FillWeekends(int year);
    }
}