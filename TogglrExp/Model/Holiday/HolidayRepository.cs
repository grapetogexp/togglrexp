﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Simple.Data;
using TogglrExp.Model.DbUpdate;
using TracerAttributes;

namespace TogglrExp.Model.Holiday
{
    [TraceOn(TraceTarget.Public)]
    public class HolidayRepository : IHolidayRepository
    {
        public Task<List<Holiday>> Get(DateTime dateFrom, DateTime dateTo)
        {
            return Task.Run(() => GetInternal(dateFrom, dateTo));
        }
        
        public Task Delete(DateTime date)
        {
            return Task.Run(() => DeleteInternal(date));
        }

        public Task<bool> Exists(DateTime date)
        {
            return Task.Run(() => ExistsInternal(date));
        }

        public Task Insert(Holiday holiday)
        {
            return  Task.Run(() => InsertInternal(holiday));
        }

        private List<Holiday> GetInternal(DateTime dateFrom, DateTime dateTo)
        {
            var db = DB();
            List<dynamic> list = db.Holidays.FindAll(db.Holidays.Date >= dateFrom && db.Holidays.Date <= dateTo);
            return list.ConvertAll(o => new Holiday(o.Date, o.Name));
        }

        private void DeleteInternal(DateTime date)
        {
            var db = DB();
            db.Holidays.DeleteByDate(date);
        }

        private bool ExistsInternal(DateTime date)
        {
            var db = DB();
            return db.Holidays.Exists(db.Holidays.Date == date);
        }

        private void InsertInternal(Holiday holiday)
        {
            var db = DB();
            db.Holidays.Insert(Date: holiday.Date, Name: holiday.Name);
        }

        private dynamic DB()
        {
            return Database.OpenConnection(DbHelper.GetConnectionString());
        }
    }
}