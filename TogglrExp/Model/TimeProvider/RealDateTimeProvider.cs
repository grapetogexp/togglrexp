﻿using System;

namespace TogglrExp.Model.TimeProvider
{
    public class RealDateTimeProvider : IDateTimeProvider
    {
        public DateTime Today
        {
            get
            {
                return DateTime.Today;
            }
        }
    }
}