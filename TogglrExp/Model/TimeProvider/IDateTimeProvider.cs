﻿using System;

namespace TogglrExp.Model.TimeProvider
{
    public interface IDateTimeProvider
    {
         DateTime Today { get; }
    }
}