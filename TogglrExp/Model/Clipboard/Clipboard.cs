﻿using TracerAttributes;

namespace TogglrExp.Model.Clipboard
{
    [TraceOn]
    public class Clipboard : IClipboard
    {
        public void Copy(string text)
        {
            System.Windows.Clipboard.SetText(text);
        }
    }
}