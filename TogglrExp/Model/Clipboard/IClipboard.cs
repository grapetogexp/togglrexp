﻿namespace TogglrExp.Model.Clipboard
{
    public interface IClipboard
    {
        void Copy(string text);
    }
}