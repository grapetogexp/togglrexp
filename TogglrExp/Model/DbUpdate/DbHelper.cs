﻿using System;
using System.Data.SQLite;
using System.IO;

namespace TogglrExp.Model.DbUpdate
{
    public static class DbHelper
    {
        public static string GetConnectionString()
        {
            string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var dataBasePath = Path.Combine(appDataPath, "TogglrExp", "data.dat");

            var builder = new SQLiteConnectionStringBuilder();
            builder.Version = 3;
            builder.DataSource = dataBasePath;
            builder.FailIfMissing = false;

            return builder.ToString();
        }
    }
}