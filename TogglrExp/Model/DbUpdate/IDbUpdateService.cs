﻿using System.Threading.Tasks;

namespace TogglrExp.Model.DbUpdate
{
    // Only marker interface
    public interface IDbUpdateService
    {
        Task UpdateDataBase();
    }
}