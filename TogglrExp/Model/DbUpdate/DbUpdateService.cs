﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;
using TogglrExp.Helpers;

namespace TogglrExp.Model.DbUpdate
{
    public class DbUpdateService : IDbUpdateService
    {
        public async Task UpdateDataBase()
        {
            using (var connection = new SQLiteConnection(DbHelper.GetConnectionString()))
            {
                await connection.OpenAsync();
                await InitVersionsTable(connection);
                await UpdateToVersion(connection, 1, "Days off for 2016 and 2017", Version1);
                await UpdateToVersion(connection, 2, "Jira instances storages", Version2);
                await UpdateToVersion(connection, 3, "Days off for 2018 and 2019", Version3);
            }
        }

        #region Empty database
        
        private Task InitVersionsTable(SQLiteConnection connection)
        {
            return ExecuteParameterLess(
                connection, 
                "CREATE TABLE IF NOT EXISTS Db_versions(version_id int not null primary key, description varchar(300) not null)");
        }

        #endregion

        #region Version 1

        private async Task Version1(SQLiteConnection connection)
        {
            await ExecuteParameterLess(
                connection,
                "CREATE TABLE IF NOT EXISTS Holidays('Date' DateTime Primary key,Name Nvarchar(100) not null)");

            var holidays = new[]
            {
                new {Date = new DateTime(2016, 1, 6), Description = "Trzech Króli (Objawienie Pańskie)"},
                new {Date = new DateTime(2016, 4, 17), Description = "Poniedziałek Wielkanocny"},
                new {Date = new DateTime(2016, 5, 1), Description = "Święto Pracy"},
                new {Date = new DateTime(2016, 5, 3), Description = "Święto Konstytucji 3 5"},
                new {Date = new DateTime(2016, 6, 15), Description = "Boże Ciało"},
                new {Date = new DateTime(2016, 8, 15), Description = "Wniebowzięcie Najświętszej Maryi Panny"},
                new {Date = new DateTime(2016, 11, 1), Description = "Wszystkich Świętych"},
                new {Date = new DateTime(2016, 11, 11), Description = "Święto Niepodległości"},
                new {Date = new DateTime(2016, 12, 25), Description = "Boże Narodzenie (pierwszy dzień)"},
                new {Date = new DateTime(2016, 12, 26), Description = "Boże Narodzenie (drugi dzień)"},
                new {Date = new DateTime(2017, 1, 1), Description = "Nowy Rok"},
                new {Date = new DateTime(2017, 1, 6), Description = "Trzech Króli (Objawienie Pańskie)"},
                new {Date = new DateTime(2017, 3, 28), Description = "Poniedziałek Wielkanocny"},
                new {Date = new DateTime(2017, 5, 3), Description = "Święto Konstytucji 3 5"},
                new {Date = new DateTime(2017, 5, 26), Description = "Boże Ciało"},
                new {Date = new DateTime(2017, 8, 15), Description = "Wniebowzięcie Najświętszej Maryi Panny"},
                new {Date = new DateTime(2017, 11, 1), Description = "Wszystkich Świętych"},
                new {Date = new DateTime(2017, 11, 11), Description = "Święto Niepodległości"},
                new {Date = new DateTime(2017, 12, 25), Description = "Boże Narodzenie (pierwszy dzień)"},
                new {Date = new DateTime(2017, 12, 26), Description = "Boże Narodzenie (drugi dzień)"}
            };

            foreach (var holiday in holidays)
            {
                await AddHoliday(connection, holiday.Date, holiday.Description);
            }

            var weekends =
                from date in DateTimeExtesions.DaysRange(new DateTime(2016, 1, 1), new DateTime(2017, 12, 31))
                where date.DayOfWeek == DayOfWeek.Sunday || date.DayOfWeek == DayOfWeek.Saturday
                select new
                {
                    Date = date,
                    Description = date.DayOfWeek == DayOfWeek.Sunday ? "Niedziela" : "Sobota"
                };

            foreach (var weekend in weekends)
            {
                await AddHoliday(connection, weekend.Date, weekend.Description);
            }
        }
        #endregion

        #region Version 2

        private async Task Version2(SQLiteConnection connection)
        {
            await ExecuteParameterLess(
                connection, 
                "CREATE TABLE IF NOT EXISTS Jiras(id integer not null primary key autoincrement, name nvarchar(50) not null, url nvarchar(256) not null, " +
                "prefered_issue nvarchar(50) null, credentials blob null)");
        }

        #endregion

        #region Version 3

        private async Task Version3(SQLiteConnection connection)
        {
            var holidays = new[]
            {
                new { Date = new DateTime(2018, 1, 1), Description = "Nowy Rok"},
                new { Date = new DateTime(2018, 1, 6), Description = "Święto Trzech Króli"},
                new { Date = new DateTime(2018, 4, 1), Description = "Wielkanoc"},
                new { Date = new DateTime(2018, 4, 2), Description = "Poniedziałek Wielkanocny"},
                new { Date = new DateTime(2018, 5, 1), Description = "Święto Pracy"},
                new { Date = new DateTime(2018, 5, 3), Description = "Święto Konstytucji 3 Maja"},
                new { Date = new DateTime(2018, 5, 20), Description = "Zielone Świątki"},
                new { Date = new DateTime(2018, 5, 31), Description = "Boże Ciało"},
                new { Date = new DateTime(2018, 8, 15), Description = "Wniebowzięcie Najświętszej Maryi Panny"},
                new { Date = new DateTime(2018, 11, 1), Description = "Wszystkich Świętych"},
                new { Date = new DateTime(2018, 11, 11), Description = "Święto Niepodległości"},
                new { Date = new DateTime(2018, 12, 25), Description = "Pierwszy dzień Bożego Narodzenia"},
                new { Date = new DateTime(2018, 12, 26), Description = "Drugi dzień Bożego Narodzenia"},
                new { Date = new DateTime(2019, 1, 1), Description = "Nowy Rok"},
                new { Date = new DateTime(2019, 1, 6), Description = "Święto Trzech Króli"},
                new { Date = new DateTime(2019, 4, 21), Description = "Wielkanoc"},
                new { Date = new DateTime(2019, 4, 22), Description = "Poniedziałek Wielkanocny"},
                new { Date = new DateTime(2019, 5, 1), Description = "Święto Pracy"},
                new { Date = new DateTime(2019, 5, 3), Description = "Święto Konstytucji 3 Maja"},
                new { Date = new DateTime(2019, 6, 9), Description = "Zielone Świątki"},
                new { Date = new DateTime(2019, 6, 20), Description = "Boże Ciało"},
                new { Date = new DateTime(2019, 8, 15), Description = "Wniebowzięcie Najświętszej Maryi Panny"},
                new { Date = new DateTime(2019, 11, 1), Description = "Wszystkich Świętych"},
                new { Date = new DateTime(2019, 11, 11), Description = "Święto Niepodległości"},
                new { Date = new DateTime(2019, 12, 25), Description = "Pierwszy dzień Bożego Narodzenia"},
                new { Date = new DateTime(2019, 12, 26), Description = "Drugi dzień Bożego Narodzenia"},
            };

            foreach (var holiday in holidays)
            {
                await AddHoliday(connection, holiday.Date, holiday.Description);
            }

            var weekends =
                from date in DateTimeExtesions.DaysRange(new DateTime(2018, 1, 1), new DateTime(2019, 12, 31))
                where date.DayOfWeek == DayOfWeek.Sunday || date.DayOfWeek == DayOfWeek.Saturday
                select new
                {
                    Date = date,
                    Description = date.DayOfWeek == DayOfWeek.Sunday ? "Niedziela" : "Sobota"
                };

            foreach (var weekend in weekends)
            {
                await AddHoliday(connection, weekend.Date, weekend.Description);
            }
        }

        #endregion

        private async Task ExecuteParameterLess(SQLiteConnection connection, string query)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = query;

                await command.ExecuteNonQueryAsync();
            }
        }

        private async Task AddHoliday(SQLiteConnection connection, DateTime dateTime, string name)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "insert or ignore into holidays(Date, Name) values(@date, @name)";
                command.Parameters.Add("date", DbType.DateTime).Value = dateTime;
                command.Parameters.Add("name", DbType.String, 200).Value = name;
                await command.ExecuteNonQueryAsync();
            }
        }

        private async Task UpdateToVersion(SQLiteConnection sqLiteConnection, int version, string description, Func<SQLiteConnection, Task> updateMethod)
        {
            using (var command = sqLiteConnection.CreateCommand())
            {
                command.CommandText = "select version_id from db_versions where version_id = @version";
                command.Parameters.Add("version", DbType.Int32).Value = version;

                object result = await command.ExecuteScalarAsync();
                if (result != DBNull.Value && Convert.ToInt32(result) == version) return;
            }

            await updateMethod(sqLiteConnection);

            using (var command = sqLiteConnection.CreateCommand())
            {
                command.CommandText = "insert into db_versions(version_id, description) values (@version, @description)";
                command.Parameters.Add("version", DbType.Int32).Value = version;
                command.Parameters.Add("description", DbType.String, 300).Value = description;

                await command.ExecuteNonQueryAsync();
            }
        }
    }
}