﻿using System;

namespace TogglrExp.Model.Settings
{
    public interface ISettingsManager
    {
        void Modify(Action<ModableSettings> modificationAction);
        
        Settings Get();
    }
}