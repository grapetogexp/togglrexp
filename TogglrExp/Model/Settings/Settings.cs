﻿
namespace TogglrExp.Model.Settings
{
    public class Settings
    {
        public Settings(
            string userToken, 
            bool descriptionExported, 
            bool timeFromExported, 
            bool timeToExported, 
            bool durationExported,
            long? jiraConfigurationId)
        {
            UserToken = userToken;
            DescriptionExported = descriptionExported;
            TimeFromExported = timeFromExported;
            TimeToExported = timeToExported;
            DurationExported = durationExported;
            JiraConfigurationId = jiraConfigurationId;
        }

        public string UserToken { get; private set; }

        public bool DescriptionExported { get; private set; }

        public bool TimeFromExported { get; private set; }

        public bool TimeToExported { get; private set; }

        public bool DurationExported { get; private set; }

        public long? JiraConfigurationId { get; private set; }
        
        public override string ToString()
        {
            return $"Settings [DescriptionExported ({DescriptionExported}) " +
                $"TimeFromExported ({TimeFromExported})  TimeToExported ({TimeToExported}) " +
                $"DurationExported ({DurationExported}) JiraConfigurationId ({JiraConfigurationId})]";
        }
    }
}