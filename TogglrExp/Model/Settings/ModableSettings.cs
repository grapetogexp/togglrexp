﻿namespace TogglrExp.Model.Settings
{
    public class ModableSettings
    {
        public static ModableSettings FromReadOnly(Settings settings)
        {
            return new ModableSettings
            {
                UserToken = settings.UserToken,

                DescriptionExported = settings.DescriptionExported,
                TimeFromExported = settings.TimeFromExported,
                TimeToExported = settings.TimeToExported,
                DurationExported = settings.DurationExported,
                JiraConfigurationId = settings.JiraConfigurationId
            };
        }

        public string UserToken { get; set; }

        public bool DescriptionExported { get; set; }

        public bool TimeFromExported { get; set; }

        public bool TimeToExported { get; set; }

        public bool DurationExported { get; set; }

        public long? JiraConfigurationId { get; set; }
    }
}