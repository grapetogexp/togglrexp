﻿using System;
using System.Collections.Generic;
using TracerAttributes;

namespace TogglrExp.Model.Settings
{
    [TraceOn]
    public class SettingsManager : ISettingsManager
    {
        private const string DescriptionTag = "Description";
        private const string TimeFromTag = "TimeFrom";
        private const string TimeToTag = "TimeTo";
        private const string DurationTag = "Duration";

        public void Modify(Action<ModableSettings> modificationAction)
        {
            var pSettings = Properties.Settings.Default;

            var settings = ConvertToModable(pSettings);

            modificationAction(settings);

            pSettings.TogglToken = settings.UserToken;

            List<string> exportedColumns = new List<string>();

            if (settings.DescriptionExported)
            {
                exportedColumns.Add(DescriptionTag);
            }

            if (settings.TimeFromExported)
            {
                exportedColumns.Add(TimeFromTag);
            }

            if (settings.TimeToExported)
            {
                exportedColumns.Add(TimeToTag);
            }

            if (settings.DurationExported)
            {
                exportedColumns.Add(DurationTag);
            }

            pSettings.VisibleColumns = string.Join(";", exportedColumns);
            pSettings.JiraConfigurationId = settings.JiraConfigurationId.GetValueOrDefault();

            pSettings.Save();
        }
        
        public Settings Get()
        {
            var pSettings = Properties.Settings.Default;
            if (pSettings.RequiresUpdate)
            {
                pSettings.Upgrade();
                pSettings.RequiresUpdate = false;
                pSettings.Save();
            }

            var result = ConvertToReadOnly(pSettings);
            
            return result;
        }

        private static ModableSettings ConvertToModable(Properties.Settings pSettings)
        {
            var settings = ConvertToReadOnly(pSettings);
            return ModableSettings.FromReadOnly(settings);
        }

        private static Settings ConvertToReadOnly(Properties.Settings pSettings)
        {
            string[] exportedColumnsTags = pSettings.VisibleColumns.Split(';');
            bool
                descriptionExported = false,
                timeFromExported = false,
                timeToExported = false,
                durrationExported = false;

            foreach (var tag in exportedColumnsTags)
            {
                switch (tag)
                {
                    case DescriptionTag:
                        descriptionExported = true;
                        break;
                    case TimeFromTag:
                        timeFromExported = true;
                        break;
                    case TimeToTag:
                        timeToExported = true;
                        break;
                    case DurationTag:
                        durrationExported = true;
                        break;
                }
            }

            return new Settings(
                pSettings.TogglToken,
                descriptionExported,
                timeFromExported,
                timeToExported,
                durrationExported,
                pSettings.JiraConfigurationId == 0 ? (long?) null : pSettings.JiraConfigurationId);
        }
    }
}