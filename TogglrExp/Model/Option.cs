﻿using System;
using System.Collections.Generic;

namespace TogglrExp.Model
{
    public static class Option
    {
        public static Option<T> Some<T>(T value)
        {
            return Option<T>.Some(value);
        }

        public static T GetValueOrDefault<T>(this Option<T> self)
        {
            return self.IsNone ? default(T) : self.Value;
        }

        public static Option<TResult> Map<TSource, TResult>(this Option<TSource> self, Func<TSource, TResult> map)
        {
            return self.IsNone ? Option<TResult>.None : Option.Some(map(self.Value));
        }
    }

    public class Option<T> : IEquatable<Option<T>>
    {
        private readonly T _value;
        private readonly bool _isNone;

        static Option()
        {
            None = new Option<T>();
        }

        private Option()
        {
            _isNone = true;
        }

        private Option(T value)
        {
            _isNone = false;
            _value = value;
        }

        public static Option<T> None { get; private set; }

        public bool IsNone
        {
            get { return _isNone; }
        }

        public T Value
        {
            get
            {
                if (IsNone)
                {
                    throw new InvalidOperationException("Cannot call Value on None");
                }
                return _value;
            }
        }

        public static Option<T> Some(T value)
        {
            return new Option<T>(value);
        }

        public bool Equals(Option<T> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return EqualityComparer<T>.Default.Equals(_value, other._value) && IsNone == other.IsNone;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Option<T>) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<T>.Default.GetHashCode(_value) * 397) ^ IsNone.GetHashCode();
            }
        }
    }
}