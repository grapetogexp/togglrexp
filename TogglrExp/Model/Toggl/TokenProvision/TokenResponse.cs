﻿namespace TogglrExp.Model.Toggl.TokenProvision
{
    public class TokenResponse
    {
        public TokenResponse(string token)
        {
            Token = token;
        }

        public string Token { get; private set; }
    }
}