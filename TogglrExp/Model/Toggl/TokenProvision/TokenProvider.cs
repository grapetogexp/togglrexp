﻿using System.Threading.Tasks;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Settings;

namespace TogglrExp.Model.Toggl.TokenProvision
{
    public class TokenProvider : StoredOptionalSampleProvider<TokenRequest, TokenResponse>
    {
        private readonly ISettingsManager _settingsManager;

        public TokenProvider(
            IAsyncOptionalStoredSampleProvider<TokenRequest, TokenResponse> innerProvider,
            ISettingsManager settingsManager) 
            : base(innerProvider)
        {
            _settingsManager = settingsManager;
        }

        protected override Task<Option<TokenResponse>> Retrieve(TokenRequest param)
        {
            var settings = _settingsManager.Get();

            if (string.IsNullOrWhiteSpace(settings.UserToken))
            {
                return Task.FromResult(Option<TokenResponse>.None);
            }

            return Task.FromResult(Option.Some(new TokenResponse(settings.UserToken)));
        }

        protected override Task Store(TokenRequest param, TokenResponse result)
        {
            _settingsManager.Modify(settings =>
            {
                settings.UserToken = result.Token;
            });
            return Task.FromResult(false);
        }

        protected override Task Clear(TokenRequest param)
        {
            _settingsManager.Modify(settings =>
            {
                settings.UserToken = string.Empty;
            });
            return Task.FromResult(false);
        }
    }
}