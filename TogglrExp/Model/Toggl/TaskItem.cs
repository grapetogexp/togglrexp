﻿using System;

namespace TogglrExp.Model.Toggl
{
    public class TaskItem
    {
        public string Description { get; set; }

        public DateTime TimeFrom { get; set; }

        public DateTime? TimeTo { get; set; }

        public TimeSpan Duration { get; set; }

        public override string ToString()
        {
            return string.Format(
                "TaskItem [Description ({0}) TimePeriod ({1} - {2}) Duration: ({3})]",
                Description, TimeFrom, TimeTo, Duration);
        }
    }
}
