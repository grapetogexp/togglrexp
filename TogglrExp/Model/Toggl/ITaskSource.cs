﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TogglrExp.Model.Toggl
{
    public interface ITaskSource
    {
        Task<IEnumerable<WorkspaceItem>> GetWorkspaces(CancellationToken cancellationToken);

        Task<IEnumerable<ClientItem>> GetClients(int workspaceId, CancellationToken cancellationToken);

        Task<IEnumerable<ProjectItem>> GetProjects(int workspaceId, int? clientId, CancellationToken cancellationToken);

        Task<IEnumerable<TaskItem>> GetItems(TaskFilter taskFilter, CancellationToken cancellationToken);
    }
}