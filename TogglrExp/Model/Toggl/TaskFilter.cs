﻿using System;

namespace TogglrExp.Model.Toggl
{
    public class TaskFilter
    {
        public TaskFilter(DateTime dateFrom, DateTime dateTo, int? workspaceId, int? clientId, int? projectId)
        {
            DateFrom = dateFrom;
            DateTo = dateTo;
            WorkspaceId = workspaceId;
            ClientId = clientId;
            ProjectId = projectId;
        }

        public DateTime DateFrom { get; }

        public DateTime DateTo { get; }

        public int? WorkspaceId { get; }

        public int? ClientId { get; }

        public int? ProjectId { get; }
    }
}