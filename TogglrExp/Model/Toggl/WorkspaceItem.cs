﻿namespace TogglrExp.Model.Toggl
{
    public class WorkspaceItem
    {
        public WorkspaceItem(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; private set; }

        public string Name { get; private set; }
    }
}