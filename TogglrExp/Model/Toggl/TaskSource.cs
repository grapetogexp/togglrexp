﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Toggl.QueryObjects;
using Toggl.Services;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Toggl.TokenProvision;
using TracerAttributes;

namespace TogglrExp.Model.Toggl
{
    [TraceOn(TraceTarget.Public)]
    public class TaskSource : ITaskSource
    {
        private readonly IAsyncOptionalProvider<TokenRequest, TokenResponse> _tokenProvider;

        public TaskSource(IAsyncOptionalProvider<TokenRequest, TokenResponse> tokenProvider)
        {
            _tokenProvider = tokenProvider;
        }

        public async Task<IEnumerable<WorkspaceItem>> GetWorkspaces(CancellationToken cancellationToken)
        {
            var tokenResponse = await GetToken(cancellationToken);

            if (tokenResponse.IsNone)
            {
                return Enumerable.Empty<WorkspaceItem>();
            }
            
            var workspaceitems = await Task.Run(() =>
            {
                var workspaceService = new WorkspaceService(tokenResponse.Value.Token);
                return workspaceService.List();
            });

            // ReSharper disable once PossibleInvalidOperationException
            return workspaceitems.ConvertAll(o => new WorkspaceItem(o.Id.Value, o.Name));
        }

        public async Task<IEnumerable<ClientItem>> GetClients(int workspaceId, CancellationToken cancellationToken)
        {
            var tokenResponse = await GetToken(cancellationToken);

            if (tokenResponse.IsNone)
            {
                return Enumerable.Empty<ClientItem>();
            }

            var workspaceitems = await Task.Run(() =>
            {
                var workspaceService = new WorkspaceService(tokenResponse.Value.Token);
                return workspaceService.Clients(workspaceId);
            });

            // ReSharper disable once PossibleInvalidOperationException
            return workspaceitems.ConvertAll(o => new ClientItem(o.Id.Value, o.Name));
        }

        public async Task<IEnumerable<ProjectItem>> GetProjects(int workspaceId, int? clientId, CancellationToken cancellationToken)
        {
            var tokenResponse = await GetToken(cancellationToken);

            if (tokenResponse.IsNone)
            {
                return Enumerable.Empty<ProjectItem>();
            }

            var workspaceitems = await Task.Run(() =>
            {
                var workspaceService = new WorkspaceService(tokenResponse.Value.Token);
                var result = workspaceService.Projects(workspaceId);

                if (clientId.HasValue)
                {
                    return result.FindAll(o => o.ClientId == clientId);
                }
                return result;
            });

            // ReSharper disable once PossibleInvalidOperationException
            return workspaceitems.ConvertAll(o => new ProjectItem(o.Id.Value, o.Name));
        }

        public async Task<IEnumerable<TaskItem>> GetItems(TaskFilter taskFilter, CancellationToken cancellationToken)
        {
            var tokenResponse = await GetToken(cancellationToken);

            if (tokenResponse.IsNone)
            {
                return Enumerable.Empty<TaskItem>();
            }
            
            var taskItems = await Task.Run(() =>
            {
                var apiService = new ApiService(tokenResponse.Value.Token);

                var service = new TimeEntryService(apiService);

                int? projectId = null;
                int? workspaceId = null;
                if (taskFilter.WorkspaceId.HasValue && !taskFilter.ProjectId.HasValue)
                {
                    workspaceId = taskFilter.WorkspaceId;
                }
                else if (!taskFilter.ProjectId.HasValue)
                {
                    projectId = taskFilter.ProjectId;
                }

                var result = service.List(new TimeEntryParams
                {
                    StartDate = taskFilter.DateFrom,
                    EndDate = taskFilter.DateTo,
                    ProjectId = projectId,
                    WorkspaceId = workspaceId
                });

                if (!taskFilter.ProjectId.HasValue)
                {
                    if (taskFilter.ClientId.HasValue)
                    {
                        if (taskFilter.WorkspaceId.HasValue)
                        {
                            var workspaceService = new WorkspaceService(apiService);
                            var projects = workspaceService.Projects(taskFilter.WorkspaceId.Value);

                            result = (from i in result
                                join project in projects on i.ProjectId equals project.Id
                                where project.ClientId == taskFilter.ClientId
                                select i).ToList();
                        }
                        else
                        {
                            throw new Exception("Client is selected and workspace is empty it shouldn't happend");
                        }
                    }
                }

                return result.ConvertAll(o =>
                    new TaskItem
                    {
                        Description = o.Description,
                        TimeFrom = FormatDate(o.Start).Value,
                        TimeTo = FormatDate(o.Stop),
                        Duration = FromSeconds(o.Duration)

                    });
            });
            return taskItems;
        }

        private Task<Option<TokenResponse>> GetToken(CancellationToken cancellationToken)
        {
            var tokenRequest = new TokenRequest();
            return _tokenProvider.Provide(tokenRequest, response =>
            {
                var token = response.Token;
                if (string.IsNullOrWhiteSpace(token))
                {
                    return Task.FromResult(false);
                }

                return Task.Run(() =>
                {
                    try
                    {
                        // ReSharper disable once ObjectCreationAsStatement
                        new TimeEntryService(token);
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
            }, cancellationToken);
        }

        private static DateTime? FormatDate(string original)
        {
            if (string.IsNullOrEmpty(original))
            {
                return null;
            }
            return DateTime.Parse(original);
        }

        private static TimeSpan FromSeconds(long? duration)
        {
            if (!duration.HasValue)
            {
                return TimeSpan.Zero;
            }

            if (duration < 0)
            {
                return TimeSpan.Zero;
            }

            return TimeSpan.FromSeconds(duration.Value);
        }
    }
}