﻿namespace TogglrExp.Model.Secrets
{
    public interface IProtector
    {
        byte[] Protect<T>(T data);

        T Unprotect<T>(byte[] protectedBytes);
    }
}