﻿using System.IO;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace TogglrExp.Model.Secrets
{
    public class Protector : IProtector
    {
        private readonly byte[] _entropy = { 59, 121, 44, 34, 49 };
        
        public byte[] Protect<T>(T data)
        {
            byte[] plaintextBytes;
            using (var stream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    var serializer = new JsonSerializer();
                    serializer.Serialize(streamWriter, data);
                    streamWriter.Flush();
                    stream.Position = 0;
                    plaintextBytes = stream.ToArray();
                }
            }
            return ProtectedData.Protect(plaintextBytes, _entropy, DataProtectionScope.CurrentUser);
        }

        public T Unprotect<T>(byte[] protectedBytes)
        {
            var plainBytes = ProtectedData.Unprotect(protectedBytes, _entropy, DataProtectionScope.CurrentUser);
            
            using (var stream = new MemoryStream(plainBytes))
            {
                using (var reader = new StreamReader(stream))
                {
                    using (var jsonReader = new JsonTextReader(reader))
                    {
                        var serializer = new JsonSerializer();
                        return serializer.Deserialize<T>(jsonReader);
                    }
                }
            }
        }
    }
}