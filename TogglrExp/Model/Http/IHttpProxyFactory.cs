﻿namespace TogglrExp.Model.Http
{
    public interface IHttpProxyFactory
    {
        IHttpProxy Create(string rootUrl = null);
    }
}