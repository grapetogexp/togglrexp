﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Tracer.Log4Net;
using TracerAttributes;

namespace TogglrExp.Model.Http
{
    [NoTrace]
    public class HttpProxy : IHttpProxy
    {
        private readonly JsonSerializer _serializer;
        private readonly HttpClient _httpClient;

        public HttpProxy(string rootUrl = null)
        {
            var serializerSettings = new JsonSerializerSettings();

            // timezone hardcoded
            serializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ss.fff UTC";

            _serializer = JsonSerializer.Create(serializerSettings);

            var httpMessageHandler = new HttpClientHandler();
            var container = new CookieContainer();

            httpMessageHandler.UseCookies = true;
            httpMessageHandler.CookieContainer = container;

            _httpClient = new HttpClient(httpMessageHandler, true);
            if (!string.IsNullOrWhiteSpace(rootUrl))
            {
                _httpClient.BaseAddress = new Uri(rootUrl);
            }
        }
        
        public async Task<HttpResponse<TResult>> Get<TResult>(string url)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            var response = await _httpClient.GetAsync(url);

            var result = await Decode<TResult>(response);
            stopwatch.Stop();

            Log.DebugFormat("HttpProxy.Get url: {0}; result: {1}", url, result);

            return result;
        }

        public async Task DownloadFile(string url, string destination)
        {
            using (var response = await _httpClient.GetStreamAsync(url))
            {
                using (var fileStream = new FileStream(destination, FileMode.OpenOrCreate))
                {
                    response.CopyTo(fileStream);
                }
            }
        }

        public async Task<HttpResponse<TResult>> Post<TResult, TParam>(string url, TParam param)
        {
            var content = Encode(param);

            var response = await _httpClient.PostAsync(url, content);

            var result = await Decode<TResult>(response);

            Log.DebugFormat("HttpProxy.Post url: {0}; param: {1}; result: {2}", url, param, result);

            return result;
        }

        public async Task<HttpResponse<string>> Test(string url)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            var response = await _httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, url));

            stopwatch.Stop();

            Log.DebugFormat("HttpProxy.Get url: {0};", url);

            return new HttpResponse<string>(response.StatusCode, string.Empty);
        }

        private HttpContent Encode<TParam>(TParam param)
        {
            using (var memStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memStream))
                {
                    _serializer.Serialize(streamWriter, param);
                }
                return new ByteArrayContent(memStream.ToArray())
                {
                    Headers = { { "Content-Type", "application/json" } }
                };
            }
        }

        private async Task<HttpResponse<TResult>> Decode<TResult>(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                return new HttpResponse<TResult>(response.StatusCode, default(TResult));
            }
            using (var stream = await response.Content.ReadAsStreamAsync())
            {
                using (var textReader = new StreamReader(stream))
                {
                    var body = (TResult)_serializer.Deserialize(textReader, typeof(TResult));

                    return new HttpResponse<TResult>(response.StatusCode, body);
                }
            }
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool isDisposing)
        {
            if (!isDisposing) return;

            _httpClient.Dispose();
        }

        

        #endregion
    }
}