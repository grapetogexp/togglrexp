﻿namespace TogglrExp.Model.Http
{
    public class HttpProxyFactory : IHttpProxyFactory
    {
        public IHttpProxy Create(string rootUrl = null)
        {
            return new HttpProxy(rootUrl);
        }
    }
}