﻿using System.Net;
using TracerAttributes;

namespace TogglrExp.Model.Http
{
    [NoTrace]
    public class HttpResponse<T>
    {
        public HttpResponse(HttpStatusCode statusCode, T body)
        {
            StatusCode = statusCode;
            Body = body;
        }

        public T Body { get; private set; }

        public HttpStatusCode StatusCode { get; private set; }

        public override string ToString()
        {
            return string.Format("HttpResponse[Code: ({0}), Body: ({1})]", StatusCode, Body);
        }
    }
}