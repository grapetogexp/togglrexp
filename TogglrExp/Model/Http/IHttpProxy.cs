﻿using System;
using System.Threading.Tasks;

namespace TogglrExp.Model.Http
{
    public interface IHttpProxy : IDisposable
    {
        Task<HttpResponse<TResult>> Get<TResult>(string url);

        Task DownloadFile(string url, string destination);

        Task<HttpResponse<TResult>> Post<TResult, TParam>(string url, TParam param);

        Task<HttpResponse<string>> Test(string url);
    }
}