﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TogglrExp.Model.Jira
{
    public interface IJiraService
    {
        Task<IEnumerable<Configuration>> GetConfigurations(CancellationToken cancellationToken);

        Task<ValidationResult> AddConfiguration(ConfigrationPrototype prototype);

        Task<Configuration> GetConfiguration(long id);
        
        Task<ValidationResult> UpdateConfiguration(long id, ConfigrationPrototype prototype);

        Task DeleteConfiguration(long id);

        Task<string> GetPreferedIssueKey(long id);

        Task SetPreferedIssueKey(long id, string key);

        Task<JiraIssueModel> GetIssue(long configurationId, string key, CancellationToken cancellationToken);

        Task<IEnumerable<JiraIssueModel>> GetIssues(long configurationId, string query, CancellationToken cancellationToken);

        Task Report(IEnumerable<Worklog> worklog, JiraIssueModel issue, CancellationToken cancellationToken);

        event EventHandler WorklogReported;

        Task<IEnumerable<Worklog>> Get(DateTime dateFrom, DateTime dateTo, JiraIssueModel issue, CancellationToken cancellationToken);
    }
}