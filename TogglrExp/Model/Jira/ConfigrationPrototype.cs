﻿namespace TogglrExp.Model.Jira
{
    public class ConfigrationPrototype
    {
        public ConfigrationPrototype(string name, string url)
        {
            Name = name;
            Url = url;
        }

        public string Name { get; private set; }

        public string Url { get; private set; }
    }
}