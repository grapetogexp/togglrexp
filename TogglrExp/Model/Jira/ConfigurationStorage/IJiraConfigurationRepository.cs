﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TogglrExp.Model.Jira.ConfigurationStorage
{
    public interface IJiraConfigurationRepository
    {
        Task<IEnumerable<ConfigurationDTO>> GetAll();

        Task<ConfigurationDTO> Get(long id);

        Task Insert(ConfigurationDTO configration);

        Task Update(ConfigurationDTO configration);

        Task Delete(long id);

        Task<byte[]> GetCredentials(long id);

        Task UpdateCredentials(long id, byte[] encryptedBytes);
    }
}