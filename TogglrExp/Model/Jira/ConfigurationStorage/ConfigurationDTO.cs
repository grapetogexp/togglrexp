﻿namespace TogglrExp.Model.Jira.ConfigurationStorage
{
    public class ConfigurationDTO
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string PreferedIssue { get; set; }
    }
}