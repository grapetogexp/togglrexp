﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Threading.Tasks;
using Simple.Data;
using TogglrExp.Model.DbUpdate;

namespace TogglrExp.Model.Jira.ConfigurationStorage
{
    public class JiraConfigurationRepository : IJiraConfigurationRepository
    {
        public Task<IEnumerable<ConfigurationDTO>> GetAll()
        {
            return Task.Run(() => GetAllInternal());
        }

        public Task<ConfigurationDTO> Get(long id)
        {
            return Task.Run(() => GetInternal(id));
        }

        public Task Insert(ConfigurationDTO configration)
        {
            return Task.Run(() => InsertInternal(configration));
        }

        public Task Update(ConfigurationDTO configration)
        {
            return Task.Run(() => UpdateInternal(configration));
        }

        public Task Delete(long id)
        {
            return Task.Run(() => DeleteInternal(id));
        }

        public Task<byte[]> GetCredentials(long id)
        {
            return Task.Run(() => GetCredentialsInternal(id));
        }

        public Task UpdateCredentials(long id, byte[] encryptedBytes)
        {
            return Task.Run(() => UpdateCredentialsInternal(id, encryptedBytes));
        }

        private IEnumerable<ConfigurationDTO> GetAllInternal()
        {
            var db = DB();
            var configurations = db.Jiras.All();
            foreach (var configuration in configurations)
            {
                yield return new ConfigurationDTO
                {
                    Id = configuration.id,
                    Name = configuration.name,
                    Url = configuration.url,
                    PreferedIssue = configuration.prefered_issue
                };
            }
        }

        private ConfigurationDTO GetInternal(long id)
        {
            var db = DB();

            var configuration = db.Jiras.Get(id);

            return new ConfigurationDTO
            {
                Id = configuration.id,
                Name = configuration.name,
                Url = configuration.url,
                PreferedIssue = configuration.prefered_issue
            };
        }

        private void InsertInternal(ConfigurationDTO configuration)
        {
            var db = DB();
            db.Jiras.Insert(
                name: configuration.Name, 
                url: configuration.Url,
                prefered_issue: configuration.PreferedIssue);
        }

        private void UpdateInternal(ConfigurationDTO configuration)
        {
            var db = DB();
            db.Jiras.UpdateById(
                id: configuration.Id,
                name: configuration.Name,
                url: configuration.Url,
                prefered_issue: configuration.PreferedIssue);
        }

        private void DeleteInternal(long id)
        {
            var db = DB();
            db.Jiras.Delete(id: id);
        }

        private byte[] GetCredentialsInternal(long id)
        {
            var db = DB();
            var configuration = db.Jiras.Get(id);
            return configuration.credentials;
        }

        private void UpdateCredentialsInternal(long id, byte[] encryptedBytes)
        {
            // Bug in simple.data
            //var db = DB();
            //db.Jiras.UpdateById(id: id, credentials: encryptedBytes);

            using (var sqliteConnection = new SQLiteConnection(DbHelper.GetConnectionString()))
            {
                sqliteConnection.Open();
                using (var command = sqliteConnection.CreateCommand())
                {
                    command.CommandText = "update jiras set credentials = @p1 where id = @p2";
                    command.Parameters.Add("p1", DbType.Binary).Value = encryptedBytes;
                    command.Parameters.Add("p2", DbType.Int64).Value = id;
                    command.ExecuteNonQuery();
                }
            }
        }

        private dynamic DB()
        {
            string connectionString = DbHelper.GetConnectionString();
            return Database.OpenConnection(connectionString);
        }
    }
}