﻿using System;
using System.Threading.Tasks;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Jira.ConfigurationStorage;

namespace TogglrExp.Model.Jira.UrlProvision
{
    public class JiraUrlProvider : StoredOptionalSampleProvider<JiraUrlRequest, JiraUrl>
    {
        private readonly IJiraConfigurationRepository _jiraConfigurationRepository;

        public JiraUrlProvider(
            IAsyncOptionalStoredSampleProvider<JiraUrlRequest, JiraUrl> innerProvider,
            IJiraConfigurationRepository jiraConfigurationRepository) 
            : base(innerProvider)
        {
            _jiraConfigurationRepository = jiraConfigurationRepository;
        }

        protected override async Task<Option<JiraUrl>> Retrieve(JiraUrlRequest request)
        {
            var configuration = await _jiraConfigurationRepository.Get(request.ConfigurationId);

            return Option.Some(new JiraUrl(configuration.Url));
        }

        protected override async Task Store(JiraUrlRequest request, JiraUrl result)
        {
            var configuration = await _jiraConfigurationRepository.Get(request.ConfigurationId);
            configuration.Url = result.Url;

            await _jiraConfigurationRepository.Update(configuration);
        }

        protected override Task Clear(JiraUrlRequest request)
        {
            // Do nothing
            return Task.FromResult(false);
        }
    }
}