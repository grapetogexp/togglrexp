﻿namespace TogglrExp.Model.Jira.UrlProvision
{
    public class JiraUrl
    {
        private readonly string _url;

        public JiraUrl(string url)
        {
            _url = url;
        }

        public string Url
        {
            get { return _url; }
        }
    }
}