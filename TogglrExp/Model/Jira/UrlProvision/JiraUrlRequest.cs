﻿namespace TogglrExp.Model.Jira.UrlProvision
{
    public class JiraUrlRequest
    {
        public JiraUrlRequest(long configurationId)
        {
            ConfigurationId = configurationId;
        }

        public long ConfigurationId { get; private set; }
    }
}