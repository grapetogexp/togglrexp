﻿namespace TogglrExp.Model.Jira
{
    public class Configuration
    {
        public Configuration(
            long id, 
            string name, 
            string url, 
            string preferedIssue)
        {
            Id = id;
            Name = name;
            Url = url;
            PreferedIssue = preferedIssue;
        }

        public long Id { get; private set; }

        public string Name { get; private set; }

        public string Url { get; private set; }

        public string PreferedIssue { get; private set; }
    }
}