﻿namespace TogglrExp.Model.Jira
{
    public class JiraIssueModel
    {
        public JiraIssueModel(
            long configurationId,
            string key, 
            string summary)
        {
            ConfigurationId = configurationId;
            Key = key;
            Summary = summary;
        }

        public string Key { get; private set; }

        public string Summary { get; private set; }

        public long ConfigurationId { get; private set; }

        public override string ToString()
        {
            return string.Format("JiraIssueModel [Key ({0})]", Key);
        }
    }
}