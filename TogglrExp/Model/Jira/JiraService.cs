﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Http;
using TogglrExp.Model.Jira.ConfigurationStorage;
using TogglrExp.Model.Jira.Credentials;
using TogglrExp.Model.Jira.UrlProvision;
using TogglrExp.Model.Jira.WebDTOs;
using Tracer.Log4Net;
using JiraCredentials = TogglrExp.Model.Jira.WebDTOs.JiraCredentials;

namespace TogglrExp.Model.Jira
{
    public class JiraService : IJiraService
    {
        private readonly IAsyncOptionalProvider<JiraCredentialRequest, Credentials.JiraCredentials> _credentialsProvider;
        private readonly IAsyncOptionalProvider<JiraUrlRequest, JiraUrl> _jiraUrlProvider;
        private readonly IHttpProxyFactory _httpProxyFactory;
        private readonly IJiraConfigurationRepository _jiraConfigurationRepository;

        public JiraService(
            IAsyncOptionalProvider<JiraCredentialRequest, Credentials.JiraCredentials> credentialsProvider,
            IAsyncOptionalProvider<JiraUrlRequest, JiraUrl> jiraUrlProvider,
            IHttpProxyFactory httpProxyFactory,
            IJiraConfigurationRepository jiraConfigurationRepository)
        {
            _credentialsProvider = credentialsProvider;
            _jiraUrlProvider = jiraUrlProvider;
            _httpProxyFactory = httpProxyFactory;
            _jiraConfigurationRepository = jiraConfigurationRepository;
        }
        
        public async Task Report(IEnumerable<Worklog> worklogs, JiraIssueModel issue, CancellationToken cancellationToken)
        {
            var workLogsArray = worklogs as Worklog[] ?? worklogs.ToArray();
            Log.Info($"Reporting of worklog Amount:{workLogsArray.Length}");

            var proxyOption = await CreateProxy(issue.ConfigurationId, cancellationToken);

            if (proxyOption.IsNone)
            {
                return;
            }

            using (var proxy = proxyOption.Value)
            {
                var userResp = await proxy.Get<JiraUser>("rest/api/2/myself");
                var user = userResp.Body;

                foreach (var worklog in workLogsArray)
                {
                    var jiraWorkLog = new JiraWorkLog
                    {
                        Author = user,
                        UpdateAuthor = user,
                        Comment = worklog.Comment,
                        Started = worklog.Date.ToUniversalTime(),
                        TimeSpentSeconds = (int)worklog.TimeSpent.TotalSeconds
                    };

                    var url = string.Format("rest/api/2/issue/{0}/worklog", issue.Key);

                    var resp = await proxy.Post<JiraWorkLog, JiraWorkLog>(url, jiraWorkLog);
                    if (resp.StatusCode != HttpStatusCode.Created)
                    {
                        throw new Exception("Something gone wrong");
                    }
                }
                WorklogReported?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler WorklogReported;
        
        public async Task<IEnumerable<Worklog>> Get(DateTime dateFrom, DateTime dateTo, JiraIssueModel issue, CancellationToken cancellationToken)
        {
            Log.Info($"Worklog retrieval date range from {dateFrom} to {dateTo} started");
            var proxyOption = await CreateProxy(issue.ConfigurationId, cancellationToken);

            if (proxyOption.IsNone)
            {
                return Enumerable.Empty<Worklog>();
            }
            using (var proxy = proxyOption.Value)
            {
                var url = $"rest/api/2/issue/{issue.Key}/worklog";

                var resp = await proxy.Get<JiraWorkLogList>(url);
                if (resp.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception("Something gone wrong");
                }

                Log.Info($"Worklog retrieval Worklog date range from {dateFrom} to {dateTo} finished");
                return (from wl in resp.Body.Worklogs
                    let timeSpent = TimeSpan.FromSeconds(wl.TimeSpentSeconds)
                    where wl.Started >= dateFrom && wl.Started <= dateTo
                    select new Worklog(wl.Started, timeSpent, wl.Comment, wl.Comment)).ToList();
            }
        }
        
        public async Task<IEnumerable<Configuration>> GetConfigurations(CancellationToken cancellationToken)
        {
            Log.Info("JIRA Configurations retrieval started");
            var result = await _jiraConfigurationRepository.GetAll();
            Log.Info("JIRA Configurations retrieval completed");
            return result.Select(o => new Configuration(o.Id, o.Name, o.Url, o.PreferedIssue));
        }

        public async Task<ValidationResult> AddConfiguration(ConfigrationPrototype prototype)
        {
            Log.Info("Adding new JIRA configuration started");
            var result = await ValidatePrototype(prototype);

            if (result.IsValid)
            {
                var dto = new ConfigurationDTO
                {
                    Name = prototype.Name,
                    Url = prototype.Url
                };
                await _jiraConfigurationRepository.Insert(dto);
                Log.Info("Configuration added");
            }
            else
            {
                Log.Info("Added configuration didn't passed validation");
            }
            return result;
        }

        public async Task<Configuration> GetConfiguration(long id)
        {
            var dto = await _jiraConfigurationRepository.Get(id);
            return new Configuration(dto.Id, dto.Name, dto.Url, dto.PreferedIssue);
        }

        public async Task<ValidationResult> UpdateConfiguration(long id, ConfigrationPrototype prototype)
        {
            Log.Info("Adding new JIRA configuration started");
            var result = await ValidatePrototype(prototype);

            if (result.IsValid)
            {
                var dto = await _jiraConfigurationRepository.Get(id);
                dto.Id = id;
                dto.Name = prototype.Name;
                dto.Url = prototype.Url;

                await _jiraConfigurationRepository.Update(dto);
                Log.Info("Configuration added");
            }
            else
            {
                Log.Info("Added configuration didn't passed validation");
            }
            return result;
        }

        public Task DeleteConfiguration(long id)
        {
            return _jiraConfigurationRepository.Delete(id);
        }

        public async Task<string> GetPreferedIssueKey(long id)
        {
            var dto = await _jiraConfigurationRepository.Get(id);
            return dto.PreferedIssue;
        }

        public async Task SetPreferedIssueKey(long id, string key)
        {
            var dto = await _jiraConfigurationRepository.Get(id);
            dto.PreferedIssue = key;
            await _jiraConfigurationRepository.Update(dto);
        }

        public async Task<JiraIssueModel> GetIssue(long configurationId, string key, CancellationToken cancellationToken)
        {
            Log.Info($"Issues retrieval started for configuration {configurationId}");

            var proxyOption = await CreateProxy(configurationId, cancellationToken);

            if (proxyOption.IsNone)
            {
                return null;
            }

            using (var proxy = proxyOption.Value)
            {
                var url = $"rest/api/2/issue/{key}?fields=summary";
                
                var resp = await proxy.Get<JiraIssue>(url);

                Log.Info($"Issue retrieval finished for configuration {configurationId}");
                if (resp.StatusCode != HttpStatusCode.OK)
                {
                    return null;
                }
                return new JiraIssueModel(
                        configurationId,
                        resp.Body.Key,
                        Convert.ToString(resp.Body.Fields["summary"]));
            }
        }

        public async Task<IEnumerable<JiraIssueModel>> GetIssues(long configurationId, string query, CancellationToken cancellationToken)
        {
            Log.Info($"Issues retrieval started for configuration {configurationId}");

            var proxyOption = await CreateProxy(configurationId, cancellationToken);

            if (proxyOption.IsNone)
            {
                return Enumerable.Empty<JiraIssueModel>();
            }
            using (var proxy = proxyOption.Value)
            {
                var url = "rest/api/2/search";
                var req = new JiraIssueListRequest
                {
                    Jql = query,
                    Fields = new[] {"summary"},
                    MaxResults = 50, // TODO: Parametrize it
                    StartAt = 0 // TODO: As above
                };
                var resp = await proxy.Post<JiraIssueListResponse, JiraIssueListRequest>(url, req);

                Log.Info($"Issues retrieval finished for configuration {configurationId}");
                if (resp.StatusCode != HttpStatusCode.OK)
                {
                    return Enumerable.Empty<JiraIssueModel>();
                }
                return from ji in resp.Body.Issues
                    select new JiraIssueModel(
                        configurationId,
                        ji.Key, 
                        Convert.ToString(ji.Fields["summary"]));
            }
        }

        private async Task<Option<IHttpProxy>> CreateProxy(long configurationId, CancellationToken cancellationToken)
        {
            var jiraUrl = await _jiraUrlProvider.Provide(
                new JiraUrlRequest(configurationId),
                IsJiraUrlValid,
                cancellationToken);

            if (jiraUrl.IsNone)
            {
                return Option<IHttpProxy>.None;
            }

            var baseAddress = CleanUpSlashes(jiraUrl.Value.Url);

            var result = _httpProxyFactory.Create(baseAddress);
            var request = new JiraCredentialRequest(configurationId);
            var passwordResponse = await _credentialsProvider.Provide(request, async cred =>
            {
                var credDto = new JiraCredentials
                {
                    UserName = cred.Username,
                    Password = cred.Password
                };

                var resp = await result.Post<JiraLoginResponse, JiraCredentials>("rest/auth/1/session", credDto);

                return resp.StatusCode == HttpStatusCode.OK;
            }, cancellationToken);

            return passwordResponse.Map(o => result);
        }

        private async Task<ValidationResult> ValidatePrototype(ConfigrationPrototype configrationPrototype)
        {
            var errors = new List<string>();
            var name = configrationPrototype.Name;
            if (string.IsNullOrEmpty(name))
            {
                errors.Add("Nazwa jest wymagana");
            }
            else if (name.Length > 50)
            {
                errors.Add("Nazwa nie może być dłuższa niż 50 znaków");
            }

            var url = configrationPrototype.Url;
            if (string.IsNullOrEmpty(url))
            {
                errors.Add("Adres www jest wymagany");
            }
            else if (! await IsJiraUrlValid(url))
            {
                errors.Add("Adres www jest niepoprawny");
            }

            return new ValidationResult(errors);
        }

        private Task<bool> IsJiraUrlValid(JiraUrl jiraUrl)
        {
            return IsJiraUrlValid(jiraUrl.Url);
        }

        private async Task<bool> IsJiraUrlValid(string url)
        {
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                return false;
            }

            var uri = new Uri(url, UriKind.Absolute);
            if (uri.Scheme != Uri.UriSchemeHttp && uri.Scheme != Uri.UriSchemeHttps)
            {
                return false;
            }

            var baseAddress = CleanUpSlashes(url);
            var httpProxy = _httpProxyFactory.Create(baseAddress);
            try
            {
                var httpRespose = await httpProxy.Test("");

                return httpRespose.StatusCode == HttpStatusCode.OK;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static string CleanUpSlashes(string url)
        {
            return url.TrimEnd('/') + "/";
        }   
    }
}