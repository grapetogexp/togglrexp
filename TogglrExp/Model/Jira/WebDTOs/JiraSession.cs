﻿using Newtonsoft.Json;

namespace TogglrExp.Model.Jira.WebDTOs
{
    [JsonObject]
    public class JiraSession
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}