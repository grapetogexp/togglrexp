﻿using Newtonsoft.Json;

namespace TogglrExp.Model.Jira.WebDTOs
{
    [JsonObject]
    public class JiraLoginResponse
    {
        [JsonProperty("session")]
        public JiraSession Session { get; set; }
        [JsonProperty("loginInfo")]
        public JiraLoginInfo LoginInfo { get; set; }
        
    }
}