﻿using Newtonsoft.Json;

namespace TogglrExp.Model.Jira.WebDTOs
{
    [JsonObject]
    public class JiraUser
    {
        [JsonProperty("self")]
        public string Self { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("avatarUrls")]
        public JiraAvatarUrls AvatarUrls { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        //[JsonProperty("timeZone")]
        //public string TimeZone { get; set; }

        // Those properties are not necessary for now
        //public Groups groups { get; set; }
        //public ApplicationRoles applicationRoles { get; set; }
        //public string Expand { get; set; }
    }
}