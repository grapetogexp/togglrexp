﻿using Newtonsoft.Json;

namespace TogglrExp.Model.Jira.WebDTOs
{
    [JsonObject]
    public class JiraIssueListResponse
    {
        [JsonProperty("Expand")]
        public string Expand { get; set; }
        [JsonProperty("startAt")]
        public int StartAt { get; set; }
        [JsonProperty("maxResults")]
        public int MaxResults { get; set; }
        [JsonProperty("total")]
        public int Total { get; set; }
        [JsonProperty("issues")]
        public JiraIssue[] Issues { get; set; }
    }
}