﻿using System;
using Newtonsoft.Json;

namespace TogglrExp.Model.Jira.WebDTOs
{
    [JsonObject]
    public class JiraLoginInfo
    {
        [JsonProperty("failedLoginCount")]
        public int FailedLoginCount { get; set; }

        [JsonProperty("loginCount")]
        public int LoginCount { get; set; }

        [JsonProperty("lastFailedLoginTime")]
        public DateTime LastFailedLoginTime { get; set; }

        [JsonProperty("previousLoginTime")]
        public DateTime PreviousLoginTime { get; set; }
    }
}