﻿using System;
using Newtonsoft.Json;

namespace TogglrExp.Model.Jira.WebDTOs
{
    [JsonObject]
    public class JiraWorkLog
    {
        [JsonProperty("self")]
        public string Self { get; set; }

        [JsonProperty("author")]
        public JiraUser Author { get; set; }

        [JsonProperty("updateAuthor")]
        public JiraUser UpdateAuthor { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("created")]
        public DateTime Created { get; set; }

        [JsonProperty("updated")]
        public DateTime Updated { get; set; }

        [JsonProperty("started")]
        public DateTime Started { get; set; }

        [JsonProperty("timeSpent")]
        public string TimeSpent { get; set; }

        [JsonProperty("timeSpentSeconds")]
        public int TimeSpentSeconds { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}