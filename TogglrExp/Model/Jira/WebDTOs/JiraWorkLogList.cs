﻿using Newtonsoft.Json;

namespace TogglrExp.Model.Jira.WebDTOs
{
    [JsonObject]
    public class JiraWorkLogList
    {
        [JsonProperty("StartAt")]
        public int StartAt { get; set; }

        [JsonProperty("MaxResults")]
        public int MaxResults { get; set; }

        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("worklogs")]
        public JiraWorkLog[] Worklogs { get; set; }
    }
}