﻿namespace TogglrExp.Model.Jira.Credentials
{
    public class JiraCredentialRequest
    {
        public JiraCredentialRequest(long credentialsId)
        {
            CredentialsId = credentialsId;
        }

        public long CredentialsId { get; private set; }
    }
}