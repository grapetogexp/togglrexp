﻿using System.Threading.Tasks;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Jira.ConfigurationStorage;
using TogglrExp.Model.Secrets;

namespace TogglrExp.Model.Jira.Credentials
{
    public class JiraCredentialsProvider: StoredOptionalSampleProvider<JiraCredentialRequest, JiraCredentials>
    {
        private readonly IJiraConfigurationRepository _configurationRepository;
        private readonly IProtector _protector;

        public JiraCredentialsProvider(
            IAsyncOptionalStoredSampleProvider<JiraCredentialRequest, JiraCredentials> innerProvider,
            IJiraConfigurationRepository configurationRepository,
            IProtector protector) 
            : base(innerProvider)
        {
            _configurationRepository = configurationRepository;
            _protector = protector;
        }

        protected override async Task<Option<JiraCredentials>> Retrieve(JiraCredentialRequest param)
        {
            var encrypted = await _configurationRepository.GetCredentials(param.CredentialsId);
            if (encrypted == null)
            {
                return Option<JiraCredentials>.None;
            }
            var credentials = _protector.Unprotect<CreadentialsStoreModel>(encrypted);

            return Option.Some(new JiraCredentials(credentials.Username, credentials.Password));
        }

        protected override Task Store(JiraCredentialRequest param, JiraCredentials result)
        {
            var credentialStoreModel = new CreadentialsStoreModel
            {
                Username = result.Username,
                Password = result.Password
            };

            var protectedBytes = _protector.Protect(credentialStoreModel);
            return _configurationRepository.UpdateCredentials(param.CredentialsId, protectedBytes);
        }

        protected override Task Clear(JiraCredentialRequest param)
        {
            return _configurationRepository.UpdateCredentials(param.CredentialsId, null);
        }
    }
}