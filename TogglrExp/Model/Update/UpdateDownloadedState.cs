﻿using System.Diagnostics;
using System.Windows;
using TracerAttributes;

namespace TogglrExp.Model.Update
{
    [TraceOn(TraceTarget.Public)]
    public class UpdateDownloadedState : IUpdateDownloadedState
    {
        private readonly string _downloadLocation;

        public UpdateDownloadedState(string downloadLocation)
        {
            _downloadLocation = downloadLocation;
        }

        [TraceOn]
        public void Install()
        {
            Process.Start(_downloadLocation);
            Application.Current.Shutdown();
        }
    }
}