﻿using Newtonsoft.Json;

namespace TogglrExp.Model.Update
{
    [JsonObject]
    public class UpdateInfo
    {
        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }
    }
}