﻿using System;
using System.Net;
using System.Threading.Tasks;
using TogglrExp.Model.Http;
using Tracer.Log4Net;

namespace TogglrExp.Model.Update
{
    public class NoUpdateState : INoUpdateState
    {
        private readonly UpdateService _updateService;
        private readonly Func<UpdateInfo, UpdateService, IUpdateAvailableState> _updateAvailableFactory;
        private readonly IHttpProxyFactory _httpProxyFactory;

        public NoUpdateState(
            UpdateService updateService,
            Func<UpdateInfo, UpdateService, IUpdateAvailableState> updateAvailableFactory,
            IHttpProxyFactory httpProxyFactory)
        {
            _updateService = updateService;
            _updateAvailableFactory = updateAvailableFactory;
            _httpProxyFactory = httpProxyFactory;
            PoolUpdate();
        }

        private async void PoolUpdate()
        {
            var currentVersion = typeof(UpdateService).Assembly.GetName().Version;

            await Task.Delay(TimeSpan.FromSeconds(10));

            UpdateInfo updateInfo = null;
            using (var httpProxy = _httpProxyFactory.Create())
            {
                do
                {
                    try
                    {
                        HttpResponse<UpdateInfo> response =
                            await httpProxy.Get<UpdateInfo>("http://szogun-ui.pl/togglr-exp/version.json");
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            Log.Debug("Update server returned non-200 response");
                        }

                        var info = response.Body;

                        var upToDateVersion = new Version(info.Version);
                        if (upToDateVersion.CompareTo(currentVersion) > 0)
                        {
                            Log.Debug("Update available");
                            updateInfo = info;
                        }
                        else
                        {
                            await Task.Delay(TimeSpan.FromMinutes(10));
                        }
                    }
                    catch (Exception)
                    {
                        Log.Error("Error durring downloading version info");
                        await Task.Delay(TimeSpan.FromMinutes(10));
                    }
                } while (updateInfo == null);
            }
            _updateService.State = _updateAvailableFactory(updateInfo, _updateService);
        }
    }
}