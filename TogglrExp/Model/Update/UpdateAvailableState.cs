﻿using System;
using TracerAttributes;

namespace TogglrExp.Model.Update
{
    [TraceOn(TraceTarget.Public)]
    public class UpdateAvailableState : IUpdateAvailableState
    {
        private readonly UpdateInfo _updateInfo;
        private readonly UpdateService _updateService;
        private readonly Func<UpdateInfo, UpdateService, IUpdateDownloadingState> _updateDownloadingFactory;

        public UpdateAvailableState(
            UpdateInfo updateInfo,
            UpdateService updateService,
            Func<UpdateInfo, UpdateService, IUpdateDownloadingState> updateDownloadingFactory)
        {
            _updateInfo = updateInfo;
            _updateService = updateService;
            _updateDownloadingFactory = updateDownloadingFactory;
        }

        public string Message { get { return _updateInfo.Message; } }

        [TraceOn]
        public void Download()
        {
            _updateService.State = _updateDownloadingFactory(_updateInfo, _updateService);
        }
    }
}