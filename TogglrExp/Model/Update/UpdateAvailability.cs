﻿namespace TogglrExp.Model.Update
{
    public class UpdateAvailability
    {
        public UpdateAvailability(string message, string location)
        {
            Message = message;
            Location = location;
        }
        
        public string Message { get; private set; }
        
        public string Location { get; private set; } 
    }
}