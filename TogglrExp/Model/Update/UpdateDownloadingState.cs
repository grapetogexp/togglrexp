﻿using System;
using System.IO;
using TogglrExp.Model.Http;

namespace TogglrExp.Model.Update
{
    public class UpdateDownloadingState : IUpdateDownloadingState
    {
        private readonly UpdateInfo _updateInfo;
        private readonly UpdateService _updateService;
        private readonly IHttpProxyFactory _httpProxyFactory;
        private readonly Func<string, IUpdateDownloadedState> _updateDownloadedFactory;
        private readonly Func<UpdateInfo, string, UpdateService, IUpdateDownloadFailedState> _updateDownloadFailedFactory;

        public UpdateDownloadingState(
            UpdateInfo updateInfo,
            UpdateService updateService,
            IHttpProxyFactory httpProxyFactory,
            Func<string, IUpdateDownloadedState> updateDownloadedFactory,
            Func<UpdateInfo, string, UpdateService, IUpdateDownloadFailedState> updateDownloadFailedFactory)
        {
            _updateInfo = updateInfo;
            _updateService = updateService;
            _httpProxyFactory = httpProxyFactory;
            _updateDownloadedFactory = updateDownloadedFactory;
            _updateDownloadFailedFactory = updateDownloadFailedFactory;

            Download();
        }

        private async void Download()
        {
            var tempPath = Path.GetTempPath();
            var fileName = Guid.NewGuid() + ".exe";
            var file = Path.Combine(tempPath, fileName);

            try
            {
                using (var httpProxy = _httpProxyFactory.Create())
                {
                    await httpProxy.DownloadFile(_updateInfo.Location, file);
                    _updateService.State = _updateDownloadedFactory(file);
                }
            }
            catch (Exception e)
            {
                _updateService.State = _updateDownloadFailedFactory(_updateInfo, e.Message, _updateService);
            }
        }
    }
}