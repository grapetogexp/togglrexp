﻿using System;

namespace TogglrExp.Model.Update
{
    public interface IUpdateService
    {
        IUpdateState State { get; }

        event EventHandler StateChanged;
    }
}