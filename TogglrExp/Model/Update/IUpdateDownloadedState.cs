﻿namespace TogglrExp.Model.Update
{
    public interface IUpdateDownloadedState : IUpdateState
    {
        void Install();
    }
}