﻿using System;

namespace TogglrExp.Model.Update
{
    public class UpdateDownloadFailedState : IUpdateDownloadFailedState
    {
        private readonly UpdateInfo _updateInfo;
        private readonly UpdateService _updateService;
        private readonly Func<UpdateInfo, UpdateService, IUpdateDownloadingState> _updateDownloadingStateFactory;

        public UpdateDownloadFailedState(
            UpdateInfo updateInfo,
            string errorMessage,
            UpdateService updateService,
            Func<UpdateInfo, UpdateService, IUpdateDownloadingState> updateDownloadingStateFactory)
        {
            _updateInfo = updateInfo;
            _updateService = updateService;
            _updateDownloadingStateFactory = updateDownloadingStateFactory;
            Messsage = errorMessage;
        }

        public string Messsage { get; private set; }
        public void Retry()
        {
            _updateService.State = _updateDownloadingStateFactory(_updateInfo, _updateService);
        }
    }
}