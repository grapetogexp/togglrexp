﻿namespace TogglrExp.Model.Update
{
    public interface IUpdateAvailableState : IUpdateState
    {
        string Message { get; }

        void Download();
    }
}