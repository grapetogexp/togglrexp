﻿namespace TogglrExp.Model.Update
{
    public interface IUpdateDownloadFailedState : IUpdateState
    {
        string Messsage { get; }

        void Retry();
    }
}