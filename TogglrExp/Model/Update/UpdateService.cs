﻿using System;
using TracerAttributes;

namespace TogglrExp.Model.Update
{
    
    public class UpdateService : IUpdateService
    {
        public UpdateService(Func<UpdateService, INoUpdateState> noUpdateStateFactory)
        {
            State = noUpdateStateFactory(this);
        }
        
        private IUpdateState _state;

        [TraceOn]
        public IUpdateState State
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _state = value;
                    StateChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler StateChanged;
    }
}