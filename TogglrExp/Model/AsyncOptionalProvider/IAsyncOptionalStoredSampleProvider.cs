﻿namespace TogglrExp.Model.AsyncOptionalProvider
{
    public interface IAsyncOptionalStoredSampleProvider<TParam, TResult>
        : IAsyncOptionalSampleProvider<StoredSampleParam<TParam, TResult>, StoredSample<TResult>>
    {
        
    }
}