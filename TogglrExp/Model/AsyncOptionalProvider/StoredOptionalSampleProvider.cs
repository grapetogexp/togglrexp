﻿using System.Threading;
using System.Threading.Tasks;

namespace TogglrExp.Model.AsyncOptionalProvider
{
    public abstract class StoredOptionalSampleProvider<TParam, TResult> : IAsyncOptionalSampleProvider<TParam, TResult>
    {
        private readonly IAsyncOptionalStoredSampleProvider<TParam, TResult> _innerProvider;

        public StoredOptionalSampleProvider(
            IAsyncOptionalStoredSampleProvider<TParam, TResult> innerProvider)
        {
            _innerProvider = innerProvider;
        }
        
        public async Task<Option<TResult>> Provide(TParam parameter, int trialNumber, CancellationToken cancellationToken)
        {
            var stored = await Retrieve(parameter);
            
            if (stored.IsNone)
            {
                return await ProvideFromInner(parameter, Option<TResult>.None, trialNumber, cancellationToken);
            }

            if (trialNumber == 0)
            {
                return stored;
            }

            return await ProvideFromInner(parameter, stored, trialNumber - 1, cancellationToken);
        }

        private async  Task<Option<TResult>> ProvideFromInner(
            TParam parameter, 
            Option<TResult> previous,
            int trialNumber, 
            CancellationToken cancellationToken)
        {
            var result = await _innerProvider
                .Provide(StoredSampleParam.Create(parameter, previous), trialNumber, cancellationToken);

            if (!result.IsNone)
            {
                var value = result.Value;
                if (value.IsSaveRequested)
                {
                    await Store(parameter, value.Sample);
                }
                else
                {
                    await Clear(parameter);
                }
            }
            else
            {
                await Clear(parameter);
            }
            return result.Map(o => o.Sample);
        }

        protected abstract Task<Option<TResult>> Retrieve(TParam param);

        protected abstract Task Store(TParam param, TResult result);

        protected abstract Task Clear(TParam param);
    }
}