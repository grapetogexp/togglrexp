﻿namespace TogglrExp.Model.AsyncOptionalProvider
{
    public static class StoredSampleParam
    {
        public static StoredSampleParam<TParam, TResult> Create<TParam, TResult>(TParam param, Option<TResult> storedResult)
        {
            return new StoredSampleParam<TParam, TResult>(param, storedResult);
        }
    }

    public class StoredSampleParam<TParam, TResult>
    {
        public StoredSampleParam(TParam param, Option<TResult> storedResult)
        {
            Param = param;
            StoredResult = storedResult;
        }

        public TParam Param { get; private set; }

        public Option<TResult> StoredResult { get; private set; }
    }
}