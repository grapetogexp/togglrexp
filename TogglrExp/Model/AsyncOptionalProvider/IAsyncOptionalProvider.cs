﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TogglrExp.Model.AsyncOptionalProvider
{
    public interface IAsyncOptionalProvider<in TParam, TResult>
    {
        Task<Option<TResult>> Provide(TParam parameter, Func<TResult, Task<bool>> asyncValidator, CancellationToken cancellationToken);
    }
}