﻿using System.Threading;
using System.Threading.Tasks;

namespace TogglrExp.Model.AsyncOptionalProvider
{
    public interface IAsyncOptionalSampleProvider<in TParam, TResult>
    {
        Task<Option<TResult>> Provide(TParam parameter, int trialNumber, CancellationToken cancellationToken);
    }
}