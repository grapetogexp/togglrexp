﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TogglrExp.Model.AsyncOptionalProvider
{
    public static class AsyncOptionalProvider
    {
        public static Task<Option<TResult>> Provide<TResult, TParam>(
            this IAsyncOptionalProvider<TParam, TResult> provider,
            TParam parameter,
            Func<TResult, Task<bool>> asyncValidator)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            return provider.Provide(parameter, asyncValidator, cancellationTokenSource.Token);
        }
    }

    public class AsyncOptionalProvider<TParam, TResult> : IAsyncOptionalProvider<TParam, TResult>
    {
        private readonly IAsyncOptionalSampleProvider<TParam, TResult> _sampleProvider;

        public AsyncOptionalProvider(IAsyncOptionalSampleProvider<TParam, TResult> sampleProvider)
        {
            _sampleProvider = sampleProvider;
        }

        public async Task<Option<TResult>> Provide(TParam parameter, Func<TResult, Task<bool>> asyncValidator, CancellationToken cancellationToken)
        {
            int trialCount = 0;
            bool isValid = false;
            Option<TResult> result;
            do
            {
                var sample = await _sampleProvider.Provide(parameter, trialCount, cancellationToken);
                if (cancellationToken.IsCancellationRequested)
                {
                    return Option<TResult>.None;
                }
                if (sample.IsNone)
                {
                    return Option<TResult>.None;
                }
                isValid = await asyncValidator(sample.Value);
                if (cancellationToken.IsCancellationRequested)
                {
                    return Option<TResult>.None;
                }
                result = sample;
                trialCount++;
            } while (!isValid);

            return result;
        }
    }
}