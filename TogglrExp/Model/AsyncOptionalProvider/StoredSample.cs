﻿namespace TogglrExp.Model.AsyncOptionalProvider
{
    public static class StoredSample
    {
        public static StoredSample<T> Persisted<T>(T value)
        {
            return new StoredSample<T>(value, true);
        }

        public static StoredSample<T> Volatile<T>(T value)
        {
            return new StoredSample<T>(value, false);
        }
    }

    public class StoredSample<T>
    {
        public StoredSample(T sample, bool isSaveRequested)
        {
            Sample = sample;
            IsSaveRequested = isSaveRequested;
        }

        public T Sample { get; private set; }

        public bool IsSaveRequested { get; private set; }
    }
}