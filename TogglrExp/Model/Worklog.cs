﻿using System;

namespace TogglrExp.Model
{
    public class Worklog
    {
        public Worklog(
            DateTime date, 
            TimeSpan timeSpent,
            string header,
            string comment)
        {
            Date = date;
            TimeSpent = timeSpent;
            Header = header;
            Comment = comment;
        }

        public string Header { get; }

        public DateTime Date { get; }

        public string Comment { get; }

        public TimeSpan TimeSpent { get; }
        
        public override string ToString()
        {
            return string.Format("WorkLog [Date ({0}) TimeSpent ({1})]", Date, TimeSpent);
        }

        protected bool Equals(Worklog other)
        {
            return Date.Equals(other.Date) && string.Equals(Comment, other.Comment) && TimeSpent.Equals(other.TimeSpent);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Worklog) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode*397) ^ (Comment != null ? Comment.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ TimeSpent.GetHashCode();
                return hashCode;
            }
        }
    }
}