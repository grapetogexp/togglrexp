﻿using System.Threading.Tasks;
using TogglrExp.Model.DbUpdate;

namespace TogglrExp.ViewModel.DesignTime
{
    public class DbUpdateService : IDbUpdateService
    {
        public Task UpdateDataBase()
        {
            return Task.FromResult(1);
        }
    }
}