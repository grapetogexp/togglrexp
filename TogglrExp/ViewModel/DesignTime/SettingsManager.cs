﻿using System;
using TogglrExp.Model.Settings;

namespace TogglrExp.ViewModel.DesignTime
{
    public class SettingsManager : ISettingsManager
    {
        public Settings Get()
        {
            return new Settings(
                "Token",
                true,
                true,
                false,
                true,
                null);
        }

        public void Modify(Action<ModableSettings> modificationAction)
        {
            
        }
    }
}