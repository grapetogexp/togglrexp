﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TogglrExp.Helpers;
using TogglrExp.Model;
using TogglrExp.Model.Holiday;

namespace TogglrExp.ViewModel.DesignTime
{
    public class HolidayService : IHolidayService
    {
        public Task<IEnumerable<Model.Holiday.Holiday>> HolidayList(DateTime dateFrom, DateTime dateTo)
        {
            return Task.FromResult(HolidayListInternal(dateFrom, dateTo));
        }

        public Task Delete(DateTime dateTime)
        {
            return Task.FromResult(true);
        }

        public Task<ValidationResult> Add(Model.Holiday.Holiday holiday)
        {
            return Task.FromResult(new ValidationResult(new [] { "Błąd" }));
        }

        public Task FillWeekends(int year)
        {
            return Task.FromResult(true);
        }

        private IEnumerable<Model.Holiday.Holiday> HolidayListInternal(DateTime dateFrom, DateTime dateTo)
        {
            return from day in DateTimeExtesions.Range(dateFrom, dateTo, TimeSpan.FromDays(4))
                select new Model.Holiday.Holiday(day, "WOLNE");
        }
    }
}