﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TogglrExp.Model;
using TogglrExp.Model.Jira;

namespace TogglrExp.ViewModel.DesignTime
{
    public class JiraService : IJiraService
    {
        public Task Report(IEnumerable<Worklog> worklog, JiraIssueModel issue, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }

        public event EventHandler WorklogReported;

        public Task<IEnumerable<Worklog>> Get(DateTime dateFrom, DateTime dateTo, JiraIssueModel issue, CancellationToken cancellationToken)
        {
            return Task.FromResult(GetInternal(dateFrom));
        }

        public Task<IEnumerable<Configuration>> GetConfigurations(CancellationToken cancellationToken)
        {
            return Task.FromResult(GetConfigurationsInternal());
        }
        
        public Task<ValidationResult> AddConfiguration(ConfigrationPrototype prototype)
        {
            return Task.FromResult(ValidationResult.Valid);
        }

        public Task<Configuration> GetConfiguration(long id)
        {
            return Task.FromResult(new Configuration(id, "Some JIRA conf", "http://jira.com", string.Empty));
        }

        public Task<ValidationResult> UpdateConfiguration(long id, ConfigrationPrototype prototype)
        {
            return Task.FromResult(ValidationResult.Valid);
        }

        public Task DeleteConfiguration(long id)
        {
            return Task.FromResult(false);
        }

        public Task<string> GetPreferedIssueKey(long id)
        {
            return Task.FromResult(string.Empty);
        }

        public Task SetPreferedIssueKey(long id, string key)
        {
            return Task.FromResult(false);
        }

        public Task<JiraIssueModel> GetIssue(long configurationId, string key, CancellationToken cancellationToken)
        {
            return Task.FromResult(new JiraIssueModel(1, "KEY-123", "Summary"));
        }

        public Task<IEnumerable<JiraIssueModel>> GetIssues(long configurationId, string query, CancellationToken cancellationToken)
        {
            return Task.FromResult(GetIssuesInternal(configurationId));
        }

        private IEnumerable<Worklog> GetInternal(DateTime dateFrom)
        {
            yield return new Worklog(dateFrom, TimeSpan.FromHours(2), "Lorem ipsum", "Lorem ipsum");
        }

        private IEnumerable<Configuration> GetConfigurationsInternal()
        {
            yield return new Configuration(1, "Jakaś JIRA", "http://jira.com", string.Empty);
        }

        private IEnumerable<JiraIssueModel> GetIssuesInternal(long configurationId)
        {
            yield return new JiraIssueModel(configurationId, "KEY-123", "Summary");
        }

    }
}