﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TogglrExp.Model.Toggl;

namespace TogglrExp.ViewModel.DesignTime
{
    public class TaskSource : ITaskSource
    {
        

        public Task<IEnumerable<WorkspaceItem>> GetWorkspaces(CancellationToken cancellationToken)
        {
            return Task.FromResult(GetWorkspacesInternal());
        }

        private IEnumerable<WorkspaceItem> GetWorkspacesInternal()
        {
            yield return new WorkspaceItem(1, "1 Grupa");
            yield return new WorkspaceItem(2, "2 Grupa");
            yield return new WorkspaceItem(3, "3 Grupa");
        }

        public Task<IEnumerable<ClientItem>> GetClients(int workspaceId, CancellationToken cancellationToken)
        {
            return Task.FromResult(GetClientsInternal());
        }

        private IEnumerable<ClientItem> GetClientsInternal()
        {
            yield return new ClientItem(1, "Klient 1");
            yield return new ClientItem(2, "Klient 2");
            yield return new ClientItem(3, "Klient 3");
        }

        public Task<IEnumerable<ProjectItem>> GetProjects(int workspaceId, int? clientId, CancellationToken cancellationToken)
        {
            return Task.FromResult(GetProjectsInternal());
        }

        private IEnumerable<ProjectItem> GetProjectsInternal()
        {
            yield return new ProjectItem(1, "Projekt 1");
            yield return new ProjectItem(2, "Projekt 2");
            yield return new ProjectItem(3, "Projekt 3");
        }

        public Task<IEnumerable<TaskItem>> GetItems(TaskFilter taskFilter, CancellationToken cancellationToken)
        {
            return Task.FromResult(GetItemsForDateInternal());
        }
        private IEnumerable<TaskItem> GetItemsForDateInternal()
        {
            yield return new TaskItem
            {
                Description = "Lorem ipsum",
                Duration = TimeSpan.FromMinutes(15),
                TimeFrom = new DateTime(2016, 10, 01, 08, 00, 00),
                TimeTo = new DateTime(2016, 10, 01, 08, 15, 00)
            };
            yield return new TaskItem
            {
                Description = "Lorem ipsum 2",
                Duration = TimeSpan.FromMinutes(15),
                TimeFrom = new DateTime(2016, 10, 01, 08, 15, 00),
                TimeTo = new DateTime(2016, 10, 01, 08, 30, 00)
            };
            yield return new TaskItem
            {
                Description = "Lorem ipsum dolor",
                Duration = TimeSpan.FromMinutes(15),
                TimeFrom = new DateTime(2016, 10, 02, 08, 15, 00),
                TimeTo = new DateTime(2016, 10, 02, 08, 30, 00)
            };
        }
    }
}