﻿using System;
using TogglrExp.Model.Update;

namespace TogglrExp.ViewModel.DesignTime
{
    public class UpdateService : IUpdateService
    {
        public IUpdateState State { get; }
        public event EventHandler StateChanged;
    }
}