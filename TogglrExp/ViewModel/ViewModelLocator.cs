﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Ninject;
using Ninject.Parameters;
using TogglrExp.Helpers;
using TogglrExp.Infrastructure;
using TogglrExp.Model;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Clipboard;
using TogglrExp.Model.DbUpdate;
using TogglrExp.Model.Holiday;
using TogglrExp.Model.Http;
using TogglrExp.Model.Jira;
using TogglrExp.Model.Jira.ConfigurationStorage;
using TogglrExp.Model.Jira.Credentials;
using TogglrExp.Model.Jira.UrlProvision;
using TogglrExp.Model.OvertimeReport;
using TogglrExp.Model.Secrets;
using TogglrExp.Model.Settings;
using TogglrExp.Model.TimeProvider;
using TogglrExp.Model.Toggl;
using TogglrExp.Model.Toggl.TokenProvision;
using TogglrExp.Model.Update;
using TogglrExp.ViewModel.About;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.DispatcherService;
using TogglrExp.ViewModel.Holiday;
using TogglrExp.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel.JiraCredntials;
using TogglrExp.ViewModel.JiraUrl;
using TogglrExp.ViewModel.Navigation;
using TogglrExp.ViewModel.NotificationCenter;
using TogglrExp.ViewModel.OvertimeReport;
using TogglrExp.ViewModel.Timetrack;
using TogglrExp.ViewModel.Token;
using TogglrExp.ViewModel.Update;
using Design = TogglrExp.ViewModel.DesignTime;
using JiraUrlModel = TogglrExp.Model.Jira.UrlProvision.JiraUrl;

namespace TogglrExp.ViewModel
{
    public class ViewModelLocator : IDisposable
    {
        private readonly IKernel _kernel;
        public ViewModelLocator()
        {
            _kernel = new StandardKernel();

            if (ViewModelBase.IsInDesignModeStatic)
            {
                InitDesignTimeServices();
            }
            else
            {
                InitServices();
            }

            _kernel.Bind<IDateTimeProvider>().To<RealDateTimeProvider>().InSingletonScope();
            
            _kernel
                .BindFactory<SettingsViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<HolidayViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<TogglWorklogViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<RepeatableTimeTrackViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<IEnumerable<Worklog>, WorklogPreviewViewModel>()
                .ToFactory("worklogs");

            _kernel
                .BindFactory<SaveCallback, JiraConfigurationDialog>()
                .ToFactory("saveCallback");

            _kernel
                .BindFactory<Configuration, JiraConfigurationListItem>()
                .ToFactory("configuration");

            _kernel
                .BindFactory<JiraConfigurationsViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<JiraConfigurationSelectorViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<JiraIssueListViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<IEnumerable<Worklog>, JiraWorklogViewModel>()
                .ToFactory("worklogs");

            _kernel
                .BindFactory<Configuration, SelectIssuesViewModel>()
                .ToFactory("configuration");

            _kernel
                .BindFactory<TimeTrackViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<OvertimeReportViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<AddHolidayViewModel>()
                .ToFactory();

            _kernel
                .BindFactory<Model.Holiday.Holiday, HolidayListItemViewModel>()
                .ToFactory("holiday");
            
            _kernel
                .BindFactory<string, QuestionViewModel>()
                .ToFactory("message");
            
            _kernel
                .BindFactory<AboutViewModel>()
                .ToFactory();
        }
        
        public MainWindowViewModel WindowViewModel
        {
            get { return _kernel.Get<MainWindowViewModel>(); }
        }

        private void InitServices()
        {
            _kernel.Bind<ISettingsManager>().To<SettingsManager>().InSingletonScope();
            _kernel.Bind<ITaskSource>().To<TaskSource>().InSingletonScope();
            _kernel.Bind<IClipboard>().To<Clipboard>().InSingletonScope();
            _kernel.Bind<IBusinessManager>().To<BusinessManager>().InSingletonScope();
            _kernel.Bind<IJiraService>().To<JiraService>().InSingletonScope();

            _kernel.Bind<IProtector>().To<Protector>().InSingletonScope();
            _kernel.Bind<IUriOpener>().To<UriOpener>().InSingletonScope();

            var passwordSource = _kernel.Get<PasswordSource>();

            _kernel
                .Bind<IAsyncOptionalStoredSampleProvider<JiraCredentialRequest, JiraCredentials>>()
                .ToConstant(passwordSource);

            _kernel
                .Bind<INotificationSource>()
                .ToConstant(passwordSource);

            _kernel
                .Bind<IAsyncOptionalSampleProvider<JiraCredentialRequest, JiraCredentials>>()
                .To<JiraCredentialsProvider>()
                .InSingletonScope();

            _kernel.Bind<INavigationService>().To<NavigationService>().InSingletonScope();

            _kernel.Bind<IHolidayService>().To<HolidayService>().InSingletonScope();
            _kernel.Bind<IHolidayRepository>().To<HolidayRepository>().InSingletonScope();

            _kernel.Bind<IOvertimeReportService>().To<OvertimeReportService>().InSingletonScope();

            _kernel.Bind<IMessenger>().To<Messenger>().InSingletonScope();

            _kernel.Bind<IDispatcherService>().To<DispatcherService.DispatcherService>().InSingletonScope();

            _kernel
                .Bind<IHttpProxyFactory>()
                .To<HttpProxyFactory>();

            _kernel
                .Bind<IUpdateService>()
                .To<UpdateService>()
                .InSingletonScope();

            _kernel
                .Bind<Func<UpdateService, INoUpdateState>>()
                .ToMethod(context =>
                    updateService =>
                        context.Kernel.Get<NoUpdateState>(new ConstructorArgument("updateService", updateService)));

            _kernel
                .Bind<Func<UpdateInfo, UpdateService, IUpdateAvailableState>>()
                .ToMethod(context =>
                    (updateInfo, updateService) =>
                        context.Kernel.Get<UpdateAvailableState>(
                            new ConstructorArgument("updateInfo", updateInfo),
                            new ConstructorArgument("updateService", updateService)));

            _kernel
                .Bind<Func<UpdateInfo, UpdateService, IUpdateDownloadingState>>()
                .ToMethod(context =>
                    (updateInfo, updateService) =>
                        context.Kernel.Get<UpdateDownloadingState>(
                            new ConstructorArgument("updateInfo", updateInfo),
                            new ConstructorArgument("updateService", updateService)));

            _kernel
                .Bind<Func<string, IUpdateDownloadedState>>()
                .ToConstant(
                    new Func<string, IUpdateDownloadedState>(
                        downloadLocation => new UpdateDownloadedState(downloadLocation)));

            _kernel
                .Bind<Func<UpdateInfo, string, UpdateService, IUpdateDownloadFailedState>>()
                .ToMethod(
                    context =>
                        (updateInfo, errorMessage, updateService) =>
                            context.Kernel.Get<UpdateDownloadFailedState>(
                                new ConstructorArgument("updateInfo", updateInfo),
                                new ConstructorArgument("errorMessage", errorMessage),
                                new ConstructorArgument("updateService", updateService)));

            var jiraUrlNotificationSource = _kernel.Get<JiraUrlNotificationSource>();

            _kernel
                .Bind<INotificationSource>()
                .ToConstant(jiraUrlNotificationSource);

            _kernel
                .Bind<IAsyncOptionalStoredSampleProvider<JiraUrlRequest, JiraUrlModel>>()
                .ToConstant(jiraUrlNotificationSource);

            _kernel
                .Bind<IAsyncOptionalSampleProvider<JiraUrlRequest, JiraUrlModel>>()
                .To<JiraUrlProvider>()
                .InSingletonScope();

            var tokenNotificationSource = _kernel.Get<TokenNotificationSource>();

            _kernel
                .Bind<IAsyncOptionalStoredSampleProvider<TokenRequest, TokenResponse>>()
                .ToConstant(tokenNotificationSource);

            _kernel
                .Bind<INotificationSource>()
                .ToConstant(tokenNotificationSource);

            _kernel
                .Bind<IAsyncOptionalSampleProvider<TokenRequest, TokenResponse>>()
                .To<TokenProvider>()
                .InSingletonScope();

            _kernel
                .Bind(typeof(IAsyncOptionalProvider<,>))
                .To(typeof(AsyncOptionalProvider<,>));

            _kernel.Bind<IDbUpdateService>().To<DbUpdateService>().InSingletonScope();

            _kernel
                .Bind<INotificationSource>()
                .To<UpdateNotificationsSource>();

            _kernel
                .Bind<INotificationSource>()
                .To<WorklogNotificationSource>();

            _kernel
                .Bind<IJiraConfigurationRepository>()
                .To<JiraConfigurationRepository>();
        }

        private void InitDesignTimeServices()
        {
            _kernel.Bind<ISettingsManager>().To<Design.SettingsManager>().InSingletonScope();
            _kernel.Bind<ITaskSource>().To<Design.TaskSource>().InSingletonScope();
            _kernel.Bind<IClipboard>().To<Clipboard>().InSingletonScope();
            _kernel.Bind<IBusinessManager>().To<BusinessManager>().InSingletonScope();
            _kernel.Bind<IJiraService>().To<Design.JiraService>().InSingletonScope();

            //_kernel.Bind<IProtector>().To<Protector>().InSingletonScope();

            //_kernel.Bind<IPasswordSource>().To<StoredPasswordSource>().InSingletonScope();
            //_kernel.Bind<IPasswordSource>().To<PasswordSource>().InSingletonScope();
            //_kernel.Bind<IPasswordProvider>().To<PasswordProvider>().InSingletonScope();

            _kernel.Bind<INavigationService>().To<NavigationService>().InSingletonScope();

            _kernel.Bind<IHolidayService>().To<Design.HolidayService>().InSingletonScope();
            //_kernel.Bind<IHolidayRepository>().To<HolidayRepository>().InSingletonScope();

            _kernel.Bind<IOvertimeReportService>().To<OvertimeReportService>().InSingletonScope();

            _kernel.Bind<IMessenger>().To<Messenger>().InSingletonScope();

            _kernel.Bind<IDispatcherService>().To<DispatcherService.DispatcherService>().InSingletonScope();
            _kernel.Bind<IUpdateService>().To<Design.UpdateService>().InSingletonScope();
            _kernel.Bind<IDbUpdateService>().To<Design.DbUpdateService>().InSingletonScope();
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool isDisposing)
        {
            if (!isDisposing) return;

            _kernel.Dispose();
        }
        #endregion
    }
}