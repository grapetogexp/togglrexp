﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Clipboard;

namespace TogglrExp.ViewModel.Timetrack
{
    public class WorkLogPreviewItemViewModel : ObservableObject
    {
        private readonly IClipboard _clipboard;

        private bool _isExpanded;

        public WorkLogPreviewItemViewModel(
            DateTime date,
            string comment,
            TimeSpan timespent,
            IClipboard clipboard)
        {
            _clipboard = clipboard;
            Date = date;
            Comment = comment;
            TimeSpent = timespent;
            CopyToClipboardCommand = new RelayCommand(CopyToClipboard);
        }

        public ICommand CopyToClipboardCommand { get; }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { Set(ref _isExpanded, value); }
        }

        public DateTime Date { get; }

        public string Comment { get; }

        public TimeSpan TimeSpent { get; }

        private void CopyToClipboard()
        {
            _clipboard.Copy(Comment);
        }
    }
}