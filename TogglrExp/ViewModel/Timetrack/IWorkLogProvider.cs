﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TogglrExp.Model;
using TogglrExp.Model.Jira;

namespace TogglrExp.ViewModel.Timetrack
{
    public interface IWorkLogProvider : IDisposable
    {
        Task<IEnumerable<Worklog>> Provide();
    }
}