﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TogglrExp.Model.Settings;
using TogglrExp.Model.Toggl;

namespace TogglrExp.ViewModel.Timetrack
{
    internal static class Helpers
    {
        public static string PrepareComment(this TaskItem item, Settings settings)
        {
            var textBuilder = new StringBuilder();
            textBuilder.Append(item.FormatPeriod(settings));
            if (settings.DurationExported)
            {
                textBuilder.AppendFormat("\t{0:hh\\:mm}", item.Duration);
            }
            if (settings.DescriptionExported)
            {
                textBuilder.AppendFormat("\t{0}", item.Description);
            }

            return textBuilder.ToString().Trim();
        }

        public static TimeSpan TotalTime(this IEnumerable<TaskItem> self)
        {
            var miliSeconds = self.Sum(o => o.Duration.TotalMilliseconds);
            return TimeSpan.FromMilliseconds(miliSeconds);
        }

        public static string FormatPeriod(this TaskItem taskItem, Settings settings)
        {
            var textBuilder = new StringBuilder();
            if (settings.TimeFromExported)
            {
                textBuilder.Append(taskItem.TimeFrom.ToString("HH:mm"));
            }

            if (settings.TimeToExported)
            {
                var format = settings.TimeFromExported ? "-{0:HH:mm}" : "{0:HH:mm}";
                textBuilder.AppendFormat(format, taskItem.TimeTo);
            }
            return textBuilder.ToString();
        }
    }
}