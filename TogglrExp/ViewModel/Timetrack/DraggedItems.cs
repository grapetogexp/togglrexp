﻿using System.Collections.Generic;

namespace TogglrExp.ViewModel.Timetrack
{
    public class DraggedItems
    {
        public DraggedItems(
            WorklogSelectionViewModel[] selectedItems,
            WorklogListViewModel sourceList)
        {
            SelectedItems = selectedItems;
            SourceList = sourceList;
        }

        public WorklogSelectionViewModel[] SelectedItems { get; }
        public WorklogListViewModel SourceList { get; }
        public bool Handled { get; set; }
    }
}