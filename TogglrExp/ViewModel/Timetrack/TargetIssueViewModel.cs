﻿using TogglrExp.Model;
using TogglrExp.Model.Jira;

namespace TogglrExp.ViewModel.Timetrack
{
    public class TargetIssueViewModel 
    {
        public TargetIssueViewModel(
            JiraIssueModel jiraIssueModel)
        {
            JiraIssueModel = jiraIssueModel;
            WorklogList = new WorklogListViewModel(new Worklog[0]);
        }

        public JiraIssueModel JiraIssueModel { get; }

       public WorklogListViewModel WorklogList { get; }
    }
}