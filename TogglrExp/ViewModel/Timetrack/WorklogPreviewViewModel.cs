﻿using System;
using System.Collections.Generic;
using System.Linq;
using TogglrExp.Model;
using TogglrExp.Model.Clipboard;
using TogglrExp.Model.Settings;

namespace TogglrExp.ViewModel.Timetrack
{
    public class WorklogPreviewViewModel
    {
        public WorklogPreviewViewModel(
            IEnumerable<Worklog> worklogs,
            IClipboard clipboard)
        {
            var worklogsList = worklogs.ToList();

            Worklogs = worklogsList;
            Items = 
                from wl in worklogsList
                group wl by wl.Date.Date into grouped
                let totalTime = TimeSpan.FromMilliseconds(grouped.Sum(o => o.TimeSpent.TotalMilliseconds))
                let comment = grouped.Aggregate(string.Empty, (aggregate, wl) => aggregate + "\n" + wl.Comment).Trim()
                    select new WorkLogPreviewItemViewModel(
                        grouped.Key,
                        comment,
                        totalTime,
                        clipboard);
        }

        public IEnumerable<Worklog> Worklogs { get; }

        public IEnumerable<WorkLogPreviewItemViewModel> Items { get; }
    }
}