﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using GongSolutions.Wpf.DragDrop;
using TogglrExp.Model;

namespace TogglrExp.ViewModel.Timetrack
{
    public class WorklogListViewModel : IDropTarget, IDragSource
    {
        private readonly ObservableCollection<WorklogSelectionViewModel> _worklogs;

        public WorklogListViewModel(Worklog[] worklogs)
        {
            _worklogs = new ObservableCollection<WorklogSelectionViewModel>(
                worklogs
                    .OrderBy(o => o.Date)
                    .Select(o => new WorklogSelectionViewModel(o)));

            ToggleSelectionCommand = new RelayCommand(ToggleSelection);
        }

        public IEnumerable<WorklogSelectionViewModel> Worklogs => _worklogs;

        public ICommand ToggleSelectionCommand { get; }

        public void Reset(Worklog[] worklogs)
        {
            _worklogs.Clear();
            foreach (var worklog in worklogs
                .OrderBy(o => o.Date))
            {
                _worklogs.Add(new WorklogSelectionViewModel(worklog));
            }
        }

        public void AddItem(Worklog worklog)
        {
            int? indexToInsert = null;
            for (int i = 0; i < _worklogs.Count; i++)
            {
                var item = _worklogs[i];

                if (i == 0)
                {
                    if (item.WorkLog.Date > worklog.Date)
                    {
                        indexToInsert = 0;
                        break;
                    }
                }

                if (i == _worklogs.Count - 1)
                {
                    if (item.WorkLog.Date > worklog.Date)
                    {
                        indexToInsert = i;
                    }
                    break;
                }

                var nextItem = _worklogs[i + 1];
                if (item.WorkLog.Date <= worklog.Date
                    && worklog.Date <= nextItem.WorkLog.Date)
                {
                    indexToInsert = i + 1;
                }
            }

            if (indexToInsert.HasValue)
            {
                _worklogs.Insert(indexToInsert.Value, new WorklogSelectionViewModel(worklog));
            }
            else
            {
                _worklogs.Add(new WorklogSelectionViewModel(worklog));
            }
        }

        public void RemoveItem(WorklogSelectionViewModel worklogSelectionViewModel)
        {
            _worklogs.Remove(worklogSelectionViewModel);
        }

        public void DragOver(IDropInfo dropInfo)
        {
            if (!(dropInfo.Data is DraggedItems draggedItems)) return;

            if (draggedItems.SourceList == this) return;

            dropInfo.Effects = DragDropEffects.Move;
            dropInfo.DropTargetAdorner = DropTargetAdorners.Highlight;
        }

        public void Drop(IDropInfo dropInfo)
        {
            if (!(dropInfo.Data is DraggedItems draggedItems)) return;

            if (draggedItems.SourceList == this) return;

            draggedItems.Handled = true;
            foreach (var worklog in draggedItems.SelectedItems.Select(o => o.WorkLog).OrderBy(o => o.Date))
            {
                AddItem(worklog);
            }
        }
        
        public void StartDrag(IDragInfo dragInfo)
        {
            var items = Worklogs.Where(o => o.IsSelected).ToArray();
            dragInfo.Data = new DraggedItems(items, this);
            dragInfo.Effects = DragDropEffects.Move;
        }

        public bool CanStartDrag(IDragInfo dragInfo)
        {
            return true;
        }

        public void Dropped(IDropInfo dropInfo)
        {
            if (!(dropInfo.Data is DraggedItems draggedItems)) return;

            if (!draggedItems.Handled) return;

            foreach (var worklog in draggedItems.SelectedItems)
            {
                _worklogs.Remove(worklog);
            }
        }

        public void DragCancelled()
        {

        }

        public bool TryCatchOccurredException(Exception exception)
        {
            return false;
        }

        private void ToggleSelection()
        {
            var desiredSelection = !Worklogs.All(o => o.IsSelected);
            foreach (var workLog in Worklogs)
            {
                workLog.IsSelected = desiredSelection;
            }
        }
    }
}