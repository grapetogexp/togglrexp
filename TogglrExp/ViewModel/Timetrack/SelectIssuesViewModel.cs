﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.Timetrack
{
    public class SelectIssuesViewModel : ObservableObject
    {
        private readonly Configuration _configuration;
        private readonly IJiraService _jiraService;
        private readonly IBusinessManager _businessManager;
        private readonly INavigationService _navigationService;

        private string _query;

        public SelectIssuesViewModel(
            Configuration configuration,
            IJiraService jiraService,
            IBusinessManager businessManager,
            INavigationService navigationService)
        {
            _configuration = configuration;
            _jiraService = jiraService;
            _businessManager = businessManager;
            _navigationService = navigationService;
            JiraIssueModels = new ObservableCollection<JiraIssueSelection>();
            SearchCommand = new RelayCommand(Search);
            ToggleSelectionCommand = new RelayCommand(ToggleSelection);
            ConfirmCommand = new RelayCommand(Confirm);
            GoBackCommand = new RelayCommand(GoBack);

            Query = "assignee=currentUser()";
        }

        public ObservableCollection<JiraIssueSelection> JiraIssueModels { get; }

        public ICommand SearchCommand { get; }

        public ICommand ToggleSelectionCommand { get; }

        public ICommand GoBackCommand { get; }

        public ICommand ConfirmCommand { get; }

        public string Query
        {
            get => _query;
            set => Set(ref _query, value);
        }

        private async void Search()
        {
            using (_businessManager.GetBusy())
            {
                var issueModels = await _jiraService.GetIssues(_configuration.Id, Query, CancellationToken.None);

                JiraIssueModels.Clear();

                foreach (var issueModel in issueModels)
                {
                    JiraIssueModels.Add(new JiraIssueSelection(issueModel));
                }
            }
        }

        private void ToggleSelection()
        {
            bool shouldBeSelected = JiraIssueModels.Any(o => !o.IsSelected);

            foreach (var selection in JiraIssueModels)
            {
                selection.IsSelected = shouldBeSelected;
            }
        }

        private void Confirm()
        {
            _navigationService.GoBack(true);
        }

        private void GoBack()
        {
            _navigationService.GoBack(false);
        }

        public class JiraIssueSelection : ObservableObject
        {
            private bool _isSelected;

            public JiraIssueSelection(JiraIssueModel issueModel)
            {
                IssueModel = issueModel;
            }

            public JiraIssueModel IssueModel { get; }

            public bool IsSelected
            {
                get => _isSelected;
                set => Set(ref _isSelected, value);
            }
        }
    }
}