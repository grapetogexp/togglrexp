﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using TogglrExp.Model;
using TogglrExp.Model.Settings;
using TogglrExp.Model.TimeProvider;
using TogglrExp.Model.Toggl;

namespace TogglrExp.ViewModel.Timetrack
{
    public class TogglWorklogViewModel: ViewModelBase, IWorkLogProvider
    {
        private readonly ITaskSource _taskSource;
        private readonly ISettingsManager _settingsManager;
        private readonly CancellationTokenSource _cancellationTokenSource;

        private DateTime _dateFrom;
        private DateTime _dateTo;

        private IEnumerable<SelectListItem> _workspaces;
        private IEnumerable<SelectListItem> _clients;
        private IEnumerable<SelectListItem> _projects;

        private SelectListItem _selectedWorkspace;
        private SelectListItem _selectedClient;
        private SelectListItem _selectedProject;

        private bool _canSelectClient;
        private bool _canSelectProject;

        private bool _timeFromExported;
        private bool _timeToExported;
        private bool _durationExported;
        private bool _descriptionExported;

        public TogglWorklogViewModel(
            ITaskSource taskSource,
            ISettingsManager settingsManager,
            IDateTimeProvider dateTimeProvider)
        {
            _taskSource = taskSource;
            _settingsManager = settingsManager;
            _cancellationTokenSource = new CancellationTokenSource();
            ReloadWorkspaces();

            var settings = _settingsManager.Get();
            // begin hack set fields in here to avoid settings manager call
            _timeFromExported = settings.TimeFromExported;
            _timeToExported = settings.TimeToExported;
            _durationExported = settings.DurationExported;
            _descriptionExported = settings.DescriptionExported;
            // end hack

            _dateFrom = _dateTo = dateTimeProvider.Today;
        }

        public DateTime DateFrom
        {
            get => _dateFrom;
            set
            {
                var difference = DateTo - DateFrom;
                if (Set(ref _dateFrom, value))
                {
                    DateTo = DateFrom + difference;
                }
            }
        }

        public DateTime DateTo
        {
            get => _dateTo;
            set => Set(ref _dateTo, value);
        }

        public IEnumerable<SelectListItem> Workspaces
        {
            get => _workspaces;
            set => Set(ref _workspaces, value);
        }

        public IEnumerable<SelectListItem> Clients
        {
            get => _clients;
            set => Set(ref _clients, value);
        }

        public IEnumerable<SelectListItem> Projects
        {
            get => _projects;
            set => Set(ref _projects, value);
        }

        public SelectListItem SelectedWorkspace
        {
            get => _selectedWorkspace;
            set
            {
                if (Set(ref _selectedWorkspace, value))
                {
                    ReloadClients();
                }
            }
        }

        public bool CanSelectClient
        {
            get => _canSelectClient;
            set => Set(ref _canSelectClient, value);
        }

        public SelectListItem SelectedClient
        {
            get => _selectedClient;
            set
            {
                if (Set(ref _selectedClient, value))
                {
                    ReloadProjects();
                }
            }
        }

        public bool CanSelectProject
        {
            get => _canSelectProject;
            set => Set(ref _canSelectProject, value);
        }

        public SelectListItem SelectedProject
        {
            get => _selectedProject;
            set => Set(ref _selectedProject, value);
        }

        public bool TimeFromExported
        {
            get => _timeFromExported;
            set
            {
                if (Set(ref _timeFromExported, value))
                {
                    _settingsManager.Modify(s =>
                    {
                        s.TimeFromExported = value;
                    });
                }
            }
        }

        public bool TimeToExported
        {
            get => _timeToExported;
            set
            {
                if (Set(ref _timeToExported, value))
                {
                    _settingsManager.Modify(s =>
                    {
                        s.TimeToExported = value;
                    });
                }
            }
        }

        public bool DurationExported
        {
            get => _durationExported;
            set
            {
                if (Set(ref _durationExported, value))
                {
                    _settingsManager.Modify(s => { s.DurationExported = value; });
                }
            }
        }

        public bool DescriptionExported
        {
            get => _descriptionExported;
            set
            {
                if (Set(ref _descriptionExported, value))
                {
                    _settingsManager.Modify(s => { s.DescriptionExported = value; });
                }
            }
        }

        public async Task<IEnumerable<Worklog>> Provide()
        {
            var items = await GetTogglItems();

            var settings = _settingsManager.Get();

            return from item in items
                select new Worklog(
                    item.TimeFrom,
                    item.Duration,
                    item.Description,
                    item.PrepareComment(settings));
        }

        private async void ReloadWorkspaces()
        {
            var oldWorkspace = SelectedWorkspace;
            Workspaces = Enumerable.Empty<SelectListItem>();

            var models = await _taskSource.GetWorkspaces(_cancellationTokenSource.Token);
            var extraItem = new SelectListItem(null, "Wszystkie");

            var items = models.OrderBy(o => o.Name).Select(o => new SelectListItem(o.Id, o.Name)).ToList();
            if (!items.Contains(oldWorkspace))
            {
                oldWorkspace = extraItem;
            }
            items.Insert(0, extraItem);

            Workspaces = items;
            SelectedWorkspace = oldWorkspace;

            ReloadClients();
        }

        private async void ReloadClients()
        {
            var oldClient = SelectedClient;
            Clients = Enumerable.Empty<SelectListItem>();

            int? selectedWorkspaceId = null;
            if (SelectedWorkspace != null)
            {
                selectedWorkspaceId = SelectedWorkspace.Id;
            }

            CanSelectClient = false;

            IEnumerable<ClientItem> clients;
            if (selectedWorkspaceId.HasValue)
            {
                clients = await _taskSource.GetClients(selectedWorkspaceId.Value, _cancellationTokenSource.Token);
                CanSelectClient = true;
            }
            else
            {
                clients = Enumerable.Empty<ClientItem>();
            }

            var extraItem = new SelectListItem(null, "Wszyscy");

            var items = clients.OrderBy(o => o.Name).Select(o => new SelectListItem(o.Id, o.Name)).ToList();
            if (!items.Contains(oldClient))
            {
                oldClient = extraItem;
            }
            items.Insert(0, extraItem);

            Clients = items;
            SelectedClient = oldClient;

            ReloadProjects();
        }

        private async void ReloadProjects()
        {
            var oldProject = SelectedProject;
            Projects = Enumerable.Empty<SelectListItem>();
            CanSelectProject = false;

            int? selectedWorkspaceId = null;
            if (SelectedWorkspace != null)
            {
                selectedWorkspaceId = SelectedWorkspace.Id;
            }

            IEnumerable<ProjectItem> projectItems;
            if (selectedWorkspaceId.HasValue)
            {
                int? selectedClientId = null;
                if (SelectedClient != null)
                {
                    selectedClientId = SelectedClient.Id;
                }

                projectItems = await _taskSource.GetProjects(selectedWorkspaceId.Value, selectedClientId,
                    _cancellationTokenSource.Token);
                CanSelectProject = true;
            }
            else
            {
                projectItems = Enumerable.Empty<ProjectItem>();
            }

            var extraItem = new SelectListItem(null, "Wszystkie");

            var items = projectItems.OrderBy(o => o.Name).Select(o => new SelectListItem(o.Id, o.Name)).ToList();
            if (!items.Contains(oldProject))
            {
                oldProject = extraItem;
            }
            items.Insert(0, extraItem);

            Projects = items;
            SelectedProject = oldProject;
        }

        private Task<IEnumerable<TaskItem>> GetTogglItems()
        {
            var secondBeforeMidnight = new TimeSpan(23, 59, 59);
            var dateTo = DateTo + secondBeforeMidnight;

            int? workspaceId = null;
            if (SelectedWorkspace != null)
            {
                workspaceId = SelectedWorkspace.Id;
            }

            int? clientId = null;
            if (SelectedClient != null)
            {
                clientId = SelectedClient.Id;
            }

            int? projectId = null;
            if (SelectedProject != null)
            {
                projectId = SelectedProject.Id;
            }

            var filter = new TaskFilter(DateFrom, dateTo, workspaceId, clientId, projectId);

            return _taskSource.GetItems(filter, _cancellationTokenSource.Token);
        }

        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
        }
    }
}