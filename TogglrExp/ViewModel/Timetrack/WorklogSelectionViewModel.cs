﻿using GalaSoft.MvvmLight;
using TogglrExp.Model;

namespace TogglrExp.ViewModel.Timetrack
{
    public class WorklogSelectionViewModel : ObservableObject
    {
        private bool _isSelected;

        public WorklogSelectionViewModel(Worklog workLog)
        {
            WorkLog = workLog;
        }

        public Worklog WorkLog { get; }

        public bool IsSelected
        {
            get => _isSelected;
            set => Set(ref _isSelected, value);
        }
    }
}