﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using TogglrExp.Helpers;
using TogglrExp.Model;
using TogglrExp.Model.Holiday;
using TogglrExp.Model.TimeProvider;

namespace TogglrExp.ViewModel.Timetrack
{
    public class RepeatableTimeTrackViewModel : ObservableObject, IWorkLogProvider
    {
        private readonly IHolidayService _holidayService;
        private DateTime _dateFrom;
        private DateTime _dateTo;
        private string _comment;
        private bool _skipHolidays;

        private int _hours;
        private int _minutes;

        public RepeatableTimeTrackViewModel(
            IHolidayService holidayService,
            IDateTimeProvider dateTimeProvider)
        {
            _holidayService = holidayService;

            _hours = 8;
            _skipHolidays = true;
            _dateFrom = _dateTo = dateTimeProvider.Today;
        }

        public DateTime DateFrom
        {
            get { return _dateFrom; }
            set
            {
                var difference = DateTo - DateFrom;
                if (Set(ref _dateFrom, value))
                {
                    DateTo = DateFrom + difference;
                }
            }
        }

        public DateTime DateTo
        {
            get { return _dateTo; }
            set { Set(ref _dateTo, value); }
        }

        public string Comment
        {
            get { return _comment; }
            set { Set(ref _comment, value); }
        }

        public bool SkipHolidays
        {
            get { return _skipHolidays; }
            set { Set(ref _skipHolidays, value); }
        }

        public int Hours
        {
            get { return _hours; }
            set { Set(ref _hours, value); }
        }

        public int Minutes
        {
            get { return _minutes; }
            set { Set(ref _minutes, value); }
        }

        public async Task<IEnumerable<Worklog>> Provide()
        {
            IEnumerable<Model.Holiday.Holiday> holidays;
            if (SkipHolidays)
            {
                holidays = await _holidayService.HolidayList(DateFrom, DateTo);
            }
            else
            {   
                holidays = Enumerable.Empty<Model.Holiday.Holiday>();
            }
            
            var holidaysSet = new HashSet<DateTime>(holidays.Select(o => o.Date));

            var workHours = new TimeSpan(Hours, Minutes, 0);
            return from date in DateTimeExtesions.DaysRange(DateFrom, DateTo)
                where !holidaysSet.Contains(date)
                select new Worklog(date, workHours, Comment, Comment);
        }

        public void Dispose()
        {
        }
    }
}