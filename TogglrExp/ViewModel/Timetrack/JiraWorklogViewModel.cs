﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TogglrExp.Model;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.Timetrack
{
    public class JiraWorklogViewModel : ObservableObject, IDisposable
    {
        private static readonly Regex IssueKeyRegex = new Regex("^\\s*(?<key>\\w+-\\d+)", RegexOptions.Compiled);

        private readonly INavigationService _navigationService;
        private readonly IBusinessManager _businessManager;
        private readonly IJiraService _jiraService;
        private readonly Func<Configuration, SelectIssuesViewModel> _selectIssuesFactory;

        private readonly Worklog[] _originalWorklogs;
        private IEnumerable<JiraIssueModel> _myIssues;
        private bool _canAddIssues;
        private bool _isLoadingIssues;

        private CancellationTokenSource _issuesLoadingCancelation;

        public JiraWorklogViewModel(
            IEnumerable<Worklog> worklogs,
            INavigationService navigationService,
            IBusinessManager businessManager,
            IJiraService jiraService,
            Func<JiraConfigurationSelectorViewModel> configurationSelectorFactory,
            Func<Configuration, SelectIssuesViewModel> selectIssuesFactory)
        {
            _navigationService = navigationService;
            _businessManager = businessManager;
            _jiraService = jiraService;
            _selectIssuesFactory = selectIssuesFactory;
            
            GoBackCommand = new RelayCommand(GoBack);
            AddSingleIssueCommand = new RelayCommand<JiraIssueModel>(AddSingleIssue, o => CanAddIssues);
            AddIssuesCommand = new RelayCommand(AddIssues, () => CanAddIssues);
            AddIssuesMagicallyCommand = new RelayCommand(AddIssuesMagically, () => CanAddIssues);
            ExportToJiraCommand = new RelayCommand(ExportToJira, () => CanAddIssues);

            _originalWorklogs = worklogs.OrderBy(o => o.Date).ToArray();
            WorklogList = new WorklogListViewModel(_originalWorklogs);
            TargetIssues = new ObservableCollection<TargetIssueViewModel>();

            ConfigurationSelector = configurationSelectorFactory();
            ConfigurationSelector.SelectedConfigurationChanged += OnSelectedConfigurationChanged;

            ReloadMyIssues();
        }
        
        public JiraConfigurationSelectorViewModel ConfigurationSelector { get; }

        public ICommand GoBackCommand { get; }

        public WorklogListViewModel WorklogList { get; }

        public ObservableCollection<TargetIssueViewModel> TargetIssues { get; }

        public RelayCommand<JiraIssueModel> AddSingleIssueCommand { get; }

        public RelayCommand AddIssuesCommand { get; }

        public RelayCommand AddIssuesMagicallyCommand { get; }

        public RelayCommand ExportToJiraCommand { get; }

        public IEnumerable<JiraIssueModel> MyIssues
        {
            get => _myIssues;
            set => Set(ref _myIssues, value);
        }

        private void AddSingleIssue(JiraIssueModel jiraWorkLog)
        {
            TargetIssues.Add(new TargetIssueViewModel(jiraWorkLog));
        }

        private async void AddIssues()
        {
            var configuration = ConfigurationSelector.SelectedConfiguration;
            if (configuration == null) return;

            var selectIssues = _selectIssuesFactory(configuration);

            if (await _navigationService.Open(selectIssues))
            {
                var targetIssues =
                    selectIssues
                        .JiraIssueModels
                        .Where(o => o.IsSelected)
                        .Select(o => new TargetIssueViewModel(o.IssueModel))
                        .ToList();

                foreach (var targetIssue in targetIssues)
                {
                    TargetIssues.Add(targetIssue);
                }
            }
        }

        private async void AddIssuesMagically()
        {
            var configuration = ConfigurationSelector.SelectedConfiguration;
            if (configuration == null) return;

            using (_businessManager.GetBusy())
            {
                var targetIssues = await Task.Run(() =>
                    TargetIssues
                        .GroupBy(o => o.JiraIssueModel.Key)
                        .ToDictionary(o => o.Key, o => o.First()));

                var matchingWorklogs = await Task.Run(() => WorklogList.Worklogs
                    .Where(o => IssueKeyRegex.IsMatch(o.WorkLog.Header))
                    .GroupBy(o => IssueKeyRegex.Match(o.WorkLog.Header).Groups["key"].Value));

                foreach (var groupedWorklog in matchingWorklogs)
                {
                    if (!targetIssues.TryGetValue(groupedWorklog.Key, out TargetIssueViewModel targetIssueViewModel))
                    {
                        var jiraIssueModel = await _jiraService
                            .GetIssue(configuration.Id, groupedWorklog.Key, CancellationToken.None);

                        if (jiraIssueModel == null)
                        {
                            continue;
                        }
                        targetIssueViewModel = new TargetIssueViewModel(jiraIssueModel);
                        TargetIssues.Add(targetIssueViewModel);
                    }

                    foreach (var worklog in groupedWorklog)
                    {
                        WorklogList.RemoveItem(worklog);
                        targetIssueViewModel.WorklogList.AddItem(worklog.WorkLog);
                    }
                }
            }
        }

        public bool CanAddIssues
        {
            get => _canAddIssues;
            set
            {
                if (Set(ref _canAddIssues, value))
                {
                    AddIssuesCommand.RaiseCanExecuteChanged();
                    AddIssuesMagicallyCommand.RaiseCanExecuteChanged();
                    ExportToJiraCommand.RaiseCanExecuteChanged();
                    TargetIssues.Clear();
                    WorklogList.Reset(_originalWorklogs);
                }
            }
        }

        public bool IsLoadingIssues
        {
            get => _isLoadingIssues;
            set => Set(ref _isLoadingIssues, value);
        }

        private void OnSelectedConfigurationChanged(object sender, EventArgs e)
        {
            ReloadMyIssues();
        }

        private async void ReloadMyIssues()
        {
            var cancellation = _issuesLoadingCancelation;
            _issuesLoadingCancelation = null;

            if (cancellation != null)
            {
                cancellation.Cancel();
                cancellation.Dispose();
            }

            var configuration = ConfigurationSelector.SelectedConfiguration;
            CanAddIssues = false;
            if (configuration == null)
            {   
                MyIssues = Enumerable.Empty<JiraIssueModel>();
                return;
            }

            IsLoadingIssues = true;
            _issuesLoadingCancelation = new CancellationTokenSource();
            MyIssues = await _jiraService.GetIssues(configuration.Id, "assignee=currentUser()", _issuesLoadingCancelation.Token);
            IsLoadingIssues = false;
            CanAddIssues = true;
        }

        private void GoBack()
        {
            _navigationService.GoBack();
        }

        private async void ExportToJira()
        {
            using (_businessManager.GetBusy())
            {
                foreach (var targetIssue in TargetIssues)
                {
                    var grouppedIssues = targetIssue
                        .WorklogList
                        .Worklogs
                        .GroupBy(o => o.WorkLog.Date.Date)
                        .Select(o => new Worklog(
                            o.Key,
                            TimeSpan.FromSeconds(o.Sum(wl => wl.WorkLog.TimeSpent.TotalSeconds)),
                            string.Empty,
                            string.Join("\r\n", o.Select(wl => wl.WorkLog.Comment)).Trim()));

                    await _jiraService.Report(grouppedIssues, targetIssue.JiraIssueModel, CancellationToken.None);
                }
            }
        }

        public void Dispose()
        {
            ConfigurationSelector.SelectedConfigurationChanged -= OnSelectedConfigurationChanged;
            ConfigurationSelector.Dispose();
            _issuesLoadingCancelation?.Dispose();
        }
    }
}