﻿using System;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.NotificationCenter;

namespace TogglrExp.ViewModel.Timetrack
{
    public class WorklogNotificationSource : INotificationSource, IDisposable
    {
        private readonly IJiraService _jiraService;

        public WorklogNotificationSource(IJiraService jiraService)
        {
            _jiraService = jiraService;
            jiraService.WorklogReported += JiraServiceOnWorklogReported;
        }

        private void JiraServiceOnWorklogReported(object sender, EventArgs eventArgs)
        {
            var notification = new InfoNotification("Zapisano czas pracy");

            NotificationAppeared?.Invoke(this, new NotificationAppearedArgs(new []{notification}));
        }

        public event EventHandler<NotificationAppearedArgs> NotificationAppeared;

        public void Dispose()
        {
            _jiraService.WorklogReported += JiraServiceOnWorklogReported;
        }
    }
}