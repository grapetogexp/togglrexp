﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.Timetrack
{
    public class TimeTrackViewModel : ViewModelBase, IDisposable
    {
        private readonly Func<TogglWorklogViewModel> _togglWorklogFactory;
        private readonly Func<RepeatableTimeTrackViewModel> _repeatableWorklogFactory;
        private readonly Func<IEnumerable<Worklog>, WorklogPreviewViewModel> _worklogPreviewFactory;
        private readonly Func<IEnumerable<Worklog>, JiraWorklogViewModel> _jiraWorklogFactory;
        private readonly INavigationService _navigationService;
        
        private IWorkLogProvider _selectedWorkLogProvider;
        private WorklogPreviewViewModel _preview;

        public TimeTrackViewModel(
            INavigationService navigationService,
            Func<TogglWorklogViewModel> togglWorklogFactory,
            Func<RepeatableTimeTrackViewModel> repeatableWorklogFactory,
            Func<IEnumerable<Worklog>, WorklogPreviewViewModel> worklogPreviewFactory,
            Func<IEnumerable<Worklog>, JiraWorklogViewModel> jiraWorklogFactory)
        {
            _navigationService = navigationService;
            _togglWorklogFactory = togglWorklogFactory;
            _repeatableWorklogFactory = repeatableWorklogFactory;
            _worklogPreviewFactory = worklogPreviewFactory;
            _jiraWorklogFactory = jiraWorklogFactory;
            
            RepeatableReportCommand = new RelayCommand(RepeatableReport);
            ImportFromTogglCommand = new RelayCommand(ImportFromToggl);
            RefreshPreviewCommand = new RelayCommand(RefreshPreview, CanRefreshPreview);
            GoNextCommand = new RelayCommand(GoNext, CanGoNext);

            GoBackCommand = new RelayCommand(GoBack);
        }
        
        public IWorkLogProvider SelectedWorkLogProvider
        {
            get => _selectedWorkLogProvider;
            set
            {
                var oldValue = _selectedWorkLogProvider;
                if (Set(ref _selectedWorkLogProvider, value))
                {
                    oldValue?.Dispose();
                }
            }
        }

        public WorklogPreviewViewModel Preview
        {
            get => _preview;
            set
            {
                if (Set(ref _preview, value))
                {
                    GoNextCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand GoBackCommand { get; }

        public ICommand ImportFromTogglCommand { get; }

        public ICommand RepeatableReportCommand { get; }

        public RelayCommand RefreshPreviewCommand { get; }

        public RelayCommand GoNextCommand { get; }

        private void GoBack()
        {
            _navigationService.GoBack();
        }

        private void ImportFromToggl()
        {
            SelectedWorkLogProvider = _togglWorklogFactory();
        }

        private void RepeatableReport()
        {
            SelectedWorkLogProvider = _repeatableWorklogFactory();
        }

        private async void RefreshPreview()
        {
            var items = await SelectedWorkLogProvider.Provide();
            Preview = _worklogPreviewFactory(items);
        }

        private bool CanRefreshPreview()
        {
            return SelectedWorkLogProvider != null;
        }

        private async void GoNext()
        {
            var items = Preview.Worklogs;
            using (var jiraWarlog = _jiraWorklogFactory(items))
            {
                await _navigationService.Open(jiraWarlog);
            }
        }

        private bool CanGoNext()
        {
            return Preview != null;
        }

        public void Dispose()
        {
            _selectedWorkLogProvider?.Dispose();
        }
    }
}