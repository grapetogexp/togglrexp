﻿using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Update;

namespace TogglrExp.ViewModel.Update
{
    public class UpdateAvailableViewModel : UpdateNotificationViewModel
    {
        private readonly IUpdateAvailableState _updateAvailableState;
        
        public UpdateAvailableViewModel(
            IUpdateAvailableState updateAvailableState,
            IUpdateService updateService)
            :base(updateService)
        {
            _updateAvailableState = updateAvailableState;
            DownloadCommand = new RelayCommand(Download);
        }
        
        public string Message { get { return _updateAvailableState.Message; } }

        public ICommand DownloadCommand { get; private set; }
        
        private void Download()
        {
            _updateAvailableState.Download();
        }
    }
}