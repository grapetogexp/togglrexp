﻿using System;
using TogglrExp.Model.Update;
using TogglrExp.ViewModel.DispatcherService;
using TogglrExp.ViewModel.NotificationCenter;

namespace TogglrExp.ViewModel.Update
{
    public sealed class UpdateNotificationsSource : INotificationSource, IDisposable
    {
        private readonly IUpdateService _updateService;
        private readonly IDispatcherService _dispatcherService;

        public UpdateNotificationsSource(
            IUpdateService updateService,
            IDispatcherService dispatcherService)
        {
            _updateService = updateService;
            _dispatcherService = dispatcherService;
            _updateService.StateChanged += UpdateServiceOnStateChanged;
        }
        
        public event EventHandler<NotificationAppearedArgs> NotificationAppeared;

        private void OnNoticationAppeared(INotification notification)
        {
            NotificationAppeared?.Invoke(this, new NotificationAppearedArgs(new[] {notification}));
        }

        private void UpdateServiceOnStateChanged(object sender, EventArgs eventArgs)
        {
            _dispatcherService.BeginInvoke(DispatchedOnStateChanged);
        }

        private void DispatchedOnStateChanged()
        {
            var state = _updateService.State;
            
            if (state is INoUpdateState)
            {
                // Nothing
            }
            else if (state is IUpdateAvailableState)
            {
                var updateAvailableState = state as IUpdateAvailableState;
                OnNoticationAppeared(new UpdateAvailableViewModel(updateAvailableState, _updateService));
            }
            else if (state is IUpdateDownloadingState)
            {
                OnNoticationAppeared(new UpdateDownloadingViewModel(_updateService));
            }
            else if (state is IUpdateDownloadFailedState)
            {
                var updateDownloadFailedState = state as IUpdateDownloadFailedState;
                OnNoticationAppeared(new UpdateDownloadFailedViewModel(updateDownloadFailedState, _updateService));
            }
            else if (state is IUpdateDownloadedState)
            {
                var updateDownloadedState = state as IUpdateDownloadedState;
                OnNoticationAppeared(new InstallUpdateViewModel(updateDownloadedState, _updateService));
            }
            else
            {
                throw new Exception("Unsupported state");
            }
        }

        #region IDisposable Implementation

        public void Dispose()
        {
            _updateService.StateChanged -= UpdateServiceOnStateChanged;
        }

        #endregion
    }
}