﻿using System.Windows.Input;
using TogglrExp.Model.Update;

namespace TogglrExp.ViewModel.Update
{
    public class UpdateDownloadFailedViewModel : UpdateNotificationViewModel
    {
        private readonly IUpdateDownloadFailedState _updateDownloadFailedState;

        public UpdateDownloadFailedViewModel(
            IUpdateDownloadFailedState updateDownloadFailedState,
            IUpdateService updateService) 
            : base(updateService)
        {
            _updateDownloadFailedState = updateDownloadFailedState;
        }

        public string Message { get { return _updateDownloadFailedState.Messsage; } }

        public ICommand RetryCommand { get; private set; }

        private void Retry()
        {
            _updateDownloadFailedState.Retry();
        }
    }
}