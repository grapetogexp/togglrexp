﻿using System;
using GalaSoft.MvvmLight;
using TogglrExp.Model.Update;
using TogglrExp.ViewModel.NotificationCenter;

namespace TogglrExp.ViewModel.Update
{
    public  class UpdateNotificationViewModel : ObservableObject, INotification
    {
        private readonly IUpdateService _updateService;

        public UpdateNotificationViewModel(IUpdateService updateService)
        {
            _updateService = updateService;
            _updateService.StateChanged += UpdateServiceOnStateChanged;
        }
        
        public event EventHandler Expired;

        private void UpdateServiceOnStateChanged(object sender, EventArgs eventArgs)
        {
            Expired?.Invoke(this, EventArgs.Empty);
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            _updateService.StateChanged -= UpdateServiceOnStateChanged;
        } 
        #endregion
    }
}