﻿using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Update;

namespace TogglrExp.ViewModel.Update
{
    public class InstallUpdateViewModel : UpdateNotificationViewModel
    {
        private readonly IUpdateDownloadedState _updateDownloadedState;

        public InstallUpdateViewModel(
            IUpdateDownloadedState updateDownloadedState,
            IUpdateService updateService)
            : base(updateService)
        {
            _updateDownloadedState = updateDownloadedState;
            InstallCommand = new RelayCommand(Install);
        }

        public ICommand InstallCommand { get; private set; }

        private void Install()
        {
            _updateDownloadedState.Install();
        }
    }
}