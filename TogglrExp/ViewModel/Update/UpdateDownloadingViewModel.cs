﻿using TogglrExp.Model.Update;

namespace TogglrExp.ViewModel.Update
{
    public class UpdateDownloadingViewModel : UpdateNotificationViewModel
    {
        public UpdateDownloadingViewModel(IUpdateService updateService) : base(updateService)
        {
        }
    }
}