﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TogglrExp.Model;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.ViewModel.NotificationCenter;

namespace TogglrExp.ViewModel
{
    public abstract class ProvisionNotificationSource<TParam, TResult, TNotification> :
        IAsyncOptionalStoredSampleProvider<TParam, TResult>,
        INotificationSource
        where TNotification : INotification
    {
        public event EventHandler<NotificationAppearedArgs> NotificationAppeared;

        protected abstract TNotification ProvideNotification(
            StoredSampleParam<TParam, TResult> parameter,
            int trialNumber,
            CancellationToken cancellationToken);

        protected abstract Option<StoredSample<TResult>> ProvideResult(TNotification notification);

        public Task<Option<StoredSample<TResult>>> Provide(
            StoredSampleParam<TParam, TResult> parameter,
            int trialNumber, 
            CancellationToken cancellationToken)
        {
            var taskCompletionSource = new TaskCompletionSource<Option<StoredSample<TResult>>>();

            var notification = ProvideNotification(parameter, trialNumber, cancellationToken);
            EventHandler expiredHandler = null;
            expiredHandler = (sender, args) =>
            {
                notification.Expired -= expiredHandler;

                var result = ProvideResult(notification);

                taskCompletionSource.SetResult(result);
            };
            notification.Expired += expiredHandler;

            NotificationAppeared?.Invoke(this, new NotificationAppearedArgs(new INotification[] {notification}));

            return taskCompletionSource.Task;
        }
    }
}