﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TogglrExp.ViewModel.Navigation;
using Tracer.Log4Net;

namespace TogglrExp.ViewModel
{
    public class QuestionViewModel : ObservableObject
    {
        private readonly INavigationService _navigationService;

        public QuestionViewModel(
            string message,
            INavigationService navigationService)
        {
            Log.DebugFormat("Question have been asked {0}", message);
            _navigationService = navigationService;
            Message = message;
            YesCommand = new RelayCommand(Yes);
            NoCommand = new RelayCommand(No);
        }

        public string Message { get; private set; }

        public ICommand YesCommand { get; private set; }

        public ICommand NoCommand { get; private set; }

        private void Yes()
        {
            Log.DebugFormat("User confirm {0}", Message);
            _navigationService.GoBack(true);
        }

        private void No()
        {
            Log.DebugFormat("User denied {0}", Message);
            _navigationService.GoBack(false);
        }
    }
}