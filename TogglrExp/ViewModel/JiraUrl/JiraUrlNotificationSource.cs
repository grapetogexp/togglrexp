﻿using System.Threading;
using TogglrExp.Model;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Jira.UrlProvision;
using ModelJiraUrl = TogglrExp.Model.Jira.UrlProvision.JiraUrl;

namespace TogglrExp.ViewModel.JiraUrl
{
    public class JiraUrlNotificationSource : 
        ProvisionNotificationSource<JiraUrlRequest, ModelJiraUrl, JiraUrlNotificationViewModel>
    {
        protected override JiraUrlNotificationViewModel ProvideNotification(
            StoredSampleParam<JiraUrlRequest, ModelJiraUrl> parameter, 
            int trialNumber,
            CancellationToken cancellationToken)
        {
            var url = parameter.StoredResult.Map(o => o.Url).GetValueOrDefault();
            return new JiraUrlNotificationViewModel(url, trialNumber > 0, cancellationToken);
        }

        protected override Option<StoredSample<ModelJiraUrl>> ProvideResult(JiraUrlNotificationViewModel notification)
        {
            return notification.JiraUrlResult.Map(StoredSample.Persisted);
        }
    }
}