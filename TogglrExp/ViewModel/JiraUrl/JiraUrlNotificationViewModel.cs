﻿using System;
using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model;
using TogglrExp.ViewModel.NotificationCenter;
using ModelJiraUrl = TogglrExp.Model.Jira.UrlProvision.JiraUrl;

namespace TogglrExp.ViewModel.JiraUrl
{
    public class JiraUrlNotificationViewModel : ObservableObject, INotification
    {
        private string _jiraUrl;

        public JiraUrlNotificationViewModel(
            string jiraUrl, 
            bool hasFailed,
            CancellationToken cancellationToken)
        {
            HasFailed = hasFailed;
            JiraUrl = jiraUrl;
            JiraUrlResult = Option<ModelJiraUrl>.None;
            DoneCommand = new RelayCommand(Done);
            DismissCommand = new RelayCommand(Dismiss);

            cancellationToken.Register(Dismiss);
        }

        public bool HasFailed { get; private set; }

        public string JiraUrl
        {
            get { return _jiraUrl; }
            set { Set(ref _jiraUrl, value); }
        }

        public ICommand DoneCommand { get; private set; }

        public ICommand DismissCommand { get; private set; }

        public Option<ModelJiraUrl> JiraUrlResult { get; private set; }

        private void Done()
        {
            JiraUrlResult = Option.Some(new ModelJiraUrl(JiraUrl));
            Dismiss();
        }

        private void Dismiss()
        {
            Expired?.Invoke(this, EventArgs.Empty);
        }

        public void Dispose()
        {
            
        }

        public event EventHandler Expired;
    }
}