﻿using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.About
{
    public class AboutViewModel
    {
        private readonly INavigationService _navigationService;

        public AboutViewModel(
            INavigationService navigationService)
        {
            _navigationService = navigationService;
            GoBackCommand = new RelayCommand(GoBack);

            Version = typeof(AboutViewModel).Assembly.GetName().Version.ToString();
        }

        public string Version { get; private set; }

        public ICommand GoBackCommand { get; private set; }

        private void GoBack()
        {
            _navigationService.GoBack();
        }
    }
}