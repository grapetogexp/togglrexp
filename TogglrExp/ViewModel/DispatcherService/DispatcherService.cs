﻿using System;
using GalaSoft.MvvmLight.Threading;

namespace TogglrExp.ViewModel.DispatcherService
{
    public class DispatcherService : IDispatcherService
    {
        public void BeginInvoke(Action action)
        {
            DispatcherHelper.UIDispatcher.BeginInvoke(action);
        }
    }
}