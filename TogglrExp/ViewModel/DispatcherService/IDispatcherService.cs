﻿using System;

namespace TogglrExp.ViewModel.DispatcherService
{
    public interface IDispatcherService
    {
        void BeginInvoke(Action action);
    }
}