﻿namespace TogglrExp.ViewModel
{
    public class SelectListItem
    {
        private readonly int? _id;

        public SelectListItem(int? id, string name)
        {
            _id = id;
            Name = name;
        }

        public int? Id
        {
            get { return _id; }
        }

        public string Name { get; private set; }

        public override string ToString()
        {
            return Name;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var second = obj as SelectListItem;
            if (second == null)
            {
                return false;
            }
            return Equals(Id, second.Id);
        }
    }
}