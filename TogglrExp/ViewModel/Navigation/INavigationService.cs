﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace TogglrExp.ViewModel.Navigation
{
    public interface INavigationService
    {
        Task<bool> Open(object target);

        void GoBack(bool result = false);

        object Current { get; }

        event EventHandler CurrentChanged;
    }
}