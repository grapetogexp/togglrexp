﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TracerAttributes;

namespace TogglrExp.ViewModel.Navigation
{
    [TraceOn]
    public class NavigationService : INavigationService
    {
        private readonly Stack<NavigationItem> _internalStack;
        private NavigationItem _current;

        public NavigationService()
        {
            _internalStack = new Stack<NavigationItem>();
        }

        public event EventHandler CurrentChanged;

        public object Current
        {
            get
            {
                if (_current == null)
                {
                    return null;
                }

                return _current.ObservableObject;
            }
        }

        public void GoBack(bool result = false)
        {
            var previous = _current;
            NavigationItem historical;
            if (_internalStack.Count > 0)
            {
                historical = _internalStack.Pop();
            }
            else
            {
                historical = null;
            }
            SetCurrent(historical);

            previous.TaskCompletionSource.SetResult(result);
        }

        public Task<bool> Open(object target)
        {
            if (_current != null)
            {
                _internalStack.Push(_current);
            }
            var completionSource = new TaskCompletionSource<bool>();
            SetCurrent(new NavigationItem(completionSource, target));
            return completionSource.Task;
        }

        protected virtual void OnCurrentChanged()
        {
            var handler = CurrentChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        private void SetCurrent(NavigationItem navigationItem)
        {
            _current = navigationItem;
            OnCurrentChanged();
        }

        private class NavigationItem
        {
            public NavigationItem(TaskCompletionSource<bool> taskCompletionSource, object observableObject)
            {
                TaskCompletionSource = taskCompletionSource;
                ObservableObject = observableObject;
            }

            public TaskCompletionSource<bool> TaskCompletionSource { get; private set; }

            public object ObservableObject { get; private set; }
        }
    }
}