﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace TogglrExp.ViewModel
{
    public class ValidationSummaryViewModel : ObservableObject
    {
        private IEnumerable<string> _errorMessages;
        private bool _hasErrors;
        private bool _hasInfos;
        private IEnumerable<string> _infos;

        public ValidationSummaryViewModel()
        {
            ClearErrors = new RelayCommand(ClearErrorsMethod);
            ClearInfos = new RelayCommand(ClearInfosMethod);
        }

        public ICommand ClearErrors { get; private set; }

        public ICommand ClearInfos { get; private set; }
        
        public IEnumerable<string> ErrorMessages
        {
            get { return _errorMessages; }
            set
            {
                if (Set(ref _errorMessages, value))
                {
                    HasErrors = value.Any();
                }
            }
        }

        public bool HasErrors
        {
            get { return _hasErrors; }
            private set { Set(ref _hasErrors, value); }
        }

        public bool HasInfos
        {
            get { return _hasInfos; }
            private set { Set(ref _hasInfos, value); }
        }

        public IEnumerable<string> Infos
        {
            get { return _infos; }
            set 
            {
                if (Set(ref _infos, value))
                {
                    HasInfos = value.Any();
                } 
            }
        }

        public void Reset()
        {
            ClearErrorsMethod();
            ClearInfosMethod();
        }

        private void ClearErrorsMethod()
        {
            ErrorMessages = Enumerable.Empty<string>();
        }

        private void ClearInfosMethod()
        {
            Infos = Enumerable.Empty<string>();
        }
    }
}