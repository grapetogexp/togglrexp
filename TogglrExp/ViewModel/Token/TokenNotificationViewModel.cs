﻿using System;
using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model;
using TogglrExp.Model.Toggl.TokenProvision;
using TogglrExp.ViewModel.NotificationCenter;

namespace TogglrExp.ViewModel.Token
{
    public class TokenNotificationViewModel : ObservableObject, INotification
    {
        private string _token;

        public TokenNotificationViewModel(
            Option<TokenResponse> previousResponse,
            bool hasFailed,
            CancellationToken cancellationToken)
        {
            HasFailed = hasFailed;
            Token = previousResponse
                .Map(o => o.Token)
                .GetValueOrDefault();
            DoneCommand = new RelayCommand(Done);
            DismissCommand = new RelayCommand(Dismiss);

            TokenResponse = previousResponse;

            cancellationToken.Register(Dismiss);
        }

        public bool HasFailed { get; private set; }

        public ICommand DoneCommand { get; private set; }

        public ICommand DismissCommand { get; private set; }

        public Option<TokenResponse> TokenResponse { get; private set; }

        private void Done()
        {
            TokenResponse = Option<TokenResponse>.Some(new TokenResponse(Token));
            Dismiss();
        }

        private void Dismiss()
        {
            Expired?.Invoke(this, EventArgs.Empty);
        }

        public string Token
        {
            get { return _token; }
            set { Set(ref _token, value); }
        }
        
        public void Dispose()
        {
            
        }

        public event EventHandler Expired;
    }
}