﻿using System.Threading;
using TogglrExp.Model;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Toggl.TokenProvision;

namespace TogglrExp.ViewModel.Token
{
    public class TokenNotificationSource : ProvisionNotificationSource<TokenRequest, TokenResponse, TokenNotificationViewModel>
    {
        protected override TokenNotificationViewModel ProvideNotification(
            StoredSampleParam<TokenRequest, TokenResponse> parameter, 
            int trialNumber,
            CancellationToken cancellationToken)
        {
            return new TokenNotificationViewModel(parameter.StoredResult, trialNumber > 0, cancellationToken);
        }

        protected override Option<StoredSample<TokenResponse>> ProvideResult(TokenNotificationViewModel notification)
        {
            return notification.TokenResponse.Map(StoredSample.Persisted);
        }
    }
}