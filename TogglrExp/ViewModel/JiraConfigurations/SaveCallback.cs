﻿using System.Threading.Tasks;
using TogglrExp.Model;
using TogglrExp.Model.Jira;

namespace TogglrExp.ViewModel.JiraConfigurations
{
    public delegate Task<ValidationResult> SaveCallback(ConfigrationPrototype prototype);
}