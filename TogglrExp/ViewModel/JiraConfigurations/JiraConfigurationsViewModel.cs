﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.JiraConfigurations
{
    public class JiraConfigurationsViewModel : ObservableObject, IDisposable
    {
        private readonly IJiraService _jiraService;
        private readonly INavigationService _navigationService;
        private readonly IBusinessManager _businessManager;
        private readonly Func<Configuration, JiraConfigurationListItem> _listItemFactory;
        private readonly Func<SaveCallback, JiraConfigurationDialog> _dialogFactory;
        private readonly CancellationTokenSource _loadingCancellation;

        private IEnumerable<JiraConfigurationListItem> _items;

        public JiraConfigurationsViewModel(
            IJiraService jiraService,
            INavigationService navigationService,
            IBusinessManager businessManager,
            Func<Configuration, JiraConfigurationListItem> listItemFactory,
            Func<SaveCallback, JiraConfigurationDialog> dialogFactory)
        {
            _jiraService = jiraService;
            _navigationService = navigationService;
            _businessManager = businessManager;
            _listItemFactory = listItemFactory;
            _dialogFactory = dialogFactory;

            _loadingCancellation = new CancellationTokenSource();
            
            AddCommand = new RelayCommand(Add);

            Refresh();
        }

        public ICommand AddCommand { get; private set; }
        
        public IEnumerable<JiraConfigurationListItem> Items
        {
            get { return _items; }
            set { Set(ref _items, value); }
        }
        
        private async void Add()
        {
            if (await _navigationService.Open(_dialogFactory(_jiraService.AddConfiguration)))
            {
                Refresh();
            }
        }

        private async void Refresh()
        {
            // begin hack workaround for navigation
            await Task.Delay(TimeSpan.FromMilliseconds(10));
            // end hack
            using (_businessManager.GetBusy())
            {
                var configurations = await _jiraService.GetConfigurations(_loadingCancellation.Token);

                Items = configurations.Select(_listItemFactory).ToList();
            }
        }

        public void Dispose()
        {
            _loadingCancellation.Cancel();
            _loadingCancellation.Dispose();
        }
    }
}