﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.JiraConfigurations
{
    public class JiraConfigurationDialog : ObservableObject
    {
        private readonly SaveCallback _saveCallback;
        private readonly INavigationService _navigationService;
        private readonly IBusinessManager _businessManager;
        private string _name;

        private string _url;

        public JiraConfigurationDialog(
            SaveCallback saveCallback,
            INavigationService navigationService,
            IBusinessManager businessManager)
        {
            _saveCallback = saveCallback;
            _navigationService = navigationService;
            _businessManager = businessManager;
            ValidationSummary = new ValidationSummaryViewModel();

            SaveCommand = new RelayCommand(Save);
            CancelCommand = new RelayCommand(Cancel);
        }
        
        public ValidationSummaryViewModel ValidationSummary { get; private set; }

        public ICommand SaveCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }

        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value); }
        }

        public string Url
        {
            get { return _url; }
            set { Set(ref _url, value); }
        }

        private async void Save()
        {
            ValidationResult validation;
            using (_businessManager.GetBusy())
            {
                var prototype = new ConfigrationPrototype(Name, Url);
                validation = await _saveCallback(prototype);
            }
            if (validation.IsValid)
            {
                _navigationService.GoBack(true);
            }
            else
            {
                ValidationSummary.ErrorMessages = validation.ErrorMessages;
            }
        }

        private void Cancel()
        {
            _navigationService.GoBack(false);
        }
    }
}