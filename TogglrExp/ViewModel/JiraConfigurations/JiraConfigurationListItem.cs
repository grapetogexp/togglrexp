﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Infrastructure;
using TogglrExp.Model;
using TogglrExp.Model.Jira;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.JiraConfigurations
{
    public class JiraConfigurationListItem : ObservableObject
    {
        private readonly IJiraService _jiraService;
        private readonly INavigationService _navigationService;
        private readonly IUriOpener _uriOpener;
        private readonly Func<SaveCallback, JiraConfigurationDialog> _editDialogFactory;
        private readonly Func<string, QuestionViewModel> _questionDialogFactory;
        private readonly long _configurationId;

        private string _name;
        private string _url;
        private bool _isPreferedIssueVisible;
        private string _preferedIssueKey;

        public JiraConfigurationListItem(
            Configuration configuration,
            IJiraService jiraService,
            INavigationService navigationService,
            IUriOpener uriOpener,
            Func<SaveCallback, JiraConfigurationDialog> editDialogFactory,
            Func<string, QuestionViewModel> questionDialogFactory)
        {
            _jiraService = jiraService;
            _navigationService = navigationService;
            _uriOpener = uriOpener;
            _editDialogFactory = editDialogFactory;
            _questionDialogFactory = questionDialogFactory;
            _configurationId = configuration.Id;

            EditCommand = new RelayCommand(Edit);
            DeleteCommand = new RelayCommand(Delete);
            OpenJiraCommand = new RelayCommand(OpenJira);
            OpenPreferedIssueCommand = new RelayCommand(OpenPreferedIssue);

            RefreshFields(configuration);
        }

        public ICommand EditCommand { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        public ICommand OpenJiraCommand { get; private set; }

        public ICommand OpenPreferedIssueCommand { get; private set; }

        public string Name
        {
            get { return _name; }
            private set { Set(ref _name, value); }
        }

        public string Url
        {
            get { return _url; }
            private set { Set(ref _url, value); }
        }

        public bool IsPreferedIssueVisible
        {
            get { return _isPreferedIssueVisible; }
            private set { Set(ref _isPreferedIssueVisible, value); }
        }

        public string PrefferedIssueKey
        {
            get { return _preferedIssueKey; }
            set { Set(ref _preferedIssueKey, value); }
        }
        
        private async void Edit()
        {
            var configuration = await _jiraService.GetConfiguration(_configurationId);
            var dialog = _editDialogFactory(Edit);
            dialog.Name = configuration.Name;
            dialog.Url = configuration.Url;

            if (await _navigationService.Open(dialog))
            {
                configuration = await _jiraService.GetConfiguration(_configurationId);
                RefreshFields(configuration);
            }
        }

        private Task<ValidationResult> Edit(ConfigrationPrototype prototype)
        {
            return _jiraService.UpdateConfiguration(_configurationId, prototype);
        }
        
        private async void Delete()
        {
            var questionDialog = _questionDialogFactory("Jesteś pewien?");
            if (await _navigationService.Open(questionDialog))
            {
                await _jiraService.DeleteConfiguration(_configurationId);
            }
        }

        private void OpenJira()
        {
            _uriOpener.Open(new Uri(Url, UriKind.Absolute));
        }

        private void OpenPreferedIssue()
        {
            if (string.IsNullOrEmpty(PrefferedIssueKey))
            {
                return;
            }
            var builder = new UriBuilder(Url);
            builder.Path += "/browse/" + PrefferedIssueKey;
            _uriOpener.Open(builder.Uri);
        }

        private void RefreshFields(Configuration configuration)
        {
            Name = configuration.Name;
            Url = configuration.Url;
            IsPreferedIssueVisible = !string.IsNullOrWhiteSpace(configuration.PreferedIssue);
            PrefferedIssueKey = configuration.PreferedIssue;
        }
    }
}