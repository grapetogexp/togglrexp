﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;

namespace TogglrExp.ViewModel.NotificationCenter
{
    public sealed class NotificationCenterViewModel : ObservableObject, IDisposable
    {
        private readonly INotificationSource[] _notificationSources;

        public NotificationCenterViewModel(IEnumerable<INotificationSource> notificationSources)
        {
            _notificationSources = notificationSources.ToArray();
            Notifications = new ObservableCollection<INotification>();

            foreach (var notificationSource in _notificationSources)
            {
                notificationSource.NotificationAppeared += NotificationSourceOnNotificationAppeared;
            }
        }
        
        public ObservableCollection<INotification> Notifications { get; private set; }
        
        public void Dispose()
        {
            foreach (var notificationSource in _notificationSources)
            {
                notificationSource.NotificationAppeared -= NotificationSourceOnNotificationAppeared;
            }
        }

        private void NotificationSourceOnNotificationAppeared(object sender, NotificationAppearedArgs notificationAppearedArgs)
        {
            var noticiations = notificationAppearedArgs.Notifications;
            foreach (var notification in noticiations)
            {
                notification.Expired += NotificationOnExpired;
                Notifications.Add(notification);
            }
        }

        private void NotificationOnExpired(object sender, EventArgs eventArgs)
        {
            var notification = (INotification) sender;
            notification.Expired -= NotificationOnExpired;

            notification.Dispose();
            Notifications.Remove(notification);
        }
    }
}