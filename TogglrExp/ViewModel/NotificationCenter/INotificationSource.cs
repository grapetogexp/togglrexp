﻿using System;

namespace TogglrExp.ViewModel.NotificationCenter
{
    public interface INotificationSource
    {
        event EventHandler<NotificationAppearedArgs> NotificationAppeared;
    }
}