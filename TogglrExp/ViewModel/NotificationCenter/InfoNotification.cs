﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;

namespace TogglrExp.ViewModel.NotificationCenter
{
    public class InfoNotification : INotification
    {
        public InfoNotification(params string[] messages)
        {
            Messages = messages;
            DismissCommand = new RelayCommand(Dismiss);
        }
        
        public ICommand DismissCommand { get; private set; }

        public string[] Messages { get; private set; }

        public void Dispose()
        {
            
        }

        public event EventHandler Expired;

        private void Dismiss()
        {
            Expired?.Invoke(this, EventArgs.Empty);
        }
    }
}