﻿using System;
using System.Collections.Generic;

namespace TogglrExp.ViewModel.NotificationCenter
{
    public class NotificationAppearedArgs : EventArgs
    {
        public NotificationAppearedArgs(IEnumerable<INotification> notifications)
        {
            Notifications = notifications;
        }

        public IEnumerable<INotification> Notifications { get; private set; }

    }
}