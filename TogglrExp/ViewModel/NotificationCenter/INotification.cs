﻿using System;

namespace TogglrExp.ViewModel.NotificationCenter
{
    public interface INotification : IDisposable
    {
        event EventHandler Expired;
    }
}