﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Settings;
using TogglrExp.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel
{
    public sealed class SettingsViewModel : ObservableObject, IDisposable
    {
        private readonly INavigationService _navigationService;
        private readonly ISettingsManager _settingsManager;
        private string _token;

        public SettingsViewModel(
            ISettingsManager settingsManager, 
            INavigationService navigationService,
            Func<JiraConfigurationsViewModel> jiraConfigurationsFactory)
        {
            _settingsManager = settingsManager;
            _navigationService = navigationService;
            JiraConfigurations = jiraConfigurationsFactory();

            var settings = settingsManager.Get();

            Token = settings.UserToken;
            
            GoBackCommand = new RelayCommand(GoBack);
            ClearTokenCommand = new RelayCommand(ClearToken);
        }

        public ICommand GoBackCommand { get; private set; }

        public ICommand ClearTokenCommand { get; private set; }
        
        public JiraConfigurationsViewModel JiraConfigurations { get; private set; }

        public string Token
        {
            get { return _token; }
            set { Set(ref _token, value); }
        }
        
        private void GoBack()
        {
            _navigationService.GoBack();
        }

        private void ClearToken()
        {
            _settingsManager.Modify(ms =>
            {
                ms.UserToken = string.Empty;
            });
            Token = string.Empty;
        }

        public void Dispose()
        {
            JiraConfigurations.Dispose();
        }
    }
}