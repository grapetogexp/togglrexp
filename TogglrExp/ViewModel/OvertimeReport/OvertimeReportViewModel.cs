﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TogglrExp.Helpers;
using TogglrExp.Model.Clipboard;
using TogglrExp.Model.OvertimeReport;
using TogglrExp.Model.TimeProvider;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;
using TracerAttributes;

namespace TogglrExp.ViewModel.OvertimeReport
{
    public class OvertimeReportViewModel : ObservableObject, IDisposable
    {
        private readonly IBusinessManager _businessManager;
        private readonly IClipboard _clipboard;
        private readonly INavigationService _navigationService;
        private readonly IOvertimeReportService _overtimeReportService;
        private DateTime _dateFrom;
        private DateTime _dateTo;
        private readonly TimeSpan _dayNorm;
        private IEnumerable<OvertimeItem> _overtimeItems;
        private TimeSpan _total;

        public OvertimeReportViewModel(
            IOvertimeReportService overtimeReportService,
            IClipboard clipboard,
            IBusinessManager businessManager,
            INavigationService navigationService,
            IDateTimeProvider dateTimeProvider,
            Func<JiraIssueListViewModel> jiraIssueListFactory)
        {
            _overtimeReportService = overtimeReportService;
            _clipboard = clipboard;
            _businessManager = businessManager;
            _navigationService = navigationService;
            _dayNorm = TimeSpan.FromHours(8);

            var today = dateTimeProvider.Today;
            var beginOfMonth = new DateTime(today.Year, today.Month, 1);

            DateFrom = beginOfMonth;
            DateTo = today;
            OvertimeItems = Enumerable.Empty<OvertimeItem>();

            ValidationSummary = new ValidationSummaryViewModel();

            JiraIssueList = jiraIssueListFactory();
            JiraIssueList.SelectedIssueChanged += TimeTrackIssueListOnSelectedIssueChanged;

            BackCommand = new RelayCommand(Back);
            CopyCommand = new RelayCommand(Copy);
            RefreshCommand = new RelayCommand(Refresh, CanRefresh);
        }

        public ICommand BackCommand { get; private set; }
        public ICommand CopyCommand { get; private set; }

        public DateTime DateFrom
        {
            get { return _dateFrom; }
            set { Set(ref _dateFrom, value); }
        }

        public DateTime DateTo
        {
            get { return _dateTo; }
            set { Set(ref _dateTo, value); }
        }

        public IEnumerable<OvertimeItem> OvertimeItems
        {
            get { return _overtimeItems; }
            private set { Set(ref _overtimeItems, value); }
        }

        public TimeSpan Total
        {
            get { return _total; }
            set { Set(ref _total, value); }
        }

        public RelayCommand RefreshCommand { get; private set; }
        public ValidationSummaryViewModel ValidationSummary { get; private set; }
        public JiraIssueListViewModel JiraIssueList { get; private set; }

        private void TimeTrackIssueListOnSelectedIssueChanged(object sender, EventArgs eventArgs)
        {
            RefreshCommand.RaiseCanExecuteChanged();
        }

        private bool CanRefresh()
        {
            return JiraIssueList.SelectedIssue != null;
        }

        private void Back()
        {
            _navigationService.GoBack();
        }

        [TraceOn]
        private void Copy()
        {
            var sb = new StringBuilder();
            foreach (var item in OvertimeItems)
            {
                sb
                    .Append(item.Date.ToString("yyyy-MM-dd"))
                    .Append("\t");
                if (item.Overflow < TimeSpan.Zero)
                {
                    sb.Append("-");
                }
                sb
                    .Append(OvertimeTimeSpanFormatHelper.Format(item.Overflow))
                    .AppendLine();
            }
            sb.Append("Razem:");
            if (Total < TimeSpan.Zero)
            {
                sb.Append("-");
            }
            sb.Append(OvertimeTimeSpanFormatHelper.Format(Total));

            _clipboard.Copy(sb.ToString());
        }

        private async void Refresh()
        {
            await RefreshTaskAsync();
        }

        [TraceOn]
        internal async Task RefreshTaskAsync()
        {
            ValidationSummary.Reset();

            using (_businessManager.GetBusy())
            {
                try
                {
                    // It moves dateTo to 23:59:59 the same day just to include worklog items in report
                    // begin hack
                    var dateTo = DateTo.AddDays(1).AddSeconds(-1);
                    // end hack

                    var result = await _overtimeReportService
                        .Get(DateFrom, dateTo, _dayNorm, JiraIssueList.SelectedIssue, CancellationToken.None);
                    OvertimeItems = result.Items;
                    Total = result.Total;
                }
                catch (Exception ex)
                {
                    ValidationSummary.ErrorMessages = new[] {ex.Message};
                }
            }
        }

        public void Dispose()
        {
            JiraIssueList.Dispose();
        }
    }
}