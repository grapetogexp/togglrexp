﻿using System;
using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Jira.Credentials;
using TogglrExp.ViewModel.NotificationCenter;

namespace TogglrExp.ViewModel.JiraCredntials
{
    public class PasswordNotificationViewModel : ObservableObject, INotification
    {
        private string _username;
        private string _password;
        private bool _rememberPassword;

        public PasswordNotificationViewModel(
            Option<JiraCredentials> jiraCredentials,
            bool hasFailed,
            CancellationToken cancellationToken)
        {
            if (!jiraCredentials.IsNone)
            {
                Username = jiraCredentials.Value.Username;
                Password = jiraCredentials.Value.Password;
            }
            HasFailed = hasFailed;

            DoneCommand = new RelayCommand(Done);
            DismissCommand = new RelayCommand(Dismiss);

            Creadetials = Option<StoredSample<JiraCredentials>>.None;

            cancellationToken.Register(Dismiss);
        }

        public event EventHandler Expired;

        public ICommand DoneCommand { get; private set; }

        public ICommand DismissCommand { get; private set; }

        public Option<StoredSample<JiraCredentials>> Creadetials { get; private set; }

        public string Username
        {
            get { return _username; }
            set { Set(ref _username, value); }
        }

        public string Password
        {
            get { return _password; }
            set { Set(ref _password, value); }
        }

        public bool HasFailed { get; private set; }

        public bool RememberPassword
        {
            get { return _rememberPassword; }
            set { Set(ref _rememberPassword, value); }
        }

        private void Done()
        {
            var credentials = new JiraCredentials(Username, Password);
            Creadetials = Option.Some(new StoredSample<JiraCredentials>(credentials, RememberPassword));
            Dismiss();
        }

        private void Dismiss()
        {
            Expired?.Invoke(this, EventArgs.Empty);
        }

        #region IDisposable
        public void Dispose()
        {

        } 
        #endregion

    }
}