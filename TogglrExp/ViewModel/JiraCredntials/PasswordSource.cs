﻿using System.Threading;
using TogglrExp.Model;
using TogglrExp.Model.AsyncOptionalProvider;
using TogglrExp.Model.Jira.Credentials;

namespace TogglrExp.ViewModel.JiraCredntials
{
    public class PasswordSource :
        ProvisionNotificationSource<JiraCredentialRequest, JiraCredentials, PasswordNotificationViewModel>
    {
        protected override PasswordNotificationViewModel ProvideNotification(
            StoredSampleParam<JiraCredentialRequest, JiraCredentials> parameter, 
            int trialNumber,
            CancellationToken cancellationToken)
        {
            return new PasswordNotificationViewModel(parameter.StoredResult, trialNumber > 0, cancellationToken);
        }

        protected override Option<StoredSample<JiraCredentials>> ProvideResult(PasswordNotificationViewModel notification)
        {
            return notification.Creadetials;
        }
    }
}