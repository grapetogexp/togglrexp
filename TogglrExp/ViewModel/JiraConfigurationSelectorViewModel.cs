﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Jira;
using TogglrExp.Model.Settings;
using TogglrExp.ViewModel.JiraConfigurations;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel
{
    public class JiraConfigurationSelectorViewModel : ObservableObject, IDisposable
    {
        private readonly IJiraService _jiraService;
        private readonly ISettingsManager _settingsManager;
        private readonly INavigationService _navigationService;
        private readonly Func<SaveCallback, JiraConfigurationDialog> _configurationDialogFactory;

        private CancellationTokenSource _configurationsCancelation;

        private Configuration _selectedConfiguration;
        private IEnumerable<Configuration> _configurations;

        private bool _areConfigurationsLoading;
        private bool _isSelectionAvailable;

        public event EventHandler SelectedConfigurationChanged;

        public JiraConfigurationSelectorViewModel(
            IJiraService jiraService,
            ISettingsManager settingsManager,
            INavigationService navigationService,
            Func<SaveCallback, JiraConfigurationDialog> configurationDialogFactory)
        {
            _jiraService = jiraService;
            _settingsManager = settingsManager;
            _navigationService = navigationService;
            _configurationDialogFactory = configurationDialogFactory;

            OpenConfigurationsCommand = new RelayCommand(OpenConfigurations);

            ReloadConfigurations();
        }

        public Configuration SelectedConfiguration
        {
            get => _selectedConfiguration;
            set
            {
                if (Set(ref _selectedConfiguration, value))
                {
                    if (value != null)
                    {
                        _settingsManager.Modify(s =>
                        {
                            s.JiraConfigurationId = value.Id;
                        });
                    }
                    SelectedConfigurationChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public bool IsSelectionAvailable
        {
            get => _isSelectionAvailable;
            set => Set(ref _isSelectionAvailable, value);
        }

        public IEnumerable<Configuration> Configurations
        {
            get => _configurations;
            private set => Set(ref _configurations, value);
        }

        public bool AreConfigurationsLoading
        {
            get => _areConfigurationsLoading;
            set => Set(ref _areConfigurationsLoading, value);
        }

        public ICommand OpenConfigurationsCommand { get; }

        private async void ReloadConfigurations()
        {
            if (_configurationsCancelation != null)
            {
                _configurationsCancelation.Cancel();
                _configurationsCancelation.Dispose();
            }
            _configurationsCancelation = new CancellationTokenSource();

            AreConfigurationsLoading = true;
            try
            {
                long? selectedConfigurationId;
                if (SelectedConfiguration != null)
                {
                    selectedConfigurationId = SelectedConfiguration.Id;
                }
                else
                {
                    selectedConfigurationId = _settingsManager.Get().JiraConfigurationId;
                }

                var configurations = await _jiraService.GetConfigurations(_configurationsCancelation.Token);
                Configurations = configurations as IList<Configuration> ?? configurations.ToList();

                // Workaround for combo behavior
                SelectedConfiguration = null;

                var toSelect = Configurations.FirstOrDefault(o => o.Id == selectedConfigurationId);

                if (toSelect == null)
                {
                    toSelect = Configurations.FirstOrDefault();
                }
                SelectedConfiguration = toSelect;
                IsSelectionAvailable = Configurations.Count() > 1;
            }
            finally
            {
                AreConfigurationsLoading = false;
            }
        }

        private async void OpenConfigurations()
        {
            var jiraConfigurations = _configurationDialogFactory(_jiraService.AddConfiguration);
            if (await _navigationService.Open(jiraConfigurations))
            {
                ReloadConfigurations();
            }
        }

        public void Dispose()
        {
            if (_configurationsCancelation != null)
            {
                _configurationsCancelation.Cancel();
                _configurationsCancelation.Dispose();
            }
        }
    }
}