﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Holiday;
using TogglrExp.Model.TimeProvider;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.Holiday
{
    public class AddHolidayViewModel : ObservableObject
    {
        private readonly INavigationService _navigationService;
        private readonly IHolidayService _holidayService;
        private readonly IBusinessManager _businessManager;
        private DateTime _date;
        private string _name;

        public AddHolidayViewModel(
            INavigationService navigationService, 
            IHolidayService holidayService,
            IBusinessManager businessManager,
            IDateTimeProvider dateTimeProvider)
        {
            _navigationService = navigationService;
            _holidayService = holidayService;
            _businessManager = businessManager;

            ValidationSummary = new ValidationSummaryViewModel();

            ApproveCommand = new RelayCommand(Approve);
            CancelCommand = new RelayCommand(Cancel);

            Date = dateTimeProvider.Today;
        }
        
        public DateTime Date
        {
            get { return _date; }
            set { Set(ref _date, value); }
        }

        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value); }
        }

        public ValidationSummaryViewModel ValidationSummary { get; private set; }

        public ICommand ApproveCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }

        private async void Approve()
        {
            using (_businessManager.GetBusy())
            {
                var results = await _holidayService.Add(new Model.Holiday.Holiday(Date, Name));

                if (results.IsValid)
                {
                    _navigationService.GoBack(true);
                    return;
                }

                ValidationSummary.ErrorMessages = results.ErrorMessages;
            }
        }

        private void Cancel()
        {
            _navigationService.GoBack();
        }
    }
}