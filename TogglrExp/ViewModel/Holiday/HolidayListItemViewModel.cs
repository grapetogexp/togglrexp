﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Holiday;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.Holiday
{
    public class HolidayListItemViewModel : ObservableObject
    {
        private readonly INavigationService _navigationService;
        private readonly IHolidayService _holidayService;
        private readonly IBusinessManager _businessManager;
        private readonly Func<string, QuestionViewModel> _dialogFactory;

        public HolidayListItemViewModel(
            Model.Holiday.Holiday holiday,
            INavigationService navigationService,
            IHolidayService holidayService,
            IBusinessManager businessManager,
            Func<string, QuestionViewModel> dialogFactory)
        {
            _navigationService = navigationService;
            _holidayService = holidayService;
            _businessManager = businessManager;
            _dialogFactory = dialogFactory;
            Name = holiday.Name;
            Date = holiday.Date;
            DeleteCommand = new RelayCommand(Delete);
        }

        public string Name { get; private set; }

        public DateTime Date { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        private async void Delete()
        {
            var dialog = _dialogFactory("Czy usunąć?");
            bool result = await _navigationService.Open(dialog);
            if (result)
            {
                using (_businessManager.GetBusy())
                {
                    await _holidayService.Delete(Date);
                }
            }
        }
    }
}