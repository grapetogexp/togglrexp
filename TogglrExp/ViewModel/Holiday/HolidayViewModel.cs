﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using TogglrExp.Model.Holiday;
using TogglrExp.ViewModel.Business;
using TogglrExp.ViewModel.Navigation;
using TracerAttributes;

namespace TogglrExp.ViewModel.Holiday
{
    public class HolidayViewModel : ObservableObject, IDisposable
    {
        private readonly IHolidayService _holidayService;
        private readonly INavigationService _navigationService;
        private readonly IBusinessManager _businessManager;
        private readonly IMessenger _messenger;
        private readonly Func<AddHolidayViewModel> _addHolidayFactory;
        private readonly Func<Model.Holiday.Holiday, HolidayListItemViewModel> _listItemFactory;
        private IEnumerable<HolidayListItemViewModel> _holidays;
        private int _year;

        public HolidayViewModel(
            IHolidayService holidayService,
            INavigationService navigationService,
            IBusinessManager businessManager,
            IMessenger messenger,
            Func<AddHolidayViewModel> addHolidayFactory,
            Func<Model.Holiday.Holiday, HolidayListItemViewModel> listItemFactory)
        {
            _holidayService = holidayService;
            _navigationService = navigationService;
            _businessManager = businessManager;
            _messenger = messenger;
            _addHolidayFactory = addHolidayFactory;
            _listItemFactory = listItemFactory;

            _messenger.Register<HolidayRemovedMessage>(this, OnRemoveHoliday);

            Year = DateTime.Now.Year;

            ValidationSummary = new ValidationSummaryViewModel();

            AddCommand = new RelayCommand(Add);
            FillWeekendsCommand = new RelayCommand(FillWeekends);
            GoBackCommand = new RelayCommand(GoBack);
            RefreshCommand = new RelayCommand(Refresh);
        }

        public ICommand AddCommand { get; private set; }
        public ICommand FillWeekendsCommand { get; private set; }
        public ICommand GoBackCommand { get; private set; }

        public IEnumerable<HolidayListItemViewModel> Holidays
        {
            get { return _holidays; }
            set { Set(ref _holidays, value); }
        }

        public ICommand RefreshCommand { get; private set; }

        public ValidationSummaryViewModel ValidationSummary { get; private set; }

        public int Year
        {
            get { return _year; }
            set { Set(ref _year, value); }
        }

        private void GoBack()
        {
            _navigationService.GoBack();
        }

        [TraceOn]
        private async void Add()
        {
            bool result = await _navigationService.Open(_addHolidayFactory());
            if (result)
            {
                Refresh();
            }
        }

        [TraceOn]
        private async void Refresh()
        {
            using (_businessManager.GetBusy())
            {
                ValidationSummary.Reset();
                try
                {
                    Holidays = await GetItems();
                }
                catch (Exception ex)
                {
                    ValidationSummary.ErrorMessages = new[] { ex.Message };
                }
            }
        }

        [TraceOn]
        private async void FillWeekends()
        {
            using (_businessManager.GetBusy())
            {
                ValidationSummary.Reset();
                try
                {
                    await _holidayService.FillWeekends(Year);
                    Holidays = await GetItems();
                    ValidationSummary.Infos = new[] { "Uzupełniono weekendy" };
                }
                catch (Exception ex)
                {
                    ValidationSummary.ErrorMessages = new[] { ex.Message };
                }
            }
        }

        private async Task<IEnumerable<HolidayListItemViewModel>> GetItems()
        {
            var dateFrom = new DateTime(Year, 1, 1);
            var dateTo = new DateTime(Year, 12, 31);
            var holidays = await _holidayService.HolidayList(dateFrom, dateTo);
            return holidays.Select(h => _listItemFactory(h));
        }

        private void OnRemoveHoliday(HolidayRemovedMessage message)
        {
            Refresh();
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool isDisposing)
        {
            if (isDisposing) return;

            _messenger.Unregister(this);
        }
        #endregion
    }
}