﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.DbUpdate;
using TogglrExp.ViewModel.About;
using TogglrExp.ViewModel.Holiday;
using TogglrExp.ViewModel.Navigation;
using TogglrExp.ViewModel.NotificationCenter;
using TogglrExp.ViewModel.OvertimeReport;
using TogglrExp.ViewModel.Timetrack;
using Tracer.Log4Net;

namespace TogglrExp.ViewModel
{
    public class MainWindowViewModel : ViewModelBase, IDisposable
    {
        private readonly Func<HolidayViewModel> _holidayViewModelFactory;
        private readonly INavigationService _navigationService;
        private readonly IDbUpdateService _dbUpdateService;
        private readonly Func<OvertimeReportViewModel> _overtimeReportViewModelFactory;
        private readonly Func<AboutViewModel> _aboutViewModelFactory;
        private readonly Func<SettingsViewModel> _settingsFactory;
        private readonly Func<TimeTrackViewModel> _timeTrackFactory;
        private object _content;

        private bool _isBusy;
        private bool _isOnTop;

        public MainWindowViewModel(
            Func<SettingsViewModel> settingsFactory,
            Func<TimeTrackViewModel> timeTrackFactory,
            Func<HolidayViewModel> holidayViewModelFactory,
            Func<OvertimeReportViewModel> overtimeReportViewModelFactory,
            Func<AboutViewModel> aboutViewModelFactory,
            INavigationService navigationService,
            NotificationCenterViewModel notificationCenterViewModel,
            IDbUpdateService dbUpdateService)
        {
            _settingsFactory = settingsFactory;
            _timeTrackFactory = timeTrackFactory;
            _holidayViewModelFactory = holidayViewModelFactory;
            _overtimeReportViewModelFactory = overtimeReportViewModelFactory;
            _aboutViewModelFactory = aboutViewModelFactory;
            _navigationService = navigationService;
            NotificationCenter = notificationCenterViewModel;
            _dbUpdateService = dbUpdateService;
            _navigationService.CurrentChanged += NavigationServiceOnCurrentChanged;
            
            OpenHolidaysCommand = new RelayCommand(OpenHoliday);
            OpenOvertimeReportCommand = new RelayCommand(OpenOvertimeReport);
            OpenTimetrackCommand = new RelayCommand(OpenTimetrack);
            OpenSettingsCommand = new RelayCommand(OpenSettings);
            OpenAboutCommand = new RelayCommand(OpenAbout);

            LoadContent();

            Initialize();
        }

        public object Content
        {
            get { return _content; }
            set { Set(ref _content, value); }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set { Set(ref _isBusy, value); }
        }

        public bool IsOnTop
        {
            get { return _isOnTop; }
            private set { Set(ref _isOnTop, value); }
        }

        public ICommand OpenHolidaysCommand { get; private set; }
        public ICommand OpenOvertimeReportCommand { get; private set; }
        public ICommand OpenSettingsCommand { get; private set; }
        public ICommand OpenTimetrackCommand { get; private set; }
        public ICommand OpenAboutCommand { get; private set; }

        public NotificationCenterViewModel NotificationCenter { get; private set; }

        private async void Initialize()
        {
            IsBusy = true;
            try
            {
                await _dbUpdateService.UpdateDataBase();
            }
            finally
            {
                IsBusy = false;
            }

            if (IsInDesignMode)
            {
                OpenTimetrack();
            }
        }

        private void NavigationServiceOnCurrentChanged(object sender, EventArgs eventArgs)
        {
            LoadContent();
        }
        
        private void LoadContent()
        {
            Log.Debug("Main Window content changed");
            Content = _navigationService.Current;
            IsOnTop = Content == null;
        }

        private void OpenHoliday()
        {
            _navigationService.Open(_holidayViewModelFactory());
        }

        private async void OpenOvertimeReport()
        {
            using (var overtimeReport = _overtimeReportViewModelFactory())
            {
                await _navigationService.Open(overtimeReport);
            }
        }

        private async void OpenSettings()
        {
            using (var settings = _settingsFactory())
            {
                await _navigationService.Open(settings);
            }
        }

        private async void OpenTimetrack()
        {
            using (var timeTrack = _timeTrackFactory())
            {
                await _navigationService.Open(timeTrack);
            }
        }

        private void OpenAbout()
        {
            _navigationService.Open(_aboutViewModelFactory());
        }

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool isDisposing)
        {
            if (!isDisposing) return;

            _navigationService.CurrentChanged -= NavigationServiceOnCurrentChanged;
        }

        #endregion
    }
}