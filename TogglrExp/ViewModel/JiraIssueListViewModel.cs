﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using TogglrExp.Model.Jira;

namespace TogglrExp.ViewModel
{
    public class JiraIssueListViewModel : ObservableObject, IDisposable
    {
        private readonly IJiraService _jiraService;

        private CancellationTokenSource _issuesCancellation;
        
        private JiraIssueModel _selectedIssue;
        private IEnumerable<JiraIssueModel> _issues;

        private bool _areIssuesLoading;
        
        public JiraIssueListViewModel(
            IJiraService jiraService,
            Func<JiraConfigurationSelectorViewModel> configurationSelectorFactory)
        {
            _jiraService = jiraService;
            ConfigurationSelector = configurationSelectorFactory();
            ReloadIssuesCommand = new RelayCommand(ReloadIssues);
            ConfigurationSelector.SelectedConfigurationChanged += OnSelectedConfigurationChanged;
            ReloadIssues();
        }
        
        public event EventHandler SelectedIssueChanged;

        public JiraConfigurationSelectorViewModel ConfigurationSelector { get; }

        public JiraIssueModel SelectedIssue
        {
            get { return _selectedIssue; }
            set
            {
                if (Set(ref _selectedIssue, value))
                {
                    SavePreferedIssue();
                    OnSelectedIssueChanged();
                }
            }
        }

        public ICommand ReloadIssuesCommand { get; }

        public IEnumerable<JiraIssueModel> Issues
        {
            get { return _issues; }
            private set { Set(ref _issues, value); }
        }

        public bool AreIssuesLoading
        {
            get { return _areIssuesLoading; }
            set { Set(ref _areIssuesLoading, value); }
        }

        private async void SavePreferedIssue()
        {
            var selectedConfiguration = ConfigurationSelector.SelectedConfiguration;
            if (selectedConfiguration == null)
            {
                return;
            }
            var selectedConfigurationId = selectedConfiguration.Id;

            if (SelectedIssue == null)
            {
                return;
            }
            
            await _jiraService.SetPreferedIssueKey(selectedConfigurationId, SelectedIssue.Key);
        }

        private async void ReloadIssues()
        {
            if (_issuesCancellation != null)
            {
                _issuesCancellation.Cancel();
                _issuesCancellation.Dispose();
            }
            _issuesCancellation = new CancellationTokenSource();

            AreIssuesLoading = true;
            try
            {
                var selectedConfiguration = ConfigurationSelector.SelectedConfiguration;
                if (selectedConfiguration == null)
                {
                    Issues = Enumerable.Empty<JiraIssueModel>();
                    return;
                }

                long selectedConfigurationId = selectedConfiguration.Id;

                var issues = await _jiraService.GetIssues(selectedConfigurationId, "assignee=currentUser()", _issuesCancellation.Token);
                Issues = issues as IList<JiraIssueModel> ?? issues.ToList();

                // Workaround for combo behavior
                SelectedIssue = null;

                var preferedIssueKey = await _jiraService.GetPreferedIssueKey(selectedConfigurationId);

                var toSelect =
                    Issues.FirstOrDefault(
                        o => string.Equals(o.Key, preferedIssueKey, StringComparison.InvariantCultureIgnoreCase));
                if (toSelect == null)
                {
                    toSelect = Issues.FirstOrDefault();
                }
                SelectedIssue = toSelect;
            }
            finally
            {
                AreIssuesLoading = false;
            }
        }

        protected virtual void OnSelectedIssueChanged()
        {
            SelectedIssueChanged?.Invoke(this, EventArgs.Empty);
        }

        private void OnSelectedConfigurationChanged(object sender, EventArgs e)
        {
            ReloadIssues();
        }

        #region IDisposable

        public void Dispose()
        {
            if (_issuesCancellation != null)
            {
                _issuesCancellation.Cancel();
                _issuesCancellation.Dispose();
            }
            ConfigurationSelector.SelectedConfigurationChanged -= OnSelectedConfigurationChanged;
            ConfigurationSelector.Dispose();
        }

        #endregion
    }
}