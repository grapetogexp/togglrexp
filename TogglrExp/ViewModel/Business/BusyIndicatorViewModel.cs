﻿using System;
using GalaSoft.MvvmLight;
using TogglrExp.ViewModel.Navigation;
using Tracer.Log4Net;

namespace TogglrExp.ViewModel.Business
{
    public class BusyIndicatorViewModel : ObservableObject, IDisposable
    {
        private readonly INavigationService _navigationService;

        public BusyIndicatorViewModel(
            INavigationService navigationService)
        {
            Log.Debug("Busy indicator appears");
            _navigationService = navigationService;
        }

        public void Dispose()
        {
            Log.Debug("Busy indicator disappers");
            _navigationService.GoBack();
        }
    }
}