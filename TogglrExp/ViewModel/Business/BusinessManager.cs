﻿using System;
using TogglrExp.ViewModel.Navigation;

namespace TogglrExp.ViewModel.Business
{
    public class BusinessManager : IBusinessManager
    {
        private readonly INavigationService _navigationService;

        public BusinessManager(
            INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public IDisposable GetBusy()
        {
            var result = new BusyIndicatorViewModel(_navigationService);
            _navigationService.Open(result);
            return result;
        }
    }
}