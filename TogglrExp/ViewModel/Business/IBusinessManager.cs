﻿using System;

namespace TogglrExp.ViewModel.Business
{
    public interface IBusinessManager
    {
        IDisposable GetBusy();
    }
}