﻿using System;
using System.Diagnostics;

namespace TogglrExp.Infrastructure
{
    public class UriOpener : IUriOpener
    {
        public void Open(Uri uri)
        {
            var processInfo = new ProcessStartInfo(uri.AbsoluteUri);
            Process.Start(processInfo);
        }
    }
}