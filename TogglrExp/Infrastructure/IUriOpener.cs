﻿using System;

namespace TogglrExp.Infrastructure
{
    public interface IUriOpener
    {
        void Open(Uri uri);
    }
}